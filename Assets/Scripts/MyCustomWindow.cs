﻿using Orion.DevConsole;
using Orion.DevConsole.UI;
using UnityEngine;

public class MyCustomWindow : CustomConsoleWindow, IConsoleOpeningHandler
{
    public MyCustomWindow()
    {
        Caption = "My Custom Window";

        this.AddButton("1");
        this.AddButton("2");
    }

    public void OnConsoleOpening()
    {
        //Debug.Log("Open");
    }
}
