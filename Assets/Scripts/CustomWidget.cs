﻿using Orion.DevConsole;
using Orion.DevConsole.UI;
using UnityEngine;

public class CustomWidget : IConsoleWidget, IConsoleOpeningHandler, IConsoleClosedHandler
{
    private IConsoleContainerWidget _container;
    private FloatInputField _x;
    private FloatInputField _y;

    public RectTransform RectTransform => _container.RectTransform;

    public bool Visible { get => _container.Visible; set => _container.Visible = value; }
    
    public bool Interactable { get => _container.Interactable; set => _container.Interactable = value; }


    public CustomWidget() : this(ConsoleHorizontalLayout.Create()) { }

    public CustomWidget(string title) : this(ConsolePropertyLayout.Create(title)) { }

    private CustomWidget (IConsoleContainerWidget container)
    {
        _container = container;

        _x = _container.AddFloatInputField();
        _y = _container.AddFloatInputField();

        _container.AddButton("Apply")
            .Click += OnApplyClick;
    }


    public void OnConsoleOpening()
    {
        //Debug.Log("Open");
        _x.Value = UnityEngine.Random.Range(0f, 1f);
        _y.Value = UnityEngine.Random.Range(0f, 1f);
    }

    public void OnConsoleClosed()
    {
        //Debug.Log("Close");
    }

    private void OnApplyClick(ConsoleButton button)
    {
        Debug.Log(_x.Value);
        Debug.Log(_y.Value);
    }
}


public static class CustomWidgetExtensions
{
    public static CustomWidget AddCustomWidget(this IConsoleContainerWidget container)
    {
        var widget = new CustomWidget();
        container.AddWidget(widget);
        return widget;
    }

    public static CustomWidget AddCustomWidgetProperty(this IConsoleContainerWidget container, string title)
    {
        var widget = new CustomWidget(title);
        container.AddWidget(widget);
        return widget;
    }
}
