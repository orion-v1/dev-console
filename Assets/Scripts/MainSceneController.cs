﻿using Orion;
using Orion.DevConsole;
using UnityEngine;

public class MainSceneController : MonoBehaviour
{
    private void Awake()
    {
        Application.targetFrameRate = 60;

        AppHost.CreateDefault()
            //.AddConfigureAction(AppHostLayer.CommonServiceModules, host => Debug.Log("BOOTSTRAP"))
            .AddDevConsole<AppDevConsole>(ConfigureDevConsole)
            .Run();
    }

    private void ConfigureDevConsole(DevConsoleConfig config)
    {
        config.Canvas.SortingOrder = 20;
        config.ConsoleTriggers.Add(new HotKeyTrigger(KeyCode.D, KeyModifiers.Ctrl));
        config.ConsoleTriggers.Add(new ThreeFingersTapTrigger());
        config.ConsoleTriggers.Add(new OverlayButtonTrigger());
    }

#if UNITY_EDITOR
    [UnityEditor.MenuItem("Dev Console/Clear PlayerPrefs")]
    private static void RemoveState()
    {
        PlayerPrefs.DeleteAll();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            Debug.Log("Test 0");
            Debug.LogWarning("Test 1");
            Debug.LogError("Test 2");
            Debug.LogAssertion("Assert");
        }
    }
#endif
}
