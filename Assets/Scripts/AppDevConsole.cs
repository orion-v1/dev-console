﻿using Orion.DevConsole;
using Orion.DevConsole.UI;
using UnityEngine;

public class AppDevConsole : DevConsole
{
    protected override void OnInitialize(IDevConsoleDesigner console)
    {
        base.OnInitialize(console);

        console.AddFpsMeterOverlay()
            //.SetOverlayVisibleByDefault(false)
            //.SetDefaultPosition(new Vector2(150f, -100f), new Vector2(0f, .8f))
            ;

        console.AddUnityLogPage()
            .SetShowErrors(true)
            .SetShowWarnings(true)
            .SetShowInfo(true)
            //.SetOverlayVisibleByDefault(false)
            ;

        var page = console.AddPage("General");
        MoveToPage(page);

        var window = page.AddWindow("Main");
        window.AddButton("Button 1");
        window.AddButton("Button 2");
        window.AddTextArea();

        window = page.AddWindow("Demo");
        window.AddText("Simple text");
        window.AddToggleProperty("Option 1");
        window.AddToggleProperty("Option 2");

        window.AddSplitter();

        // Dropdown

        window.AddStringDropdownProperty("Dropdown list")
            .AddItem("Item 1")
            .AddItem("Item 2")
            .AddItems("Item 3", "Item 4", "Item 5", "Item 6")
            //.SetMaxLines(5)
            .SetSelectedItemIndex(2)
            .ValueChanged += sender => Debug.Log(sender.Value);

        window.AddEnumDropdownProperty<TestEnum>("Dropdown list with enum")
            .SetValue(TestEnum.Two)
            .ValueChanged += sender => Debug.Log(sender.Value);

        // Selection

        var sp = window.AddStringSelectionProperty("String Selection");
        for (int i = 0; i < 30; i++)
            sp.AddItem($"Item #{i}");
        sp.SelectedItemIndex = 23;

        window.AddEnumSelectionProperty<TestEnum>("Enum Selection");

        // Input field

        window.AddStringProperty("Input field (text)")
            .AddValueChangedHandler(OnTextChanged);

        window.AddIntProperty("Input field (int)")
            .SetValue(123)
            .ValueChanged += sender => Debug.Log(sender.Value);

        window.AddFloatProperty("Input field (float)")
            .SetValue(123.321f)
            .ValueChanged += sender => Debug.Log(sender.Value);

        // Slider
        window.AddIntSliderProperty("Slider (int)")
            .SetMaxValue(20)
            .SetValue(5);

        var slider = window.AddFloatSliderProperty("Slider (float)")
            .SetValue(.5f);

        // Toggle

        window.AddToggleProperty("Show value")
            .SetValue(true)
            .ValueChanged += sender => slider.ValueViewVisible = sender.Value;

        // Property

        var p = window.AddPropertyLayout("Custom property");
        p.AddText("X:");
        p.AddIntInputField();
        p.AddText("Y:");
        p.AddIntInputField();
        p.AddButton("Apply");

        window.AddVector2Property("Point (Vector2)")
            .ValueChanged += sender => Debug.Log(sender.Value);

        window.AddVector3Property("Point (Vector3)")
            .ValueChanged += sender => Debug.Log(sender.Value);


        window.AddLabelProperty("title", "text");

        var intField = window.AddIntInputField()
            .SetValue(123);
            //.AddValueChangedHandler(sender => Debug.Log($"Changed: {sender.Value}"))
            //.AddEndEditHandler(sender => Debug.Log($"End: {sender.Value}"))
        //intField.ValueChanged += sender => Debug.Log($"Changed: {sender.Value}");
        //intField.EndEdit += sender => Debug.Log($"End: {sender.Value}");

        var floatField = window.AddFloatInputField()
            .SetValue(123.321f);
            //.AddValueChangedHandler(sender => Debug.Log($"Changed: {sender.Value}"))
            //.AddEndEditHandler(sender => Debug.Log($"End: {sender.Value}"))
        //floatField.ValueChanged += sender => Debug.Log($"Changed: {sender.Value}");
        //floatField.EndEdit += sender => Debug.Log($"End: {sender.Value}");

        // ColumnsLayout
        var layout = window.AddColumnsLayout();
            //.SetFillMode(ColumnsLayoutFillMode.ByColumns);

        layout.AddToggleProperty("tgl");
        layout.AddToggleProperty("tgl");
        layout.AddButton("test1");
        layout.AddButton("test2");
        layout.AddButton("test3");
        layout.AddButton("test4");


        window.AddWidget(new CustomWidget());
        page.AddWidget(new MyCustomWindow());

        page = console.AddPage(/*scrollable: false*/);
        AddInfoWindow(page);
    }

    private void AddInfoWindow(IConsolePage page)
    {
        var info = page.AddSystemInfoWindow();
        info.MiscSection.AddRecord("Name 0", "Value 0");

        var section = info.GetOrAddSection("My Section");
        section.AddRecord("Name 1", "Value 1");
        section.AddRecord("Name 2", "Value 2");
    }

    private void OnTextChanged(StringInputField sender)
    {
        Debug.Log(sender.Value);
    }

    //protected override void OnConsoleOpening()
    //{
    //    base.OnConsoleOpening();
    //}

    //protected override void OnConsoleClosed()
    //{
    //    base.OnConsoleClosed();
    //}
}


public enum TestEnum
{
    Zero,
    One,
    Two,
}
