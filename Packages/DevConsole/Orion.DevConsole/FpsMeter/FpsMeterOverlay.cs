﻿using Orion.DevConsole.UI;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

// more features: https://github.com/ahmedmohi/iProfiler/blob/master/Assets/iProfiler/StatsMan.cs

namespace Orion.DevConsole
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    public class FpsMeterOverlay : MonoBehaviour, IConsoleOverlayWidget, IPointerDownHandler, IPointerUpHandler
    {
        private const string PositionKey = "FpsMeter.Position";
        private const string OverlayVisibleKey = "FpsMeter.OverlayVisible";

        [SerializeField] private Sprite _toolbarIcon;
        [SerializeField] private UiMovableObject _movableObject;
        [SerializeField] private Image _background;
        [SerializeField] private Text _peakFpsView;
        [SerializeField] private Text _meanFpsView;
        [SerializeField] private float _measurementInterval = .5f;

        private ToolbarToggleButton _toolbarToggle;
        private bool _initialized;
        private bool _visible = true;
        private Vector2 _position = new Vector2(150, 100);
        private Vector2 _anchor = Vector2.zero;
        private float _backgroundNormalAlpha;
        private float _backgroundFadeTime = 0f;

        private int _framesCount = 0;
        private float _time = 0f;
        private float _lastTs;
        private float _peakFps;
        private float _meanFps;
        private bool _isMeanFpsAvailable;


        public RectTransform RectTransform => transform as RectTransform;

        public Vector2 Position { get => _movableObject.Position; set => SetPosition(value); }

        public bool Visible { get => _visible; set => SetVisible(value); }


        internal static FpsMeterOverlay Create()
        {
            const string AssetPath = "FpsMeter/FpsMeterOverlay";
            return InternalResources.CreateFromResource<FpsMeterOverlay>(AssetPath);
        }

        internal void Init(IDevConsoleDesigner console)
        {
            _backgroundNormalAlpha = _background.color.a;

            // toolbar button
            _toolbarToggle = ConsoleUtils.CreateToolbarToggleButton("FPS", _toolbarIcon);
            _toolbarToggle.ValueChanged += sender => SetVisible(sender.Value, true);
            console.Dashboard.AddToolbarWidget(_toolbarToggle);

            // movable object
            _movableObject.ResetTransformAnchors();
            _movableObject.EndDrag += sender => ConsoleStateData.SetVector2(PositionKey, sender.Position);

            _initialized = true;

            // visibility
            bool visible = ConsoleStateData.GetBool(OverlayVisibleKey, _visible);
            SetVisible(visible, false);

            // position
            var pos = ConsoleStateData.GetVector2(PositionKey);
            if (pos.HasValue) SetPosition(pos.Value, false);
            else SetPosition(_position, _anchor, false);
        }

        private void OnEnable()
        {
            ResetMesurement();
            UpdateView();

            StartCoroutine(DeferredResetMeanFps());
        }

        private void LateUpdate()
        {
            UpdateBackgroundAlpha();
            UpdateMeasurement();
        }

        private void UpdateView()
        {
            _peakFpsView.text = _peakFps.ToString("F1");
            _meanFpsView.text = _isMeanFpsAvailable
                ? _meanFps.ToString("F1")
                : "-";
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            HighlightBackground(10000f);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            HighlightBackground(3f);
        }


        private void SetVisible(bool value, bool keep = false)
        {
            _visible = value;
            if (keep) ConsoleStateData.SetBool(OverlayVisibleKey, value);

            if (!_initialized) return;
            gameObject.SetActive(value);
            _toolbarToggle.Value = value;
        }

        private void SetPosition(Vector2 position, Vector2 anchor, bool keep = false)
        {
            _position = position;
            _anchor = anchor;

            if (!_initialized) return;
            _movableObject.SetPosition(position, anchor);
            if (keep) ConsoleStateData.SetVector2(PositionKey, _movableObject.Position);
        }

        private void SetPosition(Vector2 position, bool keep = false)
            => SetPosition(position, new Vector2(.5f, .5f), keep);

        #region Measurement

        private void UpdateMeasurement()
        {
            _framesCount++;
            _time += Time.unscaledDeltaTime;
            if (_time < _measurementInterval) return;

            float ts = Time.realtimeSinceStartup;
            float dt = ts - _lastTs;

            _peakFps = dt > 0 ? _framesCount / dt : _framesCount;
            _meanFps += (_peakFps - _meanFps) * .25f;

            UpdateView();

            _lastTs = ts;
            _time = 0f;
            _framesCount = 0;
        }

        private IEnumerator DeferredResetMeanFps()
        {
            yield return new WaitForSecondsRealtime(2f);

            _meanFps = _peakFps;
            _isMeanFpsAvailable = true;
        }

        private void ResetMesurement()
        {
            _framesCount = 0;
            _time = 0f;
            _lastTs = Time.realtimeSinceStartup;
            _peakFps = 0f;
            _meanFps = 0f;
            _isMeanFpsAvailable = false;
        }

        #endregion

        #region Background fade

        private void UpdateBackgroundAlpha()
        {
            if (_backgroundFadeTime <= 0f) return;

            _backgroundFadeTime -= Time.unscaledDeltaTime;
            float f = Mathf.Clamp(_backgroundFadeTime, 0f, 1f);

            var color = _background.color;
            color.a = Mathf.Lerp(_backgroundNormalAlpha, .7f, f);
            _background.color = color;
        }

        private void HighlightBackground(float duration)
        {
            _backgroundFadeTime = duration;
        }

        #endregion

        #region API Chaining

        public FpsMeterOverlay SetOverlayVisibleByDefault(bool value)
        {
            _visible = value;
            return this;
        }

        public FpsMeterOverlay SetDefaultPosition(Vector2 position, Vector2 anchor)
        {
            _position = position;
            _anchor = anchor;
            return this;
        }

        public FpsMeterOverlay SetDefaultPosition(Vector2 position)
        {
            _position = position;
            _anchor = new Vector2(.5f, .5f);
            return this;
        }

        #endregion
    }
}
