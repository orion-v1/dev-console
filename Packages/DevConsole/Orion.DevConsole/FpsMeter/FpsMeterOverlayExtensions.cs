﻿namespace Orion.DevConsole
{
    public static class FpsMeterOverlayExtensions
    {
        public static FpsMeterOverlay AddFpsMeterOverlay(this IDevConsoleDesigner console)
        {
            var widget = FpsMeterOverlay.Create();
            console.Overlay.AddWidget(widget);
            console.AddInitializeAction(widget.Init);
            return widget;
        }
    }
}
