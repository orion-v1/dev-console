﻿using System.Collections.Generic;
using UnityEngine;

namespace Orion.DevConsole.UI
{
    [RequireComponent(typeof(RectTransform))]
    public class ConsolePage : MonoBehaviour, IConsolePage
    {
        [SerializeField] private RectTransform _rectTransform;
        [SerializeField] private RectTransform _contentRoot;
        [SerializeField] private CanvasGroup _canvasGroup;

        private List<IConsoleWidget> _widgets = new List<IConsoleWidget>();

        public RectTransform RectTransform => _rectTransform;
        public RectTransform ContentRoot => _contentRoot;
        public IReadOnlyList<IConsoleWidget> Widgets => _widgets;
        public string Title { get; set; }

        public bool Visible
        {
            get => _rectTransform.gameObject.activeSelf;
            set => _rectTransform.gameObject.SetActive(value);
        }

        public bool Interactable
        {
            get => _canvasGroup.interactable;
            set => _canvasGroup.interactable = value;
        }

        public void AddWidget(IConsoleWidget widget)
        {
            _widgets.Add(widget);
            widget.RectTransform.SetParent(_contentRoot, false);
        }
    }


    public static class ConsolePageExtensions
    {
        private const string EmptyPageAssetPath = "Pages/EmptyPage";
        private const string ScrollableEmptyPageAssetPath = "Pages/ScrollableEmptyPage";

        public static IConsolePage AddPage(this IDevConsoleDesigner console, string title = null, bool scrollable = true)
        {
            var page = InternalResources.CreateFromResource<ConsolePage>(scrollable
                ? ScrollableEmptyPageAssetPath
                : EmptyPageAssetPath);

            page.Title = title;
            console.AddPage(page);
            return page;
        }
    }
}
