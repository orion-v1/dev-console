﻿using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Orion.DevConsole.UnityLog
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    internal class LogRecordDetailsView : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private Text _type;
        [SerializeField] private Text _time;
        [SerializeField] private Text _message;
        [SerializeField] private Text _stackTrace;
        [SerializeField] private Button _copyButton;
        [SerializeField] private Button _closeButton;

        private LogRecord _record;


        private void Awake()
        {
            _copyButton.onClick.AddListener(OnCopyClicked);
            _closeButton.onClick.AddListener(OnCloseClicked);
        }

        public void Show(LogRecord record)
        {
            _record = record;

            string colorTag = UnityLogUtils.GetRichTextColorTag(_record.LogType);
            if (string.IsNullOrEmpty(colorTag))
                _type.text = UnityLogUtils.GetRecordTypeName(record.LogType);
            else
            {
                _type.text = string.Concat(
                    colorTag,
                    UnityLogUtils.GetRecordTypeName(record.LogType),
                    "</color>");
            }


            _time.text = record.Time.ToLongTimeString();
            _message.text = UnityLogUtils.TruncateMessageIfRequired(record.Message);
            _stackTrace.text = UnityLogUtils.TruncateMessageIfRequired(record.StackTrace);

            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }


        private void OnCopyClicked()
        {
            StringBuilder sb = new StringBuilder(512);

            sb.Append("Type: ");
            sb.AppendLine(UnityLogUtils.GetRecordTypeName(_record.LogType));
            sb.Append("Time: ");
            sb.AppendLine(_record.Time.ToLongTimeString());

            sb.AppendLine();
            sb.AppendLine("Message:");
            sb.AppendLine(_record.Message);

            sb.AppendLine();
            sb.AppendLine("Stack Trace:");
            sb.AppendLine(_record.StackTrace);

            ConsoleUtils.Clipboard = sb.ToString();
        }

        private void OnCloseClicked() => Hide();

        public void OnPointerClick(PointerEventData eventData) => Hide();
    }
}
