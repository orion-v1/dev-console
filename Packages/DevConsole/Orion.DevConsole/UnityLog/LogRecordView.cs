﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Orion.DevConsole.UnityLog
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    public class LogRecordView : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private Image _icon;
        [SerializeField] private Text _text;

        private RectTransform _rectTransform;
        private LogRecord _record;


        public event Action<LogRecordView> Click;

        public RectTransform RectTransform => _rectTransform ? _rectTransform : (_rectTransform = transform as RectTransform);

        public LogRecord Record => _record;


        public void SetRecord(LogRecord record)
        {
            _record = record;
            UpdateView();
            UpdateLayout();
        }

        private string GetTopMethodFromStackTrace(string stackTrace)
        {
            int startIdx = 0;

            while (startIdx < stackTrace.Length)
            {
                int lastIdx = _record.StackTrace.IndexOf('\n', startIdx);
                string line = null;
                if (lastIdx < 0)
                {
                    line = startIdx == 0
                        ? _record.StackTrace
                        : _record.StackTrace.Substring(startIdx);
                    startIdx = stackTrace.Length;
                }
                else
                {
                    int len = lastIdx - startIdx;
                    if (len > 0) line = _record.StackTrace.Substring(startIdx, len);
                    startIdx = lastIdx + 1;
                }

                if (string.IsNullOrEmpty(line)) continue;
                if (line.StartsWith("UnityEngine.Debug:Log")) continue;
                return line;
            }

            return string.Empty;
        }

        public void UpdateView()
        {
            StringBuilder sb = new StringBuilder(512);

            // Message
            string colorTag = UnityLogUtils.GetRichTextColorTag(_record.LogType);
            if (!string.IsNullOrEmpty(colorTag)) sb.Append(colorTag);

            sb.Append('[');
            sb.Append(_record.Time.ToLongTimeString());
            sb.Append("] ");
            sb.Append(UnityLogUtils.TruncateMessageIfRequired(_record.Message));

            if (sb[sb.Length - 1] != '\n')
                sb.AppendLine();

            if (!string.IsNullOrEmpty(colorTag)) sb.Append("</color>");

            // Stack trace top method
            sb.Append("<color=#999999>");
            sb.Append(GetTopMethodFromStackTrace(_record.StackTrace));
            sb.Append("</color>");

            _text.text = sb.ToString();

            // icon
            switch (_record.LogType)
            {
                case LogType.Error:
                case LogType.Exception:
                case LogType.Assert:
                    _icon.sprite = InternalResources.Instance.ErrorIcon;
                    _icon.color = InternalResources.Instance.ErrorIconColor;
                    break;

                case LogType.Warning:
                    _icon.sprite = InternalResources.Instance.WarningIcon;
                    _icon.color = InternalResources.Instance.WarningIconColor;
                    break;

                default:
                    _icon.sprite = InternalResources.Instance.InfoIcon;
                    _icon.color = InternalResources.Instance.InfoIconColor;
                    break;
            }
        }

        public void UpdateLayout()
        {
            //_text.CalculateLayoutInputHorizontal();
            //float width = LayoutUtility.GetPreferredWidth(_text.rectTransform) + _text.rectTransform.offsetMin.x;
            float width = _text.preferredWidth + _text.rectTransform.offsetMin.x;

            RectTransform rt = transform as RectTransform;
            rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
        }

        public void OnPointerClick(PointerEventData eventData) => Click?.Invoke(this);
    }
}
