﻿using Orion.DevConsole.UI;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Orion.DevConsole.UnityLog
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    public class UnityLogConsolePage : ConsolePage
    {
        private const int Capacity = 256;
        private const string OverlayVisibleKey = "UnityLogOverlay.Visible";

        private const int InfoRecordMask = 1 << (int)LogType.Log;
        private const int WarningRecordMask = 1 << (int)LogType.Warning;
        private const int ErrorRecordMask =
            1 << (int)LogType.Error |
            1 << (int)LogType.Exception |
            1 << (int)LogType.Assert;

        [Header("Templates")]
        [SerializeField] private LogRecordView _recordViewTemplate;
        [SerializeField] private UnityLogOverlay _overlayTemplate;
        [SerializeField] private Sprite _toolbarIcon;

        [Header("Components")]
        [SerializeField] private NestedScrollRect _scroll;
        [SerializeField] private DimensionsChangeHandler _contentDimensions;
        [SerializeField] private LogRecordDetailsView _recordDetailsView;
        [SerializeField] private ToolbarToggleButton _errorsButton;
        [SerializeField] private ToolbarToggleButton _warningsButton;
        [SerializeField] private ToolbarToggleButton _infoButton;
        [SerializeField] private ToolbarToggleButton _clearButton;
        [SerializeField] private ToolbarToggleButton _autoscrollButton;

        private UnityLogOverlay _overlay;
        private ToolbarToggleButton _toolbarToggle;
        private bool _initialized;
        private bool _overlayVisible = true;
        private int _recordsFilter = ErrorRecordMask;
        private int _errorsCount;
        private int _warningsCount;
        private int _infoCount;


        public bool ShowErrors
        {
            get => IsRecordFilterEnabled(ErrorRecordMask);
            set => ChangeRecordsFilter(ErrorRecordMask, value, _errorsButton, _initialized);
        }

        public bool ShowWarnings
        {
            get => IsRecordFilterEnabled(WarningRecordMask);
            set => ChangeRecordsFilter(WarningRecordMask, value, _warningsButton, _initialized);
        }

        public bool ShowInfo
        {
            get => IsRecordFilterEnabled(InfoRecordMask);
            set => ChangeRecordsFilter(InfoRecordMask, value, _infoButton, _initialized);
        }

        public bool OverlayVisible
        {
            get => _overlayVisible;
            set => SetOverlayVisible(value);
        }


        internal static UnityLogConsolePage Create()
        {
            const string AssetPath = "Pages/UnityLogPage";
            return InternalResources.CreateFromResource<UnityLogConsolePage>(AssetPath);
        }

        private void Awake()
        {
            _errorsButton.Value = ShowErrors;
            _warningsButton.Value = ShowWarnings;
            _infoButton.Value = ShowInfo;
            _autoscrollButton.Value = _autoscroll == AutoscrollMode.Enabled;
            _recordDetailsView.Hide();

            _errorsButton.BottomText = "0";
            _warningsButton.BottomText = "0";
            _infoButton.BottomText = "0";

            _scroll.onValueChanged.AddListener(OnScrollValueChanged);
            _scroll.InitializePotentialDrag += OnScrollInitializePotentialDrag;
            _contentDimensions.Changed += OnScrollContentDimensionsChanged;
            _autoscrollButton.ValueChanged += OnAutoscrollButtonClicked;
            _clearButton.ValueChanged += OnClearButtonClicked;
            _errorsButton.ValueChanged += OnErrorsButtonClicked;
            _warningsButton.ValueChanged += OnWarningsButtonClicked;
            _infoButton.ValueChanged += OnInfoButtonClicked;
        }

        private void OnDestroy()
        {
            _scroll.onValueChanged.RemoveListener(OnScrollValueChanged);
            _scroll.InitializePotentialDrag -= OnScrollInitializePotentialDrag;
            _contentDimensions.Changed -= OnScrollContentDimensionsChanged;
            _autoscrollButton.ValueChanged -= OnAutoscrollButtonClicked;
            _clearButton.ValueChanged -= OnClearButtonClicked;
            _errorsButton.ValueChanged -= OnErrorsButtonClicked;
            _warningsButton.ValueChanged -= OnWarningsButtonClicked;
            _infoButton.ValueChanged -= OnInfoButtonClicked;

            LogRecordsCache.Current.DecReference();
        }

        internal void Init(IDevConsoleDesigner console)
        {
            // overlay
            _overlay = Instantiate(_overlayTemplate);
            _overlay.name = _overlayTemplate.name;
            _overlay.Init();
            console.Overlay.AddWidget(_overlay);

            // toolbar
            _toolbarToggle = ConsoleUtils.CreateToolbarToggleButton("Log", _toolbarIcon);
            _toolbarToggle.ValueChanged += sender => SetOverlayVisible(sender.Value, true);
            console.Dashboard.AddToolbarWidget(_toolbarToggle);

            _initialized = true;

            // visibility
            bool visible = ConsoleStateData.GetBool(OverlayVisibleKey, _overlayVisible);
            SetOverlayVisible(visible, false);

            // other
            LogRecordsCache.Current.IncReference();
            ConsoleUtils.StartCoroutine(DequeueRecords());
        }

        private void SetOverlayVisible(bool value, bool keep = false)
        {
            _overlayVisible = value;
            if (keep) ConsoleStateData.SetBool(OverlayVisibleKey, value);

            if (!_initialized) return;
            _overlay.Visible = value;
            _toolbarToggle.Value = value;
        }

        private void OnClearButtonClicked(ToolbarToggleButton toggleButton)
        {
            toggleButton.Value = false;
            Clear();
        }

        #region Autoscroll

        private AutoscrollMode _autoscroll = AutoscrollMode.Enabled;

        private AutoscrollMode Autoscroll
        {
            get => _autoscroll;
            set
            {
                _autoscroll = value;
                _autoscrollButton.Value = value == AutoscrollMode.Enabled;
            }
        }

        private void OnAutoscrollButtonClicked(ToolbarToggleButton toggleButton)
        {
            Autoscroll = toggleButton.Value ? AutoscrollMode.Enabled : AutoscrollMode.DisabledByButton;
            if (_autoscroll == AutoscrollMode.Enabled) ScrollToEnd(true);
        }

        private void OnScrollContentDimensionsChanged()
        {
            if (Autoscroll == AutoscrollMode.Enabled) ScrollToEnd(true);
        }

        private void OnScrollInitializePotentialDrag(PointerEventData obj)
        {
            Autoscroll = AutoscrollMode.DisabledByScroll;
        }

        private void OnScrollValueChanged(Vector2 value)
        {
            if (_scroll.IsDragging) return;
            if (_autoscroll == AutoscrollMode.DisabledByButton) return;
            if (_autoscroll == AutoscrollMode.DisabledByScroll && value.y <= 0f && value.x <= .1f)
            {
                Autoscroll = AutoscrollMode.Enabled;
                ScrollToEnd(false);
            }
        }

        private void ScrollToEnd(bool resetVelocity)
        {
            _scroll.StopMovement();

            float offset = _scroll.content.rect.height - _scroll.viewport.rect.height;
            if (offset > 0f)
                _scroll.content.anchoredPosition = new Vector2(0f, offset);
            //_scroll.velocity = (new Vector2(0f, offset) - _scroll.content.anchoredPosition) / .1f;
        }


        private enum AutoscrollMode
        {
            Enabled = 0,
            DisabledByButton,
            DisabledByScroll
        }

        #endregion

        #region Records filter

        private bool IsRecordFilterEnabled(int mask)
            => (_recordsFilter & mask) != 0;

        private bool IsRecordMatchFilter(LogType logType)
            => (_recordsFilter & (1 << (int)logType)) != 0;

        private void ChangeRecordsFilter(int mask, bool enable, ToolbarToggleButton button, bool updateView)
        {
            int filter = _recordsFilter;
            if (enable) filter |= mask;
            else filter &= ~mask;

            if (filter == _recordsFilter) return;
            _recordsFilter = filter;

            if (button) button.Value = enable;
            if (updateView) UpdateRecordsVisibility();
        }

        private void UpdateRecordsVisibility()
        {
            int count = _scroll.content.childCount;
            for (int i = 0; i < count; i++)
            {
                var recordView = _scroll.content.GetChild(i).GetComponent<LogRecordView>();
                recordView.gameObject.SetActive(IsRecordMatchFilter(recordView.Record.LogType));
            }
        }

        private void OnErrorsButtonClicked(ToolbarToggleButton toggleButton)
            => ChangeRecordsFilter(ErrorRecordMask, toggleButton.Value, toggleButton, true);

        private void OnWarningsButtonClicked(ToolbarToggleButton toggleButton)
            => ChangeRecordsFilter(WarningRecordMask, toggleButton.Value, toggleButton, true);

        private void OnInfoButtonClicked(ToolbarToggleButton toggleButton)
            => ChangeRecordsFilter(InfoRecordMask, toggleButton.Value, toggleButton, true);

        #endregion

        #region Records management

        private IEnumerator DequeueRecords()
        {
            while (true)
            {
                int counter = Capacity;
                while (counter > 0 && LogRecordsCache.Current.TryDequeue(out var record))
                {
                    AddRecord(record);
                    counter--;
                }

                yield return null;
            }
        }

        private void OnRecordClicked(LogRecordView sender)
        {
            _recordDetailsView.Show(sender.Record);
        }

        private void AddRecord(LogRecord record)
        {
            LogRecordView recordView;

            // create new or reuse existing view
            int count = _scroll.content.childCount;
            if (count < Capacity)
            {
                recordView = Instantiate(_recordViewTemplate, _scroll.content);
                recordView.Click += OnRecordClicked;
            }
            else
            {
                recordView = _scroll.content.GetChild(0).GetComponent<LogRecordView>();
                recordView.RectTransform.SetAsLastSibling();
            }

            // set record and apply filter
            recordView.SetRecord(record);
            recordView.gameObject.SetActive(IsRecordMatchFilter(record.LogType));

            // count
            switch (record.LogType)
            {
                case LogType.Exception:
                case LogType.Assert:
                case LogType.Error:
                    _errorsCount++;
                    _errorsButton.BottomText = _errorsCount.ToString();
                    _overlay.ErrorsCounter.Show(_errorsCount);
                    break;

                case LogType.Warning:
                    _warningsCount++;
                    _warningsButton.BottomText = _warningsCount.ToString();
                    _overlay.WarningsCounter.Show(_warningsCount);
                    break;

                case LogType.Log:
                    _infoCount++;
                    _infoButton.BottomText = _infoCount.ToString();
                    break;
            }
        }

        public void Clear()
        {
            LogRecordsCache.Current.Clear();

            _errorsCount = 0;
            _warningsCount = 0;
            _infoCount = 0;

            int count = _scroll.content.childCount;
            for (int i = 0; i < count; i++)
            {
                var t = _scroll.content.GetChild(0);
                t.SetParent(null); // needed to change _scroll.content.childCount
                Destroy(t.gameObject);
            }

            _errorsButton.BottomText = "0";
            _warningsButton.BottomText = "0";
            _infoButton.BottomText = "0";

            _overlay.ErrorsCounter.Clear();
            _overlay.WarningsCounter.Clear();
        }

        #endregion

        #region API Chaining

        public UnityLogConsolePage SetShowErrors(bool value)
        {
            ShowErrors = value;
            return this;
        }

        public UnityLogConsolePage SetShowWarnings(bool value)
        {
            ShowWarnings = value;
            return this;
        }

        public UnityLogConsolePage SetShowInfo(bool value)
        {
            ShowInfo = value;
            return this;
        }

        public UnityLogConsolePage SetOverlayVisibleByDefault(bool value)
        {
            _overlayVisible = value;
            return this;
        }

        #endregion
    }
}
