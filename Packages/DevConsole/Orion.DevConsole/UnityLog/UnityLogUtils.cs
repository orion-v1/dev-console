﻿using UnityEngine;

namespace Orion.DevConsole.UnityLog
{
    internal static class UnityLogUtils
    {
        public static string GetRichTextColorTag(LogType logType)
        {
            switch (logType)
            {
                case LogType.Warning:
                    return "<color=yellow>";

                case LogType.Error:
                case LogType.Exception:
                case LogType.Assert:
                    return "<color=red>";

                default: return null;
            };
        }

        public static string GetRecordTypeName(LogType logType)
        {
            switch (logType)
            {
                case LogType.Log: return "Info";
                case LogType.Warning: return "Warning";
                case LogType.Error: return "Error";
                case LogType.Exception: return "Exception";
                case LogType.Assert: return "Assert";
                default: return logType.ToString();
            }
        }

        public static string TruncateMessageIfRequired(string message)
        {
            const int lengthLimit = 60000 / 4;
            const string mark = "<truncated>";

            return message.Length > lengthLimit
                ? message.Substring(0, lengthLimit - mark.Length) + mark
                : message;
        }
    }
}
