﻿using Orion.DevConsole.UnityLog;

namespace Orion.DevConsole
{
    public static class UnityLogConsolePageExtensions
    {
        public static UnityLogConsolePage AddUnityLogPage(this IDevConsoleDesigner console)
        {
            var page = UnityLogConsolePage.Create();
            console.AddPage(page);
            console.AddInitializeAction(page.Init);
            return page;
        }
    }
}
