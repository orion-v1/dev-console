﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Orion.DevConsole.UnityLog
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    [RequireComponent(typeof(RectTransform))]
    internal class OverlayCounter : MonoBehaviour
    {
        private const float ShowDuration = .5f;

        [SerializeField] private RectTransform _indicator;
        [SerializeField] private RectTransform _content;
        [SerializeField] private Text _counter;

        private Vector3 _expandedPosition;
        private Coroutine _showCoroutine;


        public void Init()
        {
            _expandedPosition = _content.anchoredPosition;
            Clear();
        }

        private Vector2 CalculateCollapsedPosition()
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(_content);
            return new Vector2(
                -_content.rect.width,
                _expandedPosition.y);
        }

        public void Show(int value)
        {
            StopShowCoroutine();

            if (gameObject.activeInHierarchy)
                _showCoroutine = StartCoroutine(ShowCoroutine(value));
        }

        public void Clear()
        {
            StopShowCoroutine();

            _counter.text = string.Empty;
            _content.anchoredPosition = CalculateCollapsedPosition();
            _indicator.gameObject.SetActive(false);
            _content.gameObject.SetActive(false);
        }

        private IEnumerator ShowCoroutine(int value)
        {
            _indicator.gameObject.SetActive(value > 0);
            _content.gameObject.SetActive(true);
            _counter.text = value.ToString();
            _content.anchoredPosition = _expandedPosition;

            yield return new WaitForSecondsRealtime(1f);

            var collapsedPosition = CalculateCollapsedPosition();
            float ts = 0f;
            while (true)
            {
                ts = ts + Time.unscaledDeltaTime;
                var f = ts / ShowDuration;
                f = Mathf.Clamp01(1f - Mathf.Pow(2f, -10f * f));
                _content.anchoredPosition = Vector2.Lerp(_expandedPosition, collapsedPosition, f);

                if (f >= 1f) break;
                yield return null;
            }

            _content.anchoredPosition = collapsedPosition;
            _content.gameObject.SetActive(false);

            _showCoroutine = null;
        }

        private void StopShowCoroutine()
        {
            if (_showCoroutine == null) return;

            StopCoroutine(_showCoroutine);
            _showCoroutine = null;
        }
    }
}
