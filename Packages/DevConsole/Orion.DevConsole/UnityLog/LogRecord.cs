﻿using System;
using UnityEngine;

namespace Orion.DevConsole.UnityLog
{
    public struct LogRecord
    {
        public LogType LogType { get; }
        public DateTime Time { get; }
        public string Message { get; }
        public string StackTrace { get; }

        public LogRecord(LogType type, DateTime time, string message, string stackTrace)
        {
            LogType = type;
            Time = time;
            Message = message;
            StackTrace = stackTrace;
        }
    }
}
