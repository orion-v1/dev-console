﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Orion.DevConsole.UnityLog
{
    internal class LogRecordsCache
    {
        private const int Capacity = 256;

        private static LogRecordsCache _current;
        private Queue<LogRecord> _records = new Queue<LogRecord>(Capacity);
        private int _refCount;

        public static LogRecordsCache Current => _current;


        public LogRecordsCache()
        {
        }

        public void IncReference()
        {
            _refCount++;

            if (_refCount == 1)
                Application.logMessageReceived += OnMessageReceived;
        }

        public void DecReference()
        {
            if (_refCount <= 0) return;

            _refCount--;
            if (_refCount ==0)
            {
                Application.logMessageReceived -= OnMessageReceived;
                Clear();

                if (_current == this) _current = null;
            }
        }

        public bool TrySetAsCurrent()
        {
            if (_current != null) return false;

            _current = this;
            return true;
        }

        public bool TryDequeue(out LogRecord record)
        {
            if (_records.Count == 0)
            {
                record = default;
                return false;
            }

            record = _records.Dequeue();
            return true;
        }

        public void Clear()
        {
            _records.Clear();
        }

        private void OnMessageReceived(string condition, string stackTrace, LogType type)
        {
            var record = new LogRecord(type, DateTime.Now, condition, stackTrace);
            _records.Enqueue(record);
        }
    }
}
