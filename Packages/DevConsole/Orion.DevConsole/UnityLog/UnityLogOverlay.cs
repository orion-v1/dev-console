﻿using UnityEngine;

namespace Orion.DevConsole.UnityLog
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    internal class UnityLogOverlay : MonoBehaviour, IConsoleOverlayWidget
    {
        [SerializeField] private OverlayCounter _errorsCounter;
        [SerializeField] private OverlayCounter _warningsCounter;

        public RectTransform RectTransform => transform as RectTransform;
        public bool Visible { get => gameObject.activeSelf; set => gameObject.SetActive(value); }
        public OverlayCounter ErrorsCounter => _errorsCounter;
        public OverlayCounter WarningsCounter => _warningsCounter;


        public void Init()
        {
            _errorsCounter.Init();
            _warningsCounter.Init();
        }
    }
}
