﻿using System;
using UnityEngine;

namespace Orion.DevConsole
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    internal class DimensionsChangeHandler : MonoBehaviour
    {
        public event Action Changed;


        private void OnRectTransformDimensionsChange() => Changed?.Invoke();
    }
}
