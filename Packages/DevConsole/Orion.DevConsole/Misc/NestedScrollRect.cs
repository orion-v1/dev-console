﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Orion.DevConsole.UI
{
    public class NestedScrollRect : ScrollRect
    {
        [SerializeField] private Component[] _linkedComponents;

        private IReadOnlyList<IEventSystemHandler> _linkedHandlers;
        private bool _retranslated = false;


        public event Action<PointerEventData> InitializePotentialDrag;
        public event Action<PointerEventData> BeginDrag;
        public event Action<PointerEventData> Drag;
        public event Action<PointerEventData> EndDrag;

        public bool IsDragging { get; private set; }

        /// <summary>
        /// Always route initialize potential drag event to parents
        /// </summary>
        public override void OnInitializePotentialDrag(PointerEventData eventData)
        {
            RetranslateToLinkedComponent<IInitializePotentialDragHandler>(parent => parent.OnInitializePotentialDrag(eventData));
            base.OnInitializePotentialDrag(eventData);
            InitializePotentialDrag?.Invoke(eventData);
        }

        /// <summary>
        /// Drag event
        /// </summary>
        public override void OnDrag(PointerEventData eventData)
        {
            if (_retranslated)
                RetranslateToLinkedComponent<IDragHandler>(parent => parent.OnDrag(eventData));
            else
                base.OnDrag(eventData);

            Drag?.Invoke(eventData);
        }

        /// <summary>
        /// Begin drag event
        /// </summary>
        public override void OnBeginDrag(PointerEventData eventData)
        {
            IsDragging = true;
            Vector2 absDelta = new Vector2(Math.Abs(eventData.delta.x), Math.Abs(eventData.delta.y));

            _retranslated =
                !horizontal && absDelta.x > absDelta.y ||
                !vertical && absDelta.x < absDelta.y;

            if (_retranslated)
                RetranslateToLinkedComponent<IBeginDragHandler>(parent => parent.OnBeginDrag(eventData));
            else
                base.OnBeginDrag(eventData);

            BeginDrag?.Invoke(eventData);
        }

        /// <summary>
        /// End drag event
        /// </summary>
        public override void OnEndDrag(PointerEventData eventData)
        {
            IsDragging = false;

            if (_retranslated)
                RetranslateToLinkedComponent<IEndDragHandler>(parent => parent.OnEndDrag(eventData));
            else
                base.OnEndDrag(eventData);

            _retranslated = false;
            EndDrag?.Invoke(eventData);
        }


        /// <summary>
        /// Do action for all parents
        /// </summary>
        private void RetranslateToLinkedComponent<T>(Action<T> action) where T : IEventSystemHandler
        {
            var components = GetLinkedComponents();
            if (components == null)
                return;

            for (int i = 0; i < components.Count; ++i)
                if (components[i] is T handler)
                    action(handler);
        }

        private IReadOnlyList<IEventSystemHandler> GetLinkedComponents()
        {
            if (_linkedHandlers == null)
            {
                bool rs =
                    TryUseLinkedComponents() ||
                    TrySearchForParentScroll();

                if (!rs) Debug.LogWarning("Failed to get LinkedComponents");
            }

            return _linkedHandlers;


            bool TryUseLinkedComponents()
            {
                if (_linkedComponents == null || _linkedComponents.Length == 0)
                    return false;

                List<IEventSystemHandler> handlers = GetEventSystemHandlers(_linkedComponents);
                if (handlers.Count == 0)
                {
                    Debug.LogWarning("LinkedComponents doesn't contain any component that implements interface IEventSystemHandler.");
                    return false;
                }

                _linkedHandlers = handlers;
                return true;
            }

            bool TrySearchForParentScroll()
            {
                Transform parent = transform.parent;
                while (parent != null)
                {
                    var scroll = parent.GetComponent<ScrollRect>();
                    if (scroll != null)
                    {
                        var components = scroll.transform.GetComponents<Component>();
                        var handlers = GetEventSystemHandlers(components);
                        handlers.Insert(0, scroll);
                        _linkedHandlers = handlers;
                        return true;
                    }

                    parent = parent.parent;
                }

                Debug.LogWarning("There is no ScrollRect conponent on parent objects");
                return false;
            }

            List<IEventSystemHandler> GetEventSystemHandlers(Component[] components)
            {
                if (components == null)
                    return new List<IEventSystemHandler>();

                var handlers = new List<IEventSystemHandler>(components.Length);
                for (int i = 0; i < components.Length; ++i)
                {
                    if (components[i] is IEventSystemHandler handler)
                        handlers.Add(handler);
                }

                return handlers;
            }
        }
    }
}