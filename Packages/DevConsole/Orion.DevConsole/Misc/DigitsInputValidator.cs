﻿namespace Orion.DevConsole
{
    public class DigitsInputValidator : ITextInputValidator
    {
        public static ITextInputValidator _integerValidator;
        public static ITextInputValidator IntegerValidator => _integerValidator ?? (_integerValidator = new DigitsInputValidator
        {
            AllowFloat = false
        });

        public static ITextInputValidator _floatValidator;
        public static ITextInputValidator FloatValidator => _floatValidator ?? (_floatValidator = new DigitsInputValidator
        {
            AllowFloat = true
        });


        public bool AllowFloat { get; set; }

        public char ValidateInput(string text, int charIndex, char c)
        {
            if (c >= '0' && c <= '9') return c;

            if (AllowFloat &&
                (c == '.' || c == ',') &&
                text.IndexOf('.') < 0 &&
                text.IndexOf(',') < 0)
                return c;
            
            return '\0';
        }
    }
}
