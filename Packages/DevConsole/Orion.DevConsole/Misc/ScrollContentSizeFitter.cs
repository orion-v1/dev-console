﻿using UnityEngine;
using UnityEngine.UI;

namespace Orion.DevConsole
{
    internal class ScrollContentSizeFitter : MonoBehaviour, ILayoutSelfController
    {
        [SerializeField] private RectTransform _viewport;

        private RectTransform _rectTransform;

        public RectTransform RectTransform => _rectTransform ? _rectTransform : (_rectTransform = transform as RectTransform);


        private void HandleSelfFittingAlongAxis(int axis)
        {
            // Set anchor max to same as anchor min along axis
            Vector2 anchorMax = RectTransform.anchorMax;
            anchorMax[axis] = RectTransform.anchorMin[axis];
            RectTransform.anchorMax = anchorMax;

            // Set size to min size
            Vector2 sizeDelta = RectTransform.sizeDelta;
            sizeDelta[axis] = LayoutUtility.GetPreferredSize(_rectTransform, axis);
            RectTransform.sizeDelta = Vector2.Max(_viewport.rect.size, sizeDelta);
        }

        public void SetLayoutHorizontal()
        {
            HandleSelfFittingAlongAxis(0);
        }

        public void SetLayoutVertical()
        {
            HandleSelfFittingAlongAxis(1);
        }
    }
}
