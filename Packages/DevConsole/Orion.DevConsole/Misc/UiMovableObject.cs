﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Orion.DevConsole
{
    [RequireComponent(typeof(RectTransform))]
    internal class UiMovableObject : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        [SerializeField] private RectTransform _transform;

        private Vector2 _offset;


        public event Action<UiMovableObject> EndDrag;


        public RectTransform RectTransform => _transform;

        public Vector2 Position { get => _transform.anchoredPosition; set => SetPosition(value); }

        public bool IsDragging { get; private set; }


        public void ResetTransformAnchors()
        {
            var pos = _transform.position;
            _transform.anchorMin = new Vector2(.5f, .5f);
            _transform.anchorMax = new Vector2(.5f, .5f);
            _transform.position = pos;

            ClampPosition();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            IsDragging = true;

            bool rs = ScreenToSurfacePoint(eventData, out var pos);

            _offset = rs 
                ? _transform.anchoredPosition - pos
                : Vector2.zero;

            eventData.Use();
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            IsDragging = false;
            eventData.Use();
            EndDrag?.Invoke(this);
        }

        public void OnDrag(PointerEventData eventData)
        {
            bool rs = ScreenToSurfacePoint(eventData, out var pos);
            if (rs) SetPosition(pos + _offset);
            eventData.Use();
        }


        private bool ScreenToSurfacePoint(PointerEventData eventData, out Vector2 point)
        {
            var dragArea = _transform.parent as RectTransform;

            return RectTransformUtility.ScreenPointToLocalPointInRectangle(
                dragArea,
                eventData.position,
                eventData.pressEventCamera,
                out point);
        }

        public void SetPosition(Vector2 position)
        {
            _transform.anchoredPosition = position;
            ClampPosition();
        }

        public void SetPosition(Vector2 position, Vector2 anchor)
        {
            var parent = _transform.parent as RectTransform;

            anchor.x -= .5f;
            anchor.y -= .5f;
            _transform.anchoredPosition = parent.rect.size * anchor + position;

            ClampPosition();
        }

        private void ClampPosition()
        {
            var dragArea = _transform.parent as RectTransform;
            Rect area = dragArea.rect;
            Bounds bounds = RectTransformUtility.CalculateRelativeRectTransformBounds(dragArea, _transform);

            Vector2 offset = Vector2.zero;
            if (bounds.max.x > area.xMax) offset.x += area.xMax - bounds.max.x;
            if (bounds.min.x < area.xMin) offset.x += area.xMin - bounds.min.x;
            if (bounds.max.y > area.yMax) offset.y += area.yMax - bounds.max.y;
            if (bounds.min.y < area.yMin) offset.y += area.yMin - bounds.min.y;

            _transform.anchoredPosition += offset;
        }

#if UNITY_EDITOR
        private void Reset()
        {
            _transform = GetComponent<RectTransform>();
        }
#endif
    }
}
