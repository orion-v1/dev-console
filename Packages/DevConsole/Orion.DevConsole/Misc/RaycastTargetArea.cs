﻿using Orion.DevConsole.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Orion.DevConsole
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    internal class RaycastTargetArea : MaskableGraphic
    {
        public override void SetMaterialDirty() { }
        public override void SetVerticesDirty() { }
    }
}
