﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orion.DevConsole.UI
{
    public partial class SystemInfoWindow
    {
        public class Section
        {
            public string Name { get; }

            public IList<Record> Records { get; }

            public Section(string name)
            {
                Name = name;
                Records = new List<Record>();
            }

            public int IndexOfRecord(string name)
            {
                for (int i = 0; i < Records.Count; i++)
                    if (string.Equals(Records[i].Name, name, StringComparison.OrdinalIgnoreCase))
                        return i;

                return -1;
            }

            public void AddRecord(Record record)
                => Records.Add(record);

            public void AddRecord(string name, string value)
                => Records.Add(new ConstantRecord(name, value));

            public bool RemoveRecord(string name)
            {
                int idx = IndexOfRecord(name);
                if (idx < 0) return false;

                Records.RemoveAt(idx);
                return true;
            }

            public void AddOrReplaceRecord(string name, Record record)
            {
                int idx = IndexOfRecord(name);
                if (idx < 0) AddRecord(record);
                else Records[idx] = record;
            }

            public void AddOrReplaceRecord(string name, string value)
                => AddOrReplaceRecord(name, new ConstantRecord(name, value));

            protected internal virtual void CreateView(IConsoleContainerWidget window)
            {
                if (window.Widgets.Count > 0)
                {
                    window.AddSpace(20f);
                    window.AddSplitter();
                }

                var layout = window.AddHorizontalLayout();
                layout.AddText(Name);
                layout.AddFlexibleSpace();
                layout.AddButton("Copy")
                    .SetFixedWidth(CopyButtonWidth)
                    .AddClickHandler(OnCopyClick);
            }

            protected internal virtual void WriteToReport(StringBuilder builder)
            {
                builder.AppendLine(Name);
                builder.AppendLine(new string('=', Name.Length));

                foreach (var record in Records)
                    record.WriteToReport(builder);
            }

            private void OnCopyClick(ConsoleButton button)
            {
                var sb = new StringBuilder(512);
                WriteToReport(sb);
                ConsoleUtils.Clipboard = sb.ToString();
            }
        }
    }
}
