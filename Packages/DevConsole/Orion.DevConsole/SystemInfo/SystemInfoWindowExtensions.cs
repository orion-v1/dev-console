﻿using Orion.DevConsole.UI;

namespace Orion.DevConsole
{
    public static class SystemInfoWindowExtensions
    {
        public static SystemInfoWindow AddSystemInfoWindow(this IConsolePage container)
        {
            var window = new SystemInfoWindow();
            container.AddWidget(window);
            return window;
        }
    }
}
