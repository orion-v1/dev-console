﻿using System.Text;
using UnityEngine;

namespace Orion.DevConsole.UI
{
    public partial class SystemInfoWindow
    {
        public abstract class Record
        {
            public const float NameWidth = 250f;

            public string Name { get; }
            public string Value { get; protected set; }


            public Record(string name)
            {
                Name = name;
            }

            protected internal virtual void CreateView(IConsoleContainerWidget window)
            {
                if (string.IsNullOrEmpty(Value)) return;

                var layout = window.AddHorizontalLayout()
                    .SetAlignment(TextAnchor.UpperLeft);

                AddPropertyNameView(layout);

                layout.AddText(Value);
            }

            protected internal virtual void WriteToReport(StringBuilder builder)
            {
                builder.Append(Name);
                builder.Append(": ");
                builder.AppendLine(Value);
            }


            protected void AddPropertyNameView(IConsoleContainerWidget layout)
            {
                layout.AddText(Name + ":")
                    .SetTextAlignment(TextAnchor.MiddleRight)
                    .SetColor(new Color32(0xBD, 0xBD, 0xFF, 0xFF))
                    .SetFixedWidth(NameWidth);
            }
        }
    }
}
