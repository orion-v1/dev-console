﻿using System.Collections.Generic;
using System.Globalization;
using System.Text;
using UnityEngine;

namespace Orion.DevConsole.UI
{
    public partial class SystemInfoWindow : CustomConsoleWindow, IConsoleInitializeHandler
    {
        public const float CopyButtonWidth = 150f;
        public const string MiscSectionName = "Misc";

        private IList<Section> _sections;
        private Section _miscSection;

        public Section MiscSection => _miscSection ?? (_miscSection = GetOrAddSection(MiscSectionName));


        public SystemInfoWindow()
        {
            Caption = "System Info";
            Spacing = 10;

            _sections = new List<Section>();

            var section = GetOrAddSection("Project");
            section.AddRecord("Version", Application.version);
            section.AddRecord("App id", Application.identifier);
            section.AddRecord("Unity version", Application.unityVersion);

            section = GetOrAddSection("Device");
            section.AddRecord("Name", SystemInfo.deviceName);
            section.AddRecord("Type", SystemInfo.deviceType.ToString());
            section.AddRecord("Model", SystemInfo.deviceModel);
            section.AddRecord("OS", SystemInfo.operatingSystem);
            section.AddRecord("RAM", SystemInfo.systemMemorySize.ToString());

            section = GetOrAddSection("CPU");
            section.AddRecord("Type", SystemInfo.processorType);
            section.AddRecord("Cores", SystemInfo.processorCount.ToString());
            section.AddRecord("Frequency", SystemInfo.processorFrequency.ToString());

            section = GetOrAddSection("GPU");
            section.AddRecord("Vendor", SystemInfo.graphicsDeviceVendor);
            section.AddRecord("Graphics API", SystemInfo.graphicsDeviceVersion);
            section.AddRecord("VRAM", SystemInfo.graphicsMemorySize.ToString());
            section.AddRecord("MT rendering", SystemInfo.graphicsMultiThreaded.ToString());
            section.AddRecord("Shader level", (SystemInfo.graphicsShaderLevel / 10f).ToString("N1", CultureInfo.InvariantCulture));

            section = GetOrAddSection(MiscSectionName);
            section.AddRecord(new AdvertisingIdentifierRecord());
        }

        public virtual void OnConsoleInitialize(IDevConsoleDesigner console)
        {
            // move misc section to end of list
            int lastIdx = _sections.Count - 1;
            int miscIdx = IndexOfSection(MiscSectionName);
            if (miscIdx >= 0 && miscIdx != lastIdx)
            {
                var section = _sections[miscIdx];
                _sections.RemoveAt(miscIdx);
                _sections.Add(section);
            }

            // create sections view
            foreach (var section in _sections)
            {
                section.CreateView(this);

                foreach (var record in section.Records)
                    record.CreateView(this);
            }

            // Footer
            this.AddSplitter();
            var layout = this.AddHorizontalLayout();
            layout.Alignment = TextAnchor.MiddleRight;
            layout.AddButton("Copy All")
                .SetFixedWidth(CopyButtonWidth)
                .Click += sender => ConsoleUtils.Clipboard = CreateReport();
        }

        private string CreateReport()
        {
            var report = new StringBuilder(512);

            bool firstSection = true;
            foreach (var section in _sections)
            {
                if (firstSection) firstSection = false;
                else report.AppendLine();

                section.WriteToReport(report);
            }

            return report.ToString();
        }

        #region Sections management

        public int IndexOfSection(string name)
        {
            for (int i = 0; i < _sections.Count; i++)
                if (_sections[i].Name == name)
                    return i;

            return -1;
        }

        public bool ContainsSection(string name) => IndexOfSection(name) >= 0;

        public Section GetSection(string name)
        {
            int idx = IndexOfSection(name);
            return idx >= 0 ? _sections[idx] : null;
        }

        public Section AddSection(string name)
        {
            var section = new Section(name);
            _sections.Add(section);
            return section;
        }

        public void AddSection(Section section)
        {
            _sections.Add(section);
        }

        public Section GetOrAddSection(string name)
            => GetSection(name) ?? AddSection(name);

        #endregion
    }
}
