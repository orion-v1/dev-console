﻿using UnityEngine;

namespace Orion.DevConsole.UI
{
    public partial class SystemInfoWindow
    {
        public class AdvertisingIdentifierRecord : Record
        {
            public AdvertisingIdentifierRecord() : base("IDFA")
            {
                Value = "N/A";
            }

            protected internal override void CreateView(IConsoleContainerWidget window)
            {
                var layout = window.AddHorizontalLayout();

                AddPropertyNameView(layout);

                var adv_id = layout.AddText("Requesting...")
                    .SetFlexibleWidth(1f);

                var button = layout.AddButton("Copy")
                    .SetInteractable(false)
                    .SetFixedWidth(CopyButtonWidth)
                    .AddClickHandler(_ => ConsoleUtils.Clipboard = adv_id.Text);

                bool rs = Application.RequestAdvertisingIdentifierAsync((id, _, error) =>
                {
                    if (!string.IsNullOrEmpty(id))
                    {
                        adv_id.Text = id;
                        button.Interactable = true;
                    }
                    else
                    {
                        Value = "Failed to request";
                        adv_id.Text = "Failed to request";
                    }
                });

                if (!rs)
                {
                    Value = "Not supported";
                    adv_id.Text = "Not supported";
                }
            }
        }
    }
}
