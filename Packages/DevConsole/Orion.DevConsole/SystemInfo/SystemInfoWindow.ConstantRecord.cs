﻿namespace Orion.DevConsole.UI
{
    public partial class SystemInfoWindow
    {
        public class ConstantRecord : Record
        {
            public ConstantRecord(string name, string value) : base(name)
            {
                Value = value;
            }
        }
    }
}
