﻿using System;

namespace Orion.DevConsole
{
    public static class DevConsoleModuleExtension
    {
        public static IAppHostBuilder AddDevConsole<T>(
            this IAppHostBuilder appHostBuilder,
            DevConsoleConfig config,
            Func<bool> predicate) 
            where T : DevConsole, new()
        {
            if (config == null) config = new DevConsoleConfig();

            return appHostBuilder.AddModule(new DevConsoleModule(typeof(T), config, predicate));
        }

        public static IAppHostBuilder AddDevConsole<T>(
            this IAppHostBuilder appHostBuilder,
            Action<DevConsoleConfig> action,
            Func<bool> predicate)
            where T : DevConsole, new()
        {
            var config = new DevConsoleConfig();
            action(config);

            return appHostBuilder.AddModule(new DevConsoleModule(typeof(T), config, predicate));
        }

        public static IAppHostBuilder AddDevConsole<T>(this IAppHostBuilder appHostBuilder) where T : DevConsole, new()
            => AddDevConsole<T>(appHostBuilder, new DevConsoleConfig(), null);

        public static IAppHostBuilder AddDevConsole<T>(this IAppHostBuilder appHostBuilder, DevConsoleConfig config) where T : DevConsole, new()
            => AddDevConsole<T>(appHostBuilder, config, null);

        public static IAppHostBuilder AddDevConsole<T>(this IAppHostBuilder appHostBuilder, Action<DevConsoleConfig> action) where T : DevConsole, new()
            => AddDevConsole<T>(appHostBuilder, action, null);

        public static IAppHostBuilder AddDevConsole<T>(this IAppHostBuilder appHostBuilder, Func<bool> predicate) where T : DevConsole, new()
            => AddDevConsole<T>(appHostBuilder, (DevConsoleConfig)null, predicate);
    }
}
