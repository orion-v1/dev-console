﻿using UnityEngine;
using UnityEngine.UI;

namespace Orion.DevConsole.UI
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    public class TextAreaView : BaseWidgetView
    {
        [SerializeField] private InputField _inputField;
        [SerializeField] private LayoutElement _layoutElement;

        public InputField InputField => _inputField;

        public LayoutElement LayoutElement => _layoutElement;

        public string Text { get => _inputField.text; set => _inputField.text = value; }

        public TextAnchor TextAlignment
        {
            get => _inputField.textComponent.alignment;
            set => _inputField.textComponent.alignment = value;
        }

        public static TextAreaView Create()
        {
            const string AssetPath = "Controls/TextAreaView";
            return InternalResources.CreateFromResource<TextAreaView>(AssetPath);
        }
    }
}
