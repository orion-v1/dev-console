﻿using UnityEngine;

namespace Orion.DevConsole.UI
{
    public class TextArea : IConsoleInteractableWidget, ILayoutWidthApi, ILayoutHeightApi
    {
        private TextAreaView _view;

        public RectTransform RectTransform => _view.RectTransform;

        public bool Visible { get => _view.Visible; set => _view.Visible = value; }

        public bool Interactable { get => _view.InputField.interactable; set => _view.InputField.interactable = value; }

        public float MinWidth { get => _view.LayoutElement.minWidth; set => _view.LayoutElement.minWidth = value; }

        public float PreferredWidth { get => _view.LayoutElement.preferredWidth; set => _view.LayoutElement.preferredWidth = value; }

        public float FlexibleWidth { get => _view.LayoutElement.flexibleWidth; set => _view.LayoutElement.flexibleWidth = value; }

        public float MinHeight { get => _view.LayoutElement.minHeight; set => _view.LayoutElement.minHeight = value; }

        public float PreferredHeight { get => _view.LayoutElement.preferredHeight; set => _view.LayoutElement.preferredHeight = value; }

        public float FlexibleHeight { get => _view.LayoutElement.flexibleHeight; set => _view.LayoutElement.flexibleHeight = value; }

        public string Text { get => _view.Text; set => _view.Text = value; }

        public TextAnchor TextAlignment { get => _view.TextAlignment; set => _view.TextAlignment = value; }


        public TextArea()
        {
            _view = TextAreaView.Create();
        }

        #region API Chaning

        public TextArea SetTextAlignment(TextAnchor alignment)
        {
            TextAlignment = alignment;
            return this;
        }

        public TextArea SetText(string text)
        {
            Text = text;
            return this;
        }

        #endregion
    }


    public static class TextAreaExtensions
    {
        public static TextArea AddTextArea(this IConsoleContainerWidget container)
        {
            var widget = new TextArea();
            container.AddWidget(widget);
            return widget;
        }

        public static TextArea AddTextArea(this IConsoleContainerWidget container, string text)
        {
            var widget = new TextArea();
            widget.Text = text;
            container.AddWidget(widget);
            return widget;
        }
    }
}
