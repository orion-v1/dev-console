﻿using System.Collections.Generic;
using UnityEngine;

namespace Orion.DevConsole.UI
{
    public abstract class BaseWidgetView : MonoBehaviour, IConsoleWidget
    {
        [SerializeField] private RectTransform _rectTransform;

        public RectTransform RectTransform => _rectTransform;

        public bool Visible
        {
            get => _rectTransform.gameObject.activeSelf;
            set => _rectTransform.gameObject.SetActive(value);
        }

        protected bool GetParentsAllowInteractions()
        {
            Transform t = transform;
            var interactable = true;
            var canvasGroups = new List<CanvasGroup>(8);

            while (t != null)
            {
                t.GetComponents(canvasGroups);
                bool shouldBreak = false;

                for (var i = 0; i < canvasGroups.Count; i++)
                {
                    var canvasGroup = canvasGroups[i];

                    if (!canvasGroup.interactable)
                    {
                        interactable = false;
                        shouldBreak = true;
                    }

                    if (canvasGroup.ignoreParentGroups)
                        shouldBreak = true;
                }

                if (shouldBreak) break;

                t = t.parent;
            }

            return interactable;
        }
    }
}
