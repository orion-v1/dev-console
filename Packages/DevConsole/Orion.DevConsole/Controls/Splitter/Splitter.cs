﻿using UnityEngine;

namespace Orion.DevConsole.UI
{
    public class Splitter : IConsoleWidget
    {
        private SplitterView _view;

        public RectTransform RectTransform => _view.RectTransform;

        public bool Visible { get => _view.Visible; set => _view.Visible = value; }


        public Splitter(LayoutOrientation orientation)
        {
            _view = SplitterView.Create();
            _view.Orientation = orientation;
        }
    }


    public static class SplitterExtensions
    {
        public static Splitter AddSplitter(this IConsoleContainerWidget container, LayoutOrientation orientation)
        {
            var widget = new Splitter(orientation);
            container.AddWidget(widget);
            return widget;
        }

        public static Splitter AddHorizontalSplitter(this IConsoleContainerWidget container)
            => AddSplitter(container, LayoutOrientation.Horizontal);

        public static Splitter AddVerticalSplitter(this IConsoleContainerWidget container)
            => AddSplitter(container, LayoutOrientation.Vertical);

        public static Splitter AddSplitter(this IConsoleContainerWidget container)
        {
            LayoutOrientation orientation = LayoutOrientation.Horizontal;

            if (container is ConsoleHorizontalLayout)
                orientation = LayoutOrientation.Vertical;

            return AddSplitter(container, orientation);
        }
    }
}
