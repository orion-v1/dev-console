﻿using UnityEngine;
using UnityEngine.UI;

namespace Orion.DevConsole.UI
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    public class SplitterView : BaseWidgetView
    {
        [SerializeField] private LayoutElement _layoutElement;

        private LayoutOrientation _orientation = LayoutOrientation.Horizontal;
        private float _lineWidth;


        public LayoutElement LayoutElement => _layoutElement;

        public LayoutOrientation Orientation { get => _orientation; set => SetOrientation(value); }


        public static SplitterView Create()
        {
            const string AssetPath = "Controls/SplitterView";
            return InternalResources.CreateFromResource<SplitterView>(AssetPath);
        }


        private void Awake()
        {
            _lineWidth = LayoutElement.preferredHeight;
        }

        private void SetOrientation(LayoutOrientation orientation)
        {
            if (_orientation == orientation) return;
            _orientation = orientation;

            switch (orientation)
            {
                case LayoutOrientation.Horizontal:
                    //RectTransform.anchorMin = new Vector2(0f, .5f);
                    //RectTransform.anchorMax = new Vector2(1f, .5f);
                    LayoutElement.preferredWidth = -1;
                    LayoutElement.preferredHeight = _lineWidth;
                    LayoutElement.flexibleWidth = 1;
                    LayoutElement.flexibleHeight = -1;
                    break;

                case LayoutOrientation.Vertical:
                    //RectTransform.anchorMin = new Vector2(.5f, 0f);
                    //RectTransform.anchorMax = new Vector2(.5f, 1f);
                    LayoutElement.preferredWidth = _lineWidth;
                    LayoutElement.preferredHeight = -1;
                    LayoutElement.flexibleWidth = -1;
                    LayoutElement.flexibleHeight = 1;
                    break;
            }
        }
    }
}
