﻿using UnityEngine;
using UnityEngine.UI;

namespace Orion.DevConsole.UI
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    public class ConsoleText : BaseWidgetView, ILayoutWidthApi, ILayoutHeightApi
    {
        [SerializeField] private Text _text;
        [SerializeField] private LayoutElement _layoutElement;

        private bool _enabled = true;
        private bool _enabledInHierarhy = true;
        private Color _color;

        public float MinWidth { get => _layoutElement.minWidth; set => _layoutElement.minWidth = value; }

        public float PreferredWidth { get => _layoutElement.preferredWidth; set => _layoutElement.preferredWidth = value; }

        public float FlexibleWidth { get => _layoutElement.flexibleWidth; set => _layoutElement.flexibleWidth = value; }

        public float MinHeight { get => _layoutElement.minHeight; set => _layoutElement.minHeight = value; }

        public float PreferredHeight { get => _layoutElement.preferredHeight; set => _layoutElement.preferredHeight = value; }

        public float FlexibleHeight { get => _layoutElement.flexibleHeight; set => _layoutElement.flexibleHeight = value; }

        public string Text { get => _text.text; set => _text.text = value; }

        public TextAnchor TextAlignment { get => _text.alignment; set => _text.alignment = value; }


        public bool Enabled
        {
            get => _enabled;
            set
            {
                _enabled = value;
                UpdateView();
            }
        }

        public Color Color
        {
            get => _color;
            set
            {
                _color = value;
                UpdateView();
            }
        }


        public static ConsoleText Create()
        {
            const string AssetPath = "Controls/ConsoleText";
            return InternalResources.CreateFromResource<ConsoleText>(AssetPath);
        }


        private void Awake()
        {
            _color = _text.color;
        }

        private void OnCanvasGroupChanged()
        {
            var interactable = GetParentsAllowInteractions();

            if (interactable != _enabledInHierarhy)
            {
                _enabledInHierarhy = interactable;
                UpdateView();
            }
        }


        private void UpdateView()
        {
            var color = _color;

            if (!_enabled || !_enabledInHierarhy) color.a *= .3f;

            _text.color = color;
        }

        #region API Chaning

        public ConsoleText SetTextAlignment(TextAnchor alignment)
        {
            TextAlignment = alignment;
            return this;
        }

        public ConsoleText SetColor(Color color)
        {
            Color = color;
            return this;
        }

        #endregion
    }


    public static class ConsoleTextExtensions
    {
        public static ConsoleText AddText(this IConsoleContainerWidget container)
        {
            var widget = ConsoleText.Create();
            container.AddWidget(widget);
            return widget;
        }

        public static ConsoleText AddText(this IConsoleContainerWidget container, string text)
        {
            var widget = ConsoleText.Create();
            widget.Text = text;
            container.AddWidget(widget);
            return widget;
        }

        public static ConsoleText AddLabelProperty(this IConsoleContainerWidget container, string title, string text) => container
            .AddPropertyLayout(title)
            .AddText(text);
    }
}
