﻿using UnityEngine;

namespace Orion.DevConsole.UI
{
    public class ConsolePropertyTitle : IConsoleWidget
    {
        public const float DefaultWidth = 420f;

        private ConsoleText _view;


        public RectTransform RectTransform => _view.RectTransform;

        public bool Visible { get => _view.Visible; set => _view.Visible = value; }

        public bool Enabled { get => _view.Enabled; set => _view.Enabled = value; }


        public ConsolePropertyTitle(string title)
        {
            _view = ConsoleText.Create();
            _view.Text = title;
            _view.MinWidth = 200f;
            _view.PreferredWidth = 420f;
            _view.FlexibleWidth = -1f;
            _view.TextAlignment = TextAnchor.UpperLeft;
        }
    }


    public static class ConsolePropertyTitleExtensions
    {
        public static ConsolePropertyTitle AddPropertyTitle(this IConsoleContainerWidget container, string title)
        {
            var widget = new ConsolePropertyTitle(title);
            container.AddWidget(widget);
            return widget;
        }
    }
}
