﻿using System.Collections.Generic;
using UnityEngine;

namespace Orion.DevConsole.UI
{
    public class ConsolePropertyLayout : IConsoleContainerWidget
    {
        private ConsoleHorizontalLayout _container;
        private ConsolePropertyTitle _title;
        private List<IConsoleWidget> _widgets = new List<IConsoleWidget>();
        private List<IConsoleInteractableWidget> _interactableWidgets;


        public RectTransform RectTransform => _container.RectTransform;

        public IReadOnlyList<IConsoleWidget> Widgets => _widgets;

        public bool Visible { get => _container.Visible; set => _container.Visible = value; }

        public bool Interactable { get => _container.Interactable; set => _container.Interactable = value; }


        public static ConsolePropertyLayout Create(string title)
            => new ConsolePropertyLayout(title);


        public ConsolePropertyLayout(string title)
        {
            _container = ConsoleHorizontalLayout.Create();
            _title = _container.AddPropertyTitle(title);

            _container.FrameUpdate += OnFrameUpdate;
        }

        public void AddWidget(IConsoleWidget widget)
        {
            _widgets.Add(widget);
            _container.AddWidget(widget);

            if (widget is IConsoleInteractableWidget iw)
            {
                if (_interactableWidgets == null)
                    _interactableWidgets = new List<IConsoleInteractableWidget>();

                _interactableWidgets.Add(iw);
            }
        }

        private void OnFrameUpdate()
        {
            if (_interactableWidgets == null || _interactableWidgets.Count == 0)
                return;

            bool interactable = false;
            for (int i = 0; i < _interactableWidgets.Count; i++)
                if (_interactableWidgets[i].Interactable)
                {
                    interactable = true;
                    break;
                }

            _title.Enabled = interactable;
        }
    }


    public static class ConsolePropertyLayoutExtensions
    {
        public static ConsolePropertyLayout AddPropertyLayout(this IConsoleContainerWidget container, string title)
        {
            var layout = new ConsolePropertyLayout(title);
            container.AddWidget(layout);

            return layout;
        }
    }
}
