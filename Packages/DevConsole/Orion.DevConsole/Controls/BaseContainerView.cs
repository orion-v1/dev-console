﻿using System.Collections.Generic;
using UnityEngine;

namespace Orion.DevConsole.UI
{
    public abstract class BaseContainerView : BaseWidgetView, IConsoleContainerWidget, IConsoleInteractableWidget
    {
        [SerializeField] private RectTransform _content;
        [SerializeField] private CanvasGroup _canvasGroup;

        private List<IConsoleWidget> _widgets = new List<IConsoleWidget>();

        public RectTransform Content => _content;

        public bool Interactable { get => _canvasGroup.interactable; set => _canvasGroup.interactable = value; }

        public IReadOnlyList<IConsoleWidget> Widgets => _widgets;


        public virtual void AddWidget(IConsoleWidget widget)
        {
            _widgets.Add(widget);

            widget.RectTransform.SetParent(_content);
            widget.RectTransform.localScale = Vector3.one;
        }
    }
}
