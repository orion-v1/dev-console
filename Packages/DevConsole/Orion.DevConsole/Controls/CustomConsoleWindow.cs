﻿using System.Collections.Generic;
using UnityEngine;

namespace Orion.DevConsole.UI
{
    public abstract class CustomConsoleWindow : IConsoleContainerWidget, ILayoutSpacingApi, ILayoutPaddingApi
    {
        private ConsoleWindow _window;


        public RectTransform RectTransform => _window.RectTransform;

        public string Caption { get => _window.Caption; set => _window.Caption = value; }

        public RectOffset Padding { get => _window.Padding; set => _window.Padding = value; }

        public float Spacing { get => _window.Spacing; set => _window.Spacing = value; }

        public bool Interactable { get => _window.Interactable; set => _window.Interactable = value; }

        public bool Visible { get => _window.Visible; set => _window.Visible = value; }

        public IReadOnlyList<IConsoleWidget> Widgets => _window.Widgets;


        public CustomConsoleWindow()
        {
            _window = ConsoleWindow.Create();
        }

        public void AddWidget(IConsoleWidget widget)
        {
            _window.AddWidget(widget);
        }
    }
}
