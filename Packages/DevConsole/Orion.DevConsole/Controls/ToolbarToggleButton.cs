﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Orion.DevConsole.UI
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    [RequireComponent(typeof(RectTransform))]
    public class ToolbarToggleButton : MonoBehaviour, IConsoleDashboardWidget
    {
        [SerializeField] private Button _button;
        [SerializeField] private Image _background;
        [SerializeField] private Image _icon;
        [SerializeField] private Text _topText;
        [SerializeField] private Text _bottomText;
        [SerializeField] private Color _colorOn;
        [SerializeField] private Color _colorOff;

        private bool _value;


        public event Action<ToolbarToggleButton> ValueChanged;

        public RectTransform RectTransform => transform as RectTransform;

        public bool Visible { get => gameObject.activeSelf; set => gameObject.SetActive(value); }

        public bool Value { get => _value; set => SetValue(value, false); }

        public string TopText
        {
            get => _topText.text;
            set
            {
                _topText.text = value;
                _topText.gameObject.SetActive(!string.IsNullOrEmpty(value));
            }
        }

        public string BottomText
        {
            get => _bottomText.text;
            set
            {
                _bottomText.text = value;
                _bottomText.gameObject.SetActive(!string.IsNullOrEmpty(value));
           }
        }

        public Sprite Icon
        {
            get => _icon.sprite;
            set
            {
                _icon.sprite = value;
                _icon.gameObject.SetActive(value);
            }
        }


        private void Awake()
        {
            _button.onClick.AddListener(OnButtonClicked);
            _topText.gameObject.SetActive(!string.IsNullOrEmpty(_topText.text));
            _bottomText.gameObject.SetActive(!string.IsNullOrEmpty(_bottomText.text));
            _icon.gameObject.SetActive(_icon.sprite);
            UpdateView();
        }

        private void OnButtonClicked()
        {
            SetValue(!_value, true);
        }

        private void SetValue(bool value, bool raiseEvent)
        {
            if (value == _value) return;

            _value = value;
            UpdateView();

            if (raiseEvent) ValueChanged?.Invoke(this);
        }

        private void UpdateView()
        {
            _background.color = _value ? _colorOn : _colorOff;
        }


        #region API Chaining

        public ToolbarToggleButton SetValue(bool value)
        {
            SetValue(value, false);
            return this;
        }

        public ToolbarToggleButton SetIcon(Sprite icon)
        {
            Icon = icon;
            return this;
        }

        public ToolbarToggleButton SetTopText(string value)
        {
            TopText = value;
            return this;
        }

        public ToolbarToggleButton SetBottomText(string value)
        {
            BottomText = value;
            return this;
        }

        #endregion
    }
}
