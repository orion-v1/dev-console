﻿using UnityEngine;
using UnityEngine.UI;

namespace Orion.DevConsole.UI
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    public class SliderView : BaseWidgetView
    {
        [SerializeField] private Slider _slider;
        [SerializeField] private LayoutElement _layoutElement;
        [SerializeField] private Text _text;
        private float _valueViewWidth = 80f;
        private bool _valueViewVisible = true;


        public Slider Slider => _slider;

        public LayoutElement LayoutElement => _layoutElement;

        public Text Text => _text;

        public float ValueViewWidth
        {
            get => _valueViewWidth;
            set
            {
                _valueViewWidth = value;
                UpdateValueView();
            }
        }

        public bool ValueViewVisible
        {
            get => _valueViewVisible;
            set
            {
                _valueViewVisible = value;
                UpdateValueView();
            }
        }


        public static SliderView Create()
        {
            const string AssetPath = "Controls/SliderView";
            return InternalResources.CreateFromResource<SliderView>(AssetPath);
        }


        private void Awake()
        {
            _valueViewWidth = _text.rectTransform.rect.width;
            //_slider.onValueChanged.AddListener(OnValueChanged);
        }

        private void UpdateValueView()
        {
            float width = _valueViewVisible ? _valueViewWidth : 0f;

            var rt = _slider.transform as RectTransform;
            rt.offsetMax = new Vector2(-width, 0f);

            _text.gameObject.SetActive(_valueViewVisible);
            _text.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, 0f, width);
        }

        //private void OnValueChanged(float value)
        //{
        //    _text.text = value.ToString("G");
        //}
    }
}
