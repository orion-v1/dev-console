﻿namespace Orion.DevConsole.UI
{
    public class IntSlider : SliderBase<IntSlider, int>
    {
        public IntSlider()
        {
            IntegersOnly = true;
        }

        protected override int ConvertFloatToValue(float f) => (int)f;

        protected override float ConvertValueToFloat(int value) => value;

        protected override string ConvertValueToString(int value) => value.ToString();
    }


    public static class IntSliderExtensions
    {
        public static IntSlider AddIntSlider(this IConsoleContainerWidget container)
        {
            var widget = new IntSlider();
            container.AddWidget(widget);
            return widget;
        }

        public static IntSlider AddIntSliderProperty(this IConsoleContainerWidget container, string title) => container
            .AddPropertyLayout(title)
            .AddIntSlider();
    }
}
