﻿using System;
using UnityEngine;

namespace Orion.DevConsole.UI
{
    public abstract class SliderBase<TSelf, TValue> : IConsoleWidget, IConsoleInteractableWidget,
        ILayoutWidthApi, ILayoutHeightApi,
        IValueApi<TValue>, IValueChangedApi<TSelf>
        where TSelf : SliderBase<TSelf, TValue>
    {
        private SliderView _view;
        private TValue _value;
        private bool _dismissSliderEvents;


        public event Action<TSelf> ValueChanged;


        public RectTransform RectTransform => _view.RectTransform;

        public bool Visible { get => _view.Visible; set => _view.Visible = value; }

        public bool Interactable { get => _view.Slider.interactable; set => _view.Slider.interactable = value; }

        public float MinWidth { get => _view.LayoutElement.minWidth; set => _view.LayoutElement.minWidth = value; }

        public float PreferredWidth { get => _view.LayoutElement.preferredWidth; set => _view.LayoutElement.preferredWidth = value; }

        public float FlexibleWidth { get => _view.LayoutElement.flexibleWidth; set => _view.LayoutElement.flexibleWidth = value; }

        public float MinHeight { get => _view.LayoutElement.minHeight; set => _view.LayoutElement.minHeight = value; }

        public float PreferredHeight { get => _view.LayoutElement.preferredHeight; set => _view.LayoutElement.preferredHeight = value; }

        public float FlexibleHeight { get => _view.LayoutElement.flexibleHeight; set => _view.LayoutElement.flexibleHeight = value; }

        public TValue MinValue { get => ConvertFloatToValue(_view.Slider.minValue); set => _view.Slider.minValue = ConvertValueToFloat(value); }

        public TValue MaxValue { get => ConvertFloatToValue(_view.Slider.maxValue); set => _view.Slider.maxValue = ConvertValueToFloat(value); }

        public TValue Value { get => _value; set => SetValue(value, false); }

        public bool ValueViewVisible { get => _view.ValueViewVisible; set => _view.ValueViewVisible = value; }

        public float ValueViewWidth { get => _view.ValueViewWidth; set => _view.ValueViewWidth = value; }

        protected bool IntegersOnly { get => _view.Slider.wholeNumbers; set => _view.Slider.wholeNumbers = value; }


        public SliderBase()
        {
            _view = SliderView.Create();
            _view.Slider.onValueChanged.AddListener(OnValueChanged);

            _value = ConvertFloatToValue(_view.Slider.value);
        }

        private void OnValueChanged(float f)
        {
            if (_dismissSliderEvents) return;
            SetValue(ConvertFloatToValue(f), true);
        }

        private void SetValue(TValue value, bool raiseEvent)
        {
            _value = value;

            _dismissSliderEvents = true;
            _view.Slider.value = ConvertValueToFloat(value);
            _view.Text.text = ConvertValueToString(value);
            _dismissSliderEvents = false;

            if (raiseEvent) ValueChanged?.Invoke(this as TSelf);
        }

        protected abstract TValue ConvertFloatToValue(float f);

        protected abstract float ConvertValueToFloat(TValue value);

        protected abstract string ConvertValueToString(TValue value);


        #region API Chaining

        public TSelf SetValueViewVisible(bool value)
        {
            ValueViewVisible = value;
            return this as TSelf;
        }

        public TSelf SetValueViewWidth(float value)
        {
            ValueViewWidth = value;
            return this as TSelf;
        }

        public TSelf SetValue(TValue value)
        {
            SetValue(value, false);
            return this as TSelf;
        }

        public TSelf SetMinValue(TValue value)
        {
            MinValue = value;
            return this as TSelf;
        }

        public TSelf SetMaxValue(TValue value)
        {
            MaxValue = value;
            return this as TSelf;
        }

        #endregion
    }
}
