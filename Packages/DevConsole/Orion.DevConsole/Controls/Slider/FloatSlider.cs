﻿namespace Orion.DevConsole.UI
{
    public class FloatSlider : SliderBase<FloatSlider, float>
    {
        protected override float ConvertFloatToValue(float f) => f;

        protected override float ConvertValueToFloat(float value) => value;

        protected override string ConvertValueToString(float value) => value.ToString("G");
    }


    public static class FloatSliderExtensions
    {
        public static FloatSlider AddFloatSlider(this IConsoleContainerWidget container)
        {
            var widget = new FloatSlider();
            container.AddWidget(widget);
            return widget;
        }

        public static FloatSlider AddFloatSliderProperty(this IConsoleContainerWidget container, string title) => container
            .AddPropertyLayout(title)
            .AddFloatSlider();
    }
}
