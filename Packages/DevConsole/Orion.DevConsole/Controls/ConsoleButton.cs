﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Orion.DevConsole.UI
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    public class ConsoleButton : BaseWidgetView, IConsoleInteractableWidget, ILayoutWidthApi, ILayoutHeightApi
    {
        [SerializeField] private Button _button;
        [SerializeField] private Image _background;
        [SerializeField] private Text _caption;
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private LayoutElement _layoutElement;


        public event Action<ConsoleButton> Click;

        public string Text { get => _caption.text; set => _caption.text = value; }

        public float MinWidth { get => _layoutElement.minWidth; set => _layoutElement.minWidth = value; }

        public float PreferredWidth { get => _layoutElement.preferredWidth; set => _layoutElement.preferredWidth = value; }

        public float FlexibleWidth { get => _layoutElement.flexibleWidth; set => _layoutElement.flexibleWidth = value; }

        public float MinHeight { get => _layoutElement.minHeight; set => _layoutElement.minHeight = value; }

        public float PreferredHeight { get => _layoutElement.preferredHeight; set => _layoutElement.preferredHeight = value; }

        public float FlexibleHeight { get => _layoutElement.flexibleHeight; set => _layoutElement.flexibleHeight = value; }

        public Color BackgroundColor { get => _background.color; set => _background.color = value; }

        public Color TextColor { get => _caption.color; set => _caption.color = value; }

        public bool Interactable
        {
            get => _button.interactable;
            set
            {
                _button.interactable = value;
                _canvasGroup.interactable = value;
            }
        }

        public bool InteractableInHierarchy => _button.IsInteractable();


        public static ConsoleButton Create()
        {
            const string AssetPath = "Controls/ConsoleButton";
            return InternalResources.CreateFromResource<ConsoleButton>(AssetPath);
        }


        private void Awake()
        {
            _button.onClick.AddListener(Button_OnClick);
        }

        private void Button_OnClick()
        {
            Click?.Invoke(this);
        }


        #region API Chaining

        public ConsoleButton SetText(string text)
        {
            Text = text;
            return this;
        }

        public ConsoleButton SetBackgroundColor(Color color)
        {
            BackgroundColor = color;
            return this;
        }

        public ConsoleButton SetTextColor(Color color)
        {
            TextColor = color;
            return this;
        }

        public ConsoleButton AddClickHandler(Action<ConsoleButton> handler)
        {
            Click += handler;
            return this;
        }

        #endregion
    }


    public static class ConsoleButtonExtensions
    {

        public static ConsoleButton AddButton(this IConsoleContainerWidget container)
        {
            var widget = ConsoleButton.Create();
            container.AddWidget(widget);
            return widget;
        }

        public static ConsoleButton AddButton(this IConsoleContainerWidget container, string text)
        {
            var widget = AddButton(container);
            widget.Text = text;
            return widget;
        }
    }
}
