﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Orion.DevConsole.UI
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    public class ConsoleToggle : BaseWidgetView, IConsoleInteractableWidget, IPointerClickHandler,
        IValueApi<bool>, IValueChangedApi<ConsoleToggle>
    {
        private const float KnobShiftX = 20f;
        private const float KnobShiftY = -2f;

        [SerializeField] private Image _barImage;
        [SerializeField] private Image _knobImage;
        [SerializeField] private Color _enabledKnobColor;
        [SerializeField] private Color _enabledBarColor;
        [SerializeField] private Color _disabledKnobColor;
        [SerializeField] private Color _disabledBarColor;

        private bool _value;
        private bool _interactable = true;
        private bool _interactableInHierarhy = true;


        public event Action<ConsoleToggle> ValueChanged;

        public bool Value { get => _value; set => SetValue(value, false); }

        public bool Interactable
        {
            get => _interactable;
            set
            {
                _interactable = value;
                UpdateView();
            }
        }

        public bool InteractableInHierarhy => _interactable && _interactableInHierarhy;


        public static ConsoleToggle Create()
        {
            const string AssetPath = "Controls/ConsoleToggle";
            return InternalResources.CreateFromResource<ConsoleToggle>(AssetPath);
        }


        private void SetValue(bool value, bool raiseEvent)
        {
            if (value == _value) return;

            _value = value;
            UpdateView();

            if (raiseEvent) ValueChanged?.Invoke(this);
        }

        private void UpdateView()
        {
            Color barColor;
            Color knobColor;

            if (_value)
            {
                barColor = _enabledBarColor;
                knobColor = _enabledKnobColor;
                _knobImage.rectTransform.anchoredPosition = new Vector2(KnobShiftX, KnobShiftY);
            }
            else
            {
                barColor = _disabledBarColor;
                knobColor = _disabledKnobColor;
                _knobImage.rectTransform.anchoredPosition = new Vector2(-KnobShiftX, KnobShiftY);
            }

            if (!_interactable || !_interactableInHierarhy)
            {
                barColor.r = barColor.r * .6f;
                barColor.g = barColor.g * .6f;
                barColor.b = barColor.b * .6f;

                knobColor.r = knobColor.r * .5f;
                knobColor.g = knobColor.g * .5f;
                knobColor.b = knobColor.b * .5f;
            }

            _barImage.color = barColor;
            _knobImage.color = knobColor;
        }

        private void Start()
        {
            UpdateView();
        }

        protected void OnCanvasGroupChanged()
        {
            bool interactableParents = GetParentsAllowInteractions();
            if (interactableParents != _interactableInHierarhy)
            {
                _interactableInHierarhy = interactableParents;
                UpdateView();
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (InteractableInHierarhy)
                SetValue(!_value, true);
        }

        #region API Chaining

        public ConsoleToggle SetValue(bool value)
        {
            SetValue(value, false);
            return this;
        }

        #endregion
    }


    public static class ConsoleToggleExtensions
    {
        public static ConsoleToggle AddToggle(this IConsoleContainerWidget container)
        {
            var toggle = ConsoleToggle.Create();
            container.AddWidget(toggle);
            return toggle;
        }

        public static ConsoleToggle AddToggleProperty(this IConsoleContainerWidget container, string title)
        {
            var p = container.AddPropertyLayout(title);
            p.AddFlexibleSpace();
            return p.AddToggle();
        }
    }
}
