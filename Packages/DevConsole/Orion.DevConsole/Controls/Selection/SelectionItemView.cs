using System;
using UnityEngine;
using UnityEngine.UI;

namespace Orion.DevConsole.UI
{
    internal class SelectionItemView : MonoBehaviour
    {
        [SerializeField] private Image _background;
        [SerializeField] private Image _icon;
        [SerializeField] private Text _text;
        [SerializeField] private Button _button;
        [SerializeField] private Color _normalColor;
        [SerializeField] private Color _selectedColor;

        private bool _selected;

        public event Action<SelectionItemView> Clicked;


        public int Index { get; set; }

        public Sprite Icon { get => _icon.sprite; set { _icon.sprite = value; _icon.gameObject.SetActive(value != null); } }

        public string Text { get => _text.text; set => _text.text = value; }

        public bool Visible { get => gameObject.activeSelf; set => gameObject.SetActive(value); }

        public bool Selected
        {
            get => _selected;
            set
            {
                _selected = value;
                _background.color = _selected ? _selectedColor : _normalColor;
            }
        }

        private void Awake()
        {
            _button.onClick.AddListener(OnClicked);
        }

        private void OnDestroy()
        {
            _button.onClick.RemoveListener(OnClicked);
            Clicked = null;
        }

        private void OnClicked() => Clicked?.Invoke(this);
    }
}
