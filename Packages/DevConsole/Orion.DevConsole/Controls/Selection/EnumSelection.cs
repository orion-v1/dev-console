﻿using System;

namespace Orion.DevConsole.UI
{
    public class EnumSelection<T> : SelectionBase<EnumSelection<T>, T>,
        IConsoleOpeningHandler
        where T : Enum
    {
        protected override void FillItems() => AddAllEnumItems();

        public void OnConsoleOpening()
        {
            if (Items == null || Items.Count == 0)
                FillItems();
        }

        #region API Chaining

        public EnumSelection<T> AddAllEnumItems()
        {
            var values = Enum.GetValues(typeof(T)) as T[];
            foreach (var value in values) AddItem(value);
            return this;
        }

        public EnumSelection<T> RemoveItem(T item)
        {
            int idx = FindIndex(item);
            if (idx >= 0) Items.RemoveAt(idx);
            return this;
        }

        public EnumSelection<T> RemoveItems(params T[] items)
        {
            foreach (var item in items) RemoveItem(item);
            return this;
        }

        public EnumSelection<T> RemoveAllItems()
        {
            Items.Clear();
            return this;
        }

        #endregion
    }


    public static class EnumSelectionExtensions
    {
        public static EnumSelection<T> AddEnumSelection<T>(this IConsoleContainerWidget container)
            where T : Enum
        {
            var widget = new EnumSelection<T>();
            container.AddWidget(widget);
            return widget;
        }

        public static EnumSelection<T> AddEnumSelectionProperty<T>(this IConsoleContainerWidget container, string title)
            where T : Enum
        {
            var layout = container.AddPropertyLayout(title);
            return layout.AddEnumSelection<T>();
        }
    }
}
