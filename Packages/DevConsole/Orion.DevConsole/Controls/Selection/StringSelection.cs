﻿namespace Orion.DevConsole.UI
{
    public class StringSelection : SelectionBase<StringSelection, string>
    {
    }


    public static class StringSelectionExtensions
    {
        public static StringSelection AddStringSelection(this IConsoleContainerWidget container)
        {
            var widget = new StringSelection();
            container.AddWidget(widget);
            return widget;
        }

        public static StringSelection AddStringSelectionProperty(this IConsoleContainerWidget container, string title) => container
            .AddPropertyLayout(title)
            .AddStringSelection();
    }
}
