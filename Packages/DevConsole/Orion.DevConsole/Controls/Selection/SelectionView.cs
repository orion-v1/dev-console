﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Orion.DevConsole.UI
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    internal class SelectionView : BaseWidgetView
    {
        [SerializeField] private Button _button;
        [SerializeField] private Text _text;
        [SerializeField] private LayoutElement _layoutElement;

        public event Action Clicked;

        public Button Button => _button;

        public LayoutElement LayoutElement => _layoutElement;

        public Text Text => _text;

        public bool Interactable { get => _button.interactable; set => _button.interactable = value; }


        public static SelectionView CreateView()
        {
            const string AssetPath = "Controls/SelectionView";
            return InternalResources.CreateFromResource<SelectionView>(AssetPath);
        }

        private void Awake()
        {
            _button.onClick.AddListener(OnShowSelectionClicked);
        }

        private void OnShowSelectionClicked() => Clicked?.Invoke();
    }
}
