﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

namespace Orion.DevConsole.UI
{
    internal class SelectionWindowView : MonoBehaviour
    {
        [SerializeField] private SelectionItemView _selectionItemTemplate;
        [SerializeField] private InputField _searchInputField;
        [SerializeField] private ConsoleButton _searchButton;
        [SerializeField] private ConsoleButton _closeButton;
        [SerializeField] private RectTransform _itemsRoot;
        [SerializeField] private ScrollRect _scroll;

        private Action<bool, int> _callback;
        private List<SelectionItemView> _items;
        private SelectionItemView _selection;


        public static SelectionWindowView CreateView()
        {
            const string AssetPath = "Controls/SelectionWindowView";
            return InternalResources.CreateFromResource<SelectionWindowView>(AssetPath);
        }

        private void Awake()
        {
            _closeButton.Click += OnCloseButtonClicked;
            _searchButton.Click += OnSearchButtonClicked;
            _searchInputField.onValueChanged.AddListener(OnSearchValueChanged);
        }

        private void OnDestroy()
        {
            StopSearch();
            _closeButton.Click -= OnCloseButtonClicked;
            _searchButton.Click -= OnSearchButtonClicked;
            _searchInputField.onValueChanged.RemoveListener(OnSearchValueChanged);
        }

        public void ShowModal(Action<bool, int> callback)
        {
            ConsoleUtils.MoveToDashboardTop(transform);

            LayoutRebuilder.ForceRebuildLayoutImmediate(_scroll.transform as RectTransform);

            // horizontal scrolling
            _scroll.horizontal = _scroll.content.rect.width > _scroll.viewport.rect.width;

            // focus on selected item
            if (_selection != null)
            {
                var y = (_selection.transform as RectTransform).anchoredPosition.y;
                var ch = _scroll.content.rect.height;
                var vh = _scroll.viewport.rect.height;
                var f = (Mathf.Abs(y) - vh * .5f) / (ch - vh);
                _scroll.verticalNormalizedPosition = Mathf.Clamp01(1f - f);
            }

            _callback = callback;
        }

        private void Close(bool success)
        {
            _callback?.Invoke(success, _selection?.Index ?? -1);
            _callback = null;
            Destroy(gameObject);
        }

        public void AddItem(Sprite icon, string text)
        {
            _items ??= new List<SelectionItemView>();

            var item = Instantiate(_selectionItemTemplate, _itemsRoot, false);
            item.Index = _items.Count;
            item.Icon = icon;
            item.Text = text;
            item.Clicked += OnItemClicked;

            _items.Add(item);
        }

        public void SetSelectedItemIndex(int index) => SetSelected(_items[index]);

        private void SetSelected(SelectionItemView item)
        {
            if (_selection != null)
                _selection.Selected = false;

            _selection = item;
            _selection.Selected = true;
        }

        private void OnItemClicked(SelectionItemView item)
        {
            SetSelected(item);
            Close(true);
        }

        private void OnCloseButtonClicked(ConsoleButton sender) => Close(false);

        private void OnSearchButtonClicked(ConsoleButton sender)
        {
            StopSearch();

            _searchPattern = _searchInputField.text;
            RestartSearch(_searchPattern, 0f);
        }

        private void OnSearchValueChanged(string value)
        {
            RestartSearch(value, .5f);
        }

        #region Search

        private string _searchPattern = null;
        private float _searchHoldtime = 0;
        private int _searchStage = 0;
        private Coroutine _searchCoroutine;

        private void RestartSearch(string pattern, float delay)
        {
            // stop coroutine if it's already running
            if (_searchStage > 0 && _searchCoroutine != null)
                StopSearch();

            // update variables
            _searchHoldtime = Time.realtimeSinceStartup + delay;
            _searchPattern = pattern;

            // restart coroutine if required
            if (_searchCoroutine == null)
                _searchCoroutine = StartCoroutine(SearchCoroutine());
        }

        private void StopSearch()
        {
            if (_searchCoroutine != null)
            {
                StopCoroutine(_searchCoroutine);
                _searchCoroutine = null;
                _searchStage = 0;
            }
        }

        private IEnumerator SearchCoroutine()
        {
            const int pageSize = 10;

            // waiting changes
            _searchStage = 0;
            while (Time.realtimeSinceStartup < _searchHoldtime)
                yield return null;

            // search
            _searchStage = 1;
            _scroll.content.gameObject.SetActive(false);
            _scroll.verticalNormalizedPosition = 1f;
            if (string.IsNullOrEmpty(_searchPattern))
                foreach (var item in _items)
                    item.Visible = true;
            else
                for (int i = 0; i < _items.Count; i++)
                {
                    var item = _items[i];
                    var rs = Regex.Match(item.Text, _searchPattern);
                    item.Visible = rs.Success;

                    if ((i + 1) % pageSize == 0) yield return null;
                }

            _scroll.content.gameObject.SetActive(true);

            // cleanup
            _searchCoroutine = null;
            _searchStage = 0;
        }

        #endregion
    }
}
