﻿namespace Orion.DevConsole.UI
{
    public class IntSelection : SelectionBase<IntSelection, int>
    {
    }


    public static class IntSelectionExtensions
    {
        public static IntSelection AddIntSelection(this IConsoleContainerWidget container)
        {
            var widget = new IntSelection();
            container.AddWidget(widget);
            return widget;
        }

        public static IntSelection AddIntSelectionProperty(this IConsoleContainerWidget container, string title) => container
            .AddPropertyLayout(title)
            .AddIntSelection();
    }
}
