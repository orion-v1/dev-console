﻿using UnityEngine;

namespace Orion.DevConsole.UI
{
    public readonly struct SelectionItem<T>
    {
        public Sprite Icon { get; }
        public string Text { get; }
        public T Value { get; }

        public SelectionItem(Sprite icon, string text, T value)
        {
            Icon = icon;
            Text = text;
            Value = value;
        }
    }
}
