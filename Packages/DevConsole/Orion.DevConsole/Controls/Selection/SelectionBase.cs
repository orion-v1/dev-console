﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Orion.DevConsole.UI
{
    public abstract class SelectionBase<TSelf, TValue> :
        IConsoleWidget, IConsoleInteractableWidget,
        IValueApi<TValue>, IValueChangedApi<TSelf>,
        ILayoutWidthApi, ILayoutHeightApi
        where TSelf : SelectionBase<TSelf, TValue>
    {
        private SelectionView _view;
        private TValue _value;
        private List<SelectionItem<TValue>> _items;
        private int _selectedItemIndex = -1;

        public event Action<TSelf> ValueChanged;


        public RectTransform RectTransform => _view.RectTransform;

        public bool Visible { get => _view.Visible; set => _view.Visible = value; }

        public bool Interactable { get => _view.Interactable; set => _view.Interactable = value; }

        public float MinWidth { get => _view.LayoutElement.minWidth; set => _view.LayoutElement.minWidth = value; }

        public float PreferredWidth { get => _view.LayoutElement.preferredWidth; set => _view.LayoutElement.preferredWidth = value; }

        public float FlexibleWidth { get => _view.LayoutElement.flexibleWidth; set => _view.LayoutElement.flexibleWidth = value; }

        public float MinHeight { get => _view.LayoutElement.minHeight; set => _view.LayoutElement.minHeight = value; }

        public float PreferredHeight { get => _view.LayoutElement.preferredHeight; set => _view.LayoutElement.preferredHeight = value; }

        public float FlexibleHeight { get => _view.LayoutElement.flexibleHeight; set => _view.LayoutElement.flexibleHeight = value; }

        public List<SelectionItem<TValue>> Items => _items;

        public int SelectedItemIndex
        {
            get => _selectedItemIndex;
            set => SetSelectedItemIndex(value, false, true);
        }

        public TValue Value
        {
            get => _value;
            set => SetValue(value, false, true);
        }

        public bool HasValue => _selectedItemIndex >= 0;

        protected virtual TValue UndefinedValue => default;

        protected virtual string UndefinedValueText => "<none>";


        public SelectionBase()
        {
            _view = SelectionView.CreateView();
            _view.Text.text = "<none>";
            _view.Clicked += OnClicked;

            _items = new List<SelectionItem<TValue>>();
        }

        protected virtual void FillItems() { }

        protected int FindIndex(TValue value) => _items.FindIndex(item => EqualityComparer<TValue>.Default.Equals(item.Value, value));

        private void OnClicked()
        {
            var window = SelectionWindowView.CreateView();

            foreach (var item in _items)
                window.AddItem(item.Icon, item.Text);

            if (_selectedItemIndex >= 0)
                window.SetSelectedItemIndex(_selectedItemIndex);

            window.ShowModal(OnSelectionResult);
        }

        private void OnSelectionResult(bool success, int selectedItemIndex)
        {
            if (success) SetSelectedItemIndex(selectedItemIndex, true, false);
        }

        private void SetSelectedItemIndex(int index, bool raiseValueChanged, bool initItemsIfRequired)
        {
            if (initItemsIfRequired && _items.Count == 0) FillItems();

            var oldIndex = _selectedItemIndex;
            if (index < 0)
            {
                _selectedItemIndex = -1;
                _value = UndefinedValue;
            }
            else if (index < _items.Count)
            {
                _selectedItemIndex = index;
                _value = _items[index].Value;
            }
            else throw new IndexOutOfRangeException();

            UpdateView(raiseValueChanged);

            if (raiseValueChanged && oldIndex != _selectedItemIndex)
                ValueChanged?.Invoke(this as TSelf);
        }

        private void SetValue(TValue value, bool raiseValueChanged, bool initItemsIfRequired)
        {
            if (initItemsIfRequired && _items.Count == 0) FillItems();

            var idx = FindIndex(value);
            SetSelectedItemIndex(idx, raiseValueChanged, false);
        }

        private void UpdateView(bool raiseValueChanged)
        {
            _view.Text.text = HasValue
                ? _items[_selectedItemIndex].Text
                : UndefinedValueText;
        }

        #region API Chaining

        public TSelf SetSelectedItemIndex(int index)
        {
            SelectedItemIndex = index;
            return this as TSelf;
        }

        public TSelf SetValue(TValue value)
        {
            Value = value;
            return this as TSelf;
        }

        public TSelf AddItem(TValue value, string text, Sprite icon)
        {
            _items.Add(new SelectionItem<TValue>(icon, text, value));
            return this as TSelf;
        }

        public TSelf AddItem(TValue value, string text) => AddItem(value, text, null);

        public TSelf AddItem(TValue value) => AddItem(value, value.ToString(), null);

        public TSelf AddItems(params TValue[] items)
        {
            foreach (var item in items) AddItem(item);
            return this as TSelf;
        }

        public TSelf AddItems(params (TValue Value, string Text)[] items)
        {
            foreach (var item in items) AddItem(item.Value, item.Text);
            return this as TSelf;
        }

        public TSelf AddItems(params (TValue Value, string Text, Sprite Icon)[] items)
        {
            foreach (var item in items) AddItem(item.Value, item.Text, item.Icon);
            return this as TSelf;
        }

        public TSelf AddItems(IEnumerable<TValue> items)
        {
            foreach (var item in items) AddItem(item);
            return this as TSelf;
        }

        public TSelf AddItems(IEnumerable<(TValue Value, string Text)> items)
        {
            foreach (var item in items) AddItem(item.Value, item.Text);
            return this as TSelf;
        }

        public TSelf AddItems(IEnumerable<(TValue Value, string Text, Sprite Icon)> items)
        {
            foreach (var item in items) AddItem(item.Value, item.Text, item.Icon);
            return this as TSelf;
        }

        #endregion
    }
}
