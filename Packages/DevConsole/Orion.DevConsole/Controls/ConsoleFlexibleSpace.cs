﻿using UnityEngine;

namespace Orion.DevConsole.UI
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    public class ConsoleFlexibleSpace : BaseWidgetView
    {
        public static ConsoleFlexibleSpace Create()
        {
            const string AssetPath = "Controls/ConsoleFlexibleSpace";
            return InternalResources.CreateFromResource<ConsoleFlexibleSpace>(AssetPath);
        }
    }


    public static class ConsoleFlexibleSpaceExtensions
    {
        public static ConsoleFlexibleSpace AddFlexibleSpace(this IConsoleContainerWidget container)
        {
            var widget = ConsoleFlexibleSpace.Create();
            container.AddWidget(widget);
            return widget;
        }
    }
}
