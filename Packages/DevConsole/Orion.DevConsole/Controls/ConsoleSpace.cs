﻿using UnityEngine;
using UnityEngine.UI;

namespace Orion.DevConsole.UI
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    public class ConsoleSpace : BaseWidgetView
    {
        public const float DefaultSpace = 10f;

        [SerializeField] private LayoutElement _layoutElement;

        private LayoutOrientation _orientation = LayoutOrientation.Horizontal;
        private float _space = DefaultSpace;

        public LayoutOrientation Orientation { get => _orientation; set => SetOrientation(value, true); }

        public float Space { get => _space; set => SetSpace(value, true); }


        public static ConsoleSpace Create()
        {
            const string AssetPath = "Controls/ConsoleSpace";
            return InternalResources.CreateFromResource<ConsoleSpace>(AssetPath);
        }

        internal void SetOrientation(LayoutOrientation orientation, bool updateView)
        {
            if (_orientation == orientation) return;
            _orientation = orientation;
            if (updateView) UpdateView();
        }

        internal void SetSpace(float space, bool updateView)
        {
            if (space < 0f) space = DefaultSpace;

            if (space == _space) return;
            _space = space;
            if (updateView) UpdateView();
        }

        internal void UpdateView()
        {
            switch (_orientation)
            {
                case LayoutOrientation.Horizontal:
                    _layoutElement.preferredWidth = -1;
                    _layoutElement.preferredHeight = _space;
                    _layoutElement.flexibleWidth = 1;
                    _layoutElement.flexibleHeight = -1;
                    break;

                case LayoutOrientation.Vertical:
                    _layoutElement.preferredWidth = _space;
                    _layoutElement.preferredHeight = -1;
                    _layoutElement.flexibleWidth = -1;
                    _layoutElement.flexibleHeight = 1;
                    break;
            }
        }
    }


    public static class ConsoleSpaceExtensions
    {
        public static ConsoleSpace AddSpace(this IConsoleContainerWidget container, float space = -1f)
        {
            LayoutOrientation orientation = LayoutOrientation.Horizontal;

            if (container is ConsoleHorizontalLayout)
                orientation = LayoutOrientation.Vertical;

            var widget = ConsoleSpace.Create();
            widget.SetOrientation(orientation, false);
            widget.SetSpace(space, false);
            widget.UpdateView();
            container.AddWidget(widget);
            return widget;
        }
    }
}
