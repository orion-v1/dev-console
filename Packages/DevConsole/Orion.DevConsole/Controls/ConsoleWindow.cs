﻿using UnityEngine;
using UnityEngine.UI;

namespace Orion.DevConsole.UI
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    public sealed class ConsoleWindow : BaseContainerView, ILayoutSpacingApi, ILayoutPaddingApi
    {
        [SerializeField] private GameObject _captionRoot;
        [SerializeField] private VerticalLayoutGroup _layout;
        [SerializeField] private Text _captionText;

        public RectOffset Padding { get => _layout.padding; set => _layout.padding = value; }

        public float Spacing { get => _layout.spacing; set => _layout.spacing = value; }


        public static ConsoleWindow Create()
        {
            const string AssetPath = "Controls/ConsoleWindow";
            return InternalResources.CreateFromResource<ConsoleWindow>(AssetPath);
        }

        public string Caption
        {
            get => _captionText.text;
            set
            {
                _captionText.text = value;
                _captionRoot.gameObject.SetActive(!string.IsNullOrEmpty(value));
            }
        }
    }


    public static class ConsoleWindowExtensions
    {
        public static ConsoleWindow AddWindow(this IConsolePage page, string caption = null)
        {
            var window = ConsoleWindow.Create();
            window.Caption = caption;
            page.AddWidget(window);
            return window;
        }
    }
}
