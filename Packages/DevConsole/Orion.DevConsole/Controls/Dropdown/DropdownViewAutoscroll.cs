﻿using UnityEngine;
using UnityEngine.UI;

namespace Orion.DevConsole.UI
{
    internal class DropdownViewAutoscroll : MonoBehaviour
    {
        [SerializeField] private DropdownView _view;
        [SerializeField] private Scrollbar _scrollbar;

        private void Start()
        {
            var dropdown = _view.Dropdown;
            if (!dropdown.IsActive() || !dropdown.IsInteractable()) return;

            int maxLines = _view.MaxLines;
            int count = dropdown.options.Count - maxLines;
            if (count < 0) return;

            var value = (float)(dropdown.value - (maxLines / 2)) / count;
            value = _scrollbar.direction == Scrollbar.Direction.TopToBottom ? value : 1f - value;
            _scrollbar.value = Mathf.Max(value, .0001f);
        }
    }
}
