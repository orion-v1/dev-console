﻿namespace Orion.DevConsole.UI
{
    public sealed class IntDropdown : DropdownBase<IntDropdown, IntDropdownItemsCollection, int>
    {
    }

    public class IntDropdownItemsCollection : DropdownItemsCollection<int>
    {
    }


    public static class IntDropdownExtensions
    {
        public static IntDropdown AddIntDropdown(this IConsoleContainerWidget container)
        {
            var widget = new IntDropdown();

            container.AddWidget(widget);
            return widget;
        }

        public static IntDropdown AddIntDropdownProperty(this IConsoleContainerWidget container, string title) => container
            .AddPropertyLayout(title)
            .AddIntDropdown();
    }
}
