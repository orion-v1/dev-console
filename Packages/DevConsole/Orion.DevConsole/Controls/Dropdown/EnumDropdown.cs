﻿using System;

namespace Orion.DevConsole.UI
{
    public sealed class EnumDropdown<T> : DropdownBase<EnumDropdown<T>, EnumDropdownItemsCollection<T>, T>,
        IConsoleOpeningHandler
        where T : Enum
    {
        protected override void FillItems()
        {
            base.FillItems();

            AddAllEnumItems();
            SelectedItemIndex = 0;
        }

        public void OnConsoleOpening()
        {
            if (Items == null || Items.Count == 0)
                FillItems();
        }

        #region API Chaining

        public EnumDropdown<T> AddAllEnumItems()
        {
            var values = Enum.GetValues(typeof(T)) as T[];

            foreach (var value in values)
                Items.Add(value);

            return this;
        }

        public EnumDropdown<T> RemoveItem(T item)
        {
            Items.Remove(item);
            return this;
        }

        public EnumDropdown<T> RemoveItems(params T[] items)
        {
            foreach (var item in items)
                Items.Remove(item);

            return this;
        }

        public EnumDropdown<T> RemoveAllItems()
        {
            Items.Clear();
            return this;
        }

        #endregion
    }

    public class EnumDropdownItemsCollection<T> : DropdownItemsCollection<T>
        where T : Enum
    {
    }


    public static class EnumDropdownExtensions
    {
        public static EnumDropdown<T> AddEnumDropdown<T>(this IConsoleContainerWidget container)
            where T : Enum
        {
            var widget = new EnumDropdown<T>();

            container.AddWidget(widget);
            return widget;
        }

        public static EnumDropdown<T> AddEnumDropdownProperty<T>(this IConsoleContainerWidget container, string title)
            where T : Enum
        {
            var layout = container.AddPropertyLayout(title);
            return layout.AddEnumDropdown<T>();
        }
    }
}
