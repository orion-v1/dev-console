﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

namespace Orion.DevConsole.UI
{
    public class DropdownItemsCollection<T> : IList<DropdownItem<T>>
    {
        private List<DropdownItem<T>> _items = new List<DropdownItem<T>>();
        private List<Dropdown.OptionData> _dropdownOptions = new List<Dropdown.OptionData>();

        internal List<Dropdown.OptionData> OptionsList => _dropdownOptions;

        public bool IsReadOnly => false;

        public int Count => _items.Count;


        public bool Contains(DropdownItem<T> item) => _items.Contains(item);

        public bool Contains(T value) => _items.Any(value.Equals);


        public int IndexOf(DropdownItem<T> item) => _items.IndexOf(item);

        public int IndexOf(T value) => _items.FindIndex(p => value.Equals(p.Value));


        public void Add(DropdownItem<T> item)
        {
            ValidateItem(item);

            _items.Add(item);
            _dropdownOptions.Add(item.DropdownOption);
        }

        public void Add(T value) => Add(new DropdownItem<T>
        {
            Value = value
        });

        public void Add(T value, string text) => Add(new DropdownItem<T>
        {
            Value = value,
            Text = text
        });


        public void Insert(int index, DropdownItem<T> item)
        {
            ValidateItem(item);

            _items.Insert(index, item);
            _dropdownOptions.Insert(index, item.DropdownOption);
        }

        public void Insert(int index, T value) => Insert(
            index, 
            new DropdownItem<T>
            {
                Value = value
            });

        public void Insert(int index, T value, string text) => Insert(
            index,
            new DropdownItem<T>
            {
                Value = value,
                Text = text
            });


        public bool Remove(DropdownItem<T> item)
        {
            int idx = IndexOf(item);
            if (idx < 0) return false;

            _items.RemoveAt(idx);
            _dropdownOptions.RemoveAt(idx);
            return true;
        }

        public bool Remove(T value)
        {
            int idx = IndexOf(value);
            if (idx < 0) return false;
            
            _items.RemoveAt(idx);
            _dropdownOptions.RemoveAt(idx);
            return true;
        }

        public void RemoveAt(int index)
        {
            _items.RemoveAt(index);
            _dropdownOptions.RemoveAt(index);
        }

        public void Clear()
        {
            _items.Clear();
            _dropdownOptions.Clear();
        }

        public void CopyTo(DropdownItem<T>[] array, int arrayIndex) 
            => _items.CopyTo(array, arrayIndex);

        public IEnumerator<DropdownItem<T>> GetEnumerator() 
            => _items.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public DropdownItem<T> this[int index]
        {
            get => _items[index];
            set
            {
                _items[index] = value;
                _dropdownOptions[index] = value.DropdownOption;
            }
        }


        private void ValidateItem(DropdownItem<T> item)
        {
            if (string.IsNullOrEmpty(item.Text))
                item.Text = GenerateItemText(item);
        }

        protected virtual string GenerateItemText(DropdownItem<T> item) => item.Value.ToString();
    }
}
