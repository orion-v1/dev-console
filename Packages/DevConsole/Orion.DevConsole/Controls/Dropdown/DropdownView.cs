﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Orion.DevConsole.UI
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    public class DropdownView : BaseWidgetView
    {
        [SerializeField] private Dropdown _dropdown;
        [SerializeField] private LayoutElement _layoutElement;

        private int _maxLines;

        public Dropdown Dropdown => _dropdown;

        public LayoutElement LayoutElement => _layoutElement;

        public string InitialValueText { get; set; }

        public int MaxLines
        {
            get => _maxLines;
            set
            {
                if (value == _maxLines) return;
                _maxLines = value;

                _dropdown.template.SetSizeWithCurrentAnchors(
                    RectTransform.Axis.Vertical,
                    _layoutElement.preferredHeight * value);
            }
        }

        public static DropdownView CreateView()
        {
            const string AssetPath = "Controls/DropdownView";
            return InternalResources.CreateFromResource<DropdownView>(AssetPath);
        }


        private IEnumerator Start()
        {
            yield return null;

            // hotfix that allows to set valid text on Start
            if (InitialValueText != null) _dropdown.captionText.text = InitialValueText;
        }
    }
}
