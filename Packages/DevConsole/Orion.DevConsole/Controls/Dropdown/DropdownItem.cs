﻿using UnityEngine.UI;

namespace Orion.DevConsole.UI
{
    public class DropdownItem<T>
    {
        internal Dropdown.OptionData DropdownOption { get; }

        public T Value { get; set; }

        public string Text
        {
            get => DropdownOption.text;
            set => DropdownOption.text = value;
        }


        public DropdownItem()
        {
            DropdownOption = new Dropdown.OptionData();
        }
    }
}
