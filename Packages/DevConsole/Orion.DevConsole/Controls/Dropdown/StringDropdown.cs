﻿namespace Orion.DevConsole.UI
{
    public sealed class StringDropdown : DropdownBase<StringDropdown, StringDropdownItemsCollection, string>
    {
    }

    public class StringDropdownItemsCollection : DropdownItemsCollection<string>
    {
    }


    public static class StringDropdownExtensions
    {
        public static StringDropdown AddStringDropdown(this IConsoleContainerWidget container)
        {
            var widget = new StringDropdown();

            container.AddWidget(widget);
            return widget;
        }

        public static StringDropdown AddStringDropdownProperty(this IConsoleContainerWidget container, string title)
        {
            var layout = container.AddPropertyLayout(title);
            return layout.AddStringDropdown();
        }
    }
}
