﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Orion.DevConsole.UI
{
    public abstract class DropdownBase<TSelf, TItemsCollection, TValue> :
        IConsoleWidget, IConsoleInteractableWidget,
        IValueApi<TValue>, IValueChangedApi<TSelf>,
        ILayoutWidthApi, ILayoutHeightApi
        where TSelf : DropdownBase<TSelf, TItemsCollection, TValue>
        where TItemsCollection : DropdownItemsCollection<TValue>, new()
    {
        private DropdownView _view;
        private TItemsCollection _items;
        private int _selectedItemIndex = -1;
        private TValue _value;
        private bool _dismissUiViewEvents;
        private int _maxLines;


        public event Action<TSelf> ValueChanged;


        public RectTransform RectTransform => _view.RectTransform;

        public bool Visible { get => _view.Visible; set => _view.Visible = value; }

        public bool Interactable { get => _view.Dropdown.interactable; set => _view.Dropdown.interactable = value; }

        public float MinWidth { get => _view.LayoutElement.minWidth; set => _view.LayoutElement.minWidth = value; }

        public float PreferredWidth { get => _view.LayoutElement.preferredWidth; set => _view.LayoutElement.preferredWidth = value; }

        public float FlexibleWidth { get => _view.LayoutElement.flexibleWidth; set => _view.LayoutElement.flexibleWidth = value; }

        public float MinHeight { get => _view.LayoutElement.minHeight; set => _view.LayoutElement.minHeight = value; }

        public float PreferredHeight { get => _view.LayoutElement.preferredHeight; set => _view.LayoutElement.preferredHeight = value; }

        public float FlexibleHeight { get => _view.LayoutElement.flexibleHeight; set => _view.LayoutElement.flexibleHeight = value; }

        public int MaxLines { get => _view.MaxLines; set => _view.MaxLines = value; }

        public TItemsCollection Items { get => _items; set => SetItemsCollection(value, false, false); }

        public int SelectedItemIndex
        {
            get => _selectedItemIndex;
            set => SetSelectedItemIndex(value, false, true);
        }

        public TValue Value
        {
            get => _value;
            set => SetValue(value, false, true);
        }

        public bool HasValue => _selectedItemIndex >= 0;

        protected virtual TValue UndefinedValue => default;

        protected virtual string UndefinedValueText => "<none>";


        public DropdownBase()
        {
            _view = DropdownView.CreateView();

            SetItemsCollection(new TItemsCollection(), false, false);

            _view.Dropdown.onValueChanged.AddListener(OnValueChanged);
            _view.MaxLines = 10;
        }

        protected virtual void FillItems() { }


        private void OnValueChanged(int value)
        {
            if (_dismissUiViewEvents) return;

            SetSelectedItemIndex(value, true, false);
        }

        private void SetSelectedItemIndex(int index, bool raiseValueChanged, bool initItemsIfRequired)
        {
            if (initItemsIfRequired && _items.Count == 0) FillItems();

            if (index < 0)
            {
                _selectedItemIndex = -1;
                _value = UndefinedValue;
            }
            else if (index < _items.Count)
            {
                _selectedItemIndex = index;
                _value = _items[index].Value;
            }
            else throw new IndexOutOfRangeException();

            UpdateView(raiseValueChanged);
        }

        private void SetValue(TValue value, bool raiseValueChanged, bool initItemsIfRequired)
        {
            if (initItemsIfRequired && _items.Count == 0) FillItems();

            var idx = _items.IndexOf(value);
            SetSelectedItemIndex(idx, raiseValueChanged, false);
        }

        private void SetItemsCollection(TItemsCollection items, bool raiseValueChanged, bool initItemsIfRequired)
        {
            _items = items;
            _view.Dropdown.options = _items.OptionsList;
            SetSelectedItemIndex(-1, raiseValueChanged, initItemsIfRequired);
        }

        private void UpdateView(bool raiseValueChanged)
        {
            _dismissUiViewEvents = true;
            _view.Dropdown.value = _selectedItemIndex;

            string text = HasValue
                ? _items[_selectedItemIndex].Text
                : UndefinedValueText;

            _view.InitialValueText = text; // hotfix that allows to set valid text on Start
            _view.Dropdown.captionText.text = text;
            _dismissUiViewEvents = false;

            if (raiseValueChanged) ValueChanged?.Invoke(this as TSelf);
        }

        #region API Chaining

        public TSelf SetMaxLines(int value)
        {
            MaxLines = value;
            return this as TSelf;
        }

        public TSelf SetSelectedItemIndex(int index)
        {
            SelectedItemIndex = index;
            return this as TSelf;
        }

        public TSelf SetValue(TValue value)
        {
            Value = value;
            return this as TSelf;
        }

        public TSelf AddItem(TValue value)
        {
            _items.Add(value);
            return this as TSelf;
        }

        public TSelf AddItem(TValue value, string text)
        {
            _items.Add(value, text);
            return this as TSelf;
        }

        public TSelf AddItems(params TValue[] items)
        {
            foreach (var item in items)
                _items.Add(item);

            return this as TSelf;
        }

        public TSelf AddItems(params (TValue Value, string Text)[] items)
        {
            foreach (var item in items)
                _items.Add(item.Value, item.Text);

            return this as TSelf;
        }

        public TSelf AddItems(IEnumerable<TValue> items)
        {
            foreach (var item in items)
                _items.Add(item);

            return this as TSelf;
        }

        public TSelf AddItems(IEnumerable<(TValue Value, string Text)> items)
        {
            foreach (var item in items)
                _items.Add(item.Value, item.Text);

            return this as TSelf;
        }

        #endregion
    }
}
