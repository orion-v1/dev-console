﻿using System;
using UnityEngine;

namespace Orion.DevConsole.UI
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    public class ConsoleHorizontalLayout : HorizontalOrVerticalLayout
    {
        internal event Action FrameUpdate;

        public static ConsoleHorizontalLayout Create()
        {
            const string AssetPath = "Controls/ConsoleHorizontalLayout";
            return InternalResources.CreateFromResource<ConsoleHorizontalLayout>(AssetPath);
        }

        private void LateUpdate()
        {
            FrameUpdate?.Invoke();
        }
    }


    public static class ConsoleHorizontalLayoutExtensions
    {
        public static ConsoleHorizontalLayout AddHorizontalLayout(this IConsoleContainerWidget container)
        {
            var widget = ConsoleHorizontalLayout.Create();
            container.AddWidget(widget);
            return widget;
        }
    }
}
