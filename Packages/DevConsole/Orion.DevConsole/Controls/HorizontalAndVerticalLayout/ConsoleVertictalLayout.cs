﻿using UnityEngine;

namespace Orion.DevConsole.UI
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    public class ConsoleVertictalLayout : HorizontalOrVerticalLayout
    {
        public static ConsoleVertictalLayout Create()
        {
            const string AssetPath = "Controls/ConsoleVerticalLayout";
            return InternalResources.CreateFromResource<ConsoleVertictalLayout>(AssetPath);
        }
    }


    public static class ConsoleVertictalLayoutExtensions
    {
        public static ConsoleVertictalLayout AddVerticalLayout(this IConsoleContainerWidget container)
        {
            var widget = ConsoleVertictalLayout.Create();
            container.AddWidget(widget);
            return widget;
        }
    }
}
