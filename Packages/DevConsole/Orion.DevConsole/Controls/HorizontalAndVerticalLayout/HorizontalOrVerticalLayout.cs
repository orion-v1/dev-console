﻿using UnityEngine;
using UnityEngine.UI;

namespace Orion.DevConsole.UI
{
    public abstract class HorizontalOrVerticalLayout : BaseContainerView, 
        ILayoutSpacingApi, ILayoutPaddingApi, ILayoutAlignmentApi, ILayoutChildControlApi
    {
        [SerializeField] private HorizontalOrVerticalLayoutGroup _layout;


        public RectOffset Padding { get => _layout.padding; set => _layout.padding = value; }

        public float Spacing { get => _layout.spacing; set => _layout.spacing = value; }

        public TextAnchor Alignment { get => _layout.childAlignment; set => _layout.childAlignment = value; }

        public bool ControlChildWidth { get => _layout.childControlWidth; set => _layout.childControlWidth = value; }

        public bool ControlChildHeight { get => _layout.childControlHeight; set => _layout.childControlHeight = value; }

        public bool ExpandChildWidth { get => _layout.childForceExpandWidth; set => _layout.childForceExpandWidth = value; }

        public bool ExpandChildHeight { get => _layout.childForceExpandHeight; set => _layout.childForceExpandHeight = value; }
    }
}
