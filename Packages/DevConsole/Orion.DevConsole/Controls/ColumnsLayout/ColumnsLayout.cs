﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Orion.DevConsole.UI
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    [RequireComponent(typeof(RectTransform))]
    internal class ColumnsLayout : LayoutGroup
    {
        [SerializeField, Min(1)] private int _columns = 2;
        [SerializeField] private float _spacing = 0f;
        [SerializeField] bool _controlChildWidth = true;
        [SerializeField] bool _controlChildHeight = true;
        [SerializeField] bool _expandChildWidth = true;
        [SerializeField] bool _expandChildHeigth = true;
        [SerializeField] ColumnsLayoutFillMode _fillMode = ColumnsLayoutFillMode.ByRows;

        private LayoutElementSize[] _columnsSize;


        public RectTransform RectTransform => transform as RectTransform;

        public int ColumnsCount
        {
            get => _columns;
            set
            {
                if (value < 1) value = 1;
                if (value == _columns) return;
                _columns = value;
                SetDirty();
            }
        }

        public int RowsCount { get; private set; }

        public float Spacing
        {
            get => _spacing;
            set
            {
                if (_spacing == value) return;
                _spacing = value;
                SetDirty();
            }
        }

        public bool ControlChildWidth
        {
            get => _controlChildWidth;
            set
            {
                if (_controlChildWidth == value) return;
                _controlChildWidth = value;
                SetDirty();
            }
        }

        public bool ControlChildHeight
        {
            get => _controlChildHeight;
            set
            {
                if (_controlChildHeight == value) return;
                _controlChildHeight = value;
                SetDirty();
            }
        }

        public bool ExpandChildWidth
        {
            get => _expandChildWidth;
            set
            {
                if (_expandChildWidth == value) return;
                _expandChildWidth = value;
                SetDirty();
            }
        }

        public bool ExpandChildHeight
        {
            get => _expandChildHeigth;
            set
            {
                if (_expandChildHeigth == value) return;
                _expandChildHeigth = value;
                SetDirty();
            }
        }

        public TextAnchor Alignment
        {
            get => childAlignment;
            set => childAlignment = value;
        }

        public ColumnsLayoutFillMode FillMode
        {
            get => _fillMode;
            set
            {
                if (_fillMode == value) return;
                _fillMode = value;
                SetDirty();
            }
        }


        #region UI Layout

        private int GetRowsCount()
        {
            int rowsCount = rectChildren.Count / _columns;
            if (rowsCount * _columns < rectChildren.Count) rowsCount++;
            return rowsCount;
        }

        private RectTransform GetChild(int col, int row)
        {
            int idx;

            switch (_fillMode)
            {
                case ColumnsLayoutFillMode.ByRows:
                    idx = row * ColumnsCount + col;
                    break;

                case ColumnsLayoutFillMode.ByColumns:
                    idx = col * RowsCount + row;
                    break;

                default:
                    throw new NotImplementedException(_fillMode.ToString());
            }

            return idx < rectChildren.Count
                ? rectChildren[idx]
                : null;
        }

        public override void CalculateLayoutInputHorizontal()
        {
            base.CalculateLayoutInputHorizontal();

            if (_columnsSize == null || _columnsSize.Length != ColumnsCount)
                _columnsSize = new LayoutElementSize[ColumnsCount];

            RowsCount = GetRowsCount();
            var selfSize = new LayoutElementSize();

            for (int col = 0; col < ColumnsCount; col++)
            {
                var colSize = new LayoutElementSize();

                for (int row = 0; row < RowsCount; row++)
                {
                    var child = GetChild(col, row);
                    if (!child) continue;

                    var size = GetChildSize(child, 0, _controlChildWidth, _expandChildWidth);

                    if (size.Min > colSize.Min) colSize.Min = size.Min;
                    if (size.Preferred > colSize.Preferred) colSize.Preferred = size.Preferred;
                    if (size.Flexible > colSize.Flexible) colSize.Flexible = size.Flexible;
                }

                _columnsSize[col] = colSize;
                selfSize.Min += colSize.Min;
                selfSize.Preferred += colSize.Preferred;
                selfSize.Flexible += colSize.Flexible;
            }

            float spaces = _spacing * Mathf.Max(0f, ColumnsCount - 1);
            selfSize.Min += spaces;
            selfSize.Preferred += spaces;
            //if (selfSize.Flexible >= 0f) selfSize.Flexible += spaces;

            selfSize.Preferred = Mathf.Max(selfSize.Min, selfSize.Preferred);
            SetLayoutInputForAxis(
                selfSize.Min + padding.horizontal,
                selfSize.Preferred + padding.horizontal,
                selfSize.Flexible,
                0);
        }

        public override void CalculateLayoutInputVertical()
        {
            var selfSize = new LayoutElementSize();

            for (int row = 0; row < RowsCount; row++)
            {
                var rowSize = new LayoutElementSize();

                for (int col = 0; col < ColumnsCount; col++)
                {
                    var child = GetChild(col, row);
                    if (!child) continue;

                    var size = GetChildSize(child, 1, _controlChildHeight, _expandChildHeigth);

                    if (size.Min > rowSize.Min) rowSize.Min = size.Min;
                    if (size.Preferred > rowSize.Preferred) rowSize.Preferred = size.Preferred;
                    if (size.Flexible > rowSize.Flexible) rowSize.Flexible = size.Flexible;
                }

                selfSize.Min += rowSize.Min;
                selfSize.Preferred += rowSize.Preferred;
                selfSize.Flexible += rowSize.Flexible;
            }

            float spaces = _spacing * Mathf.Max(0f, RowsCount - 1);
            selfSize.Min += spaces;
            selfSize.Preferred += spaces;
            //if (selfSize.Flexible >= 0f) selfSize.Flexible += spaces;

            selfSize.Preferred = Mathf.Max(selfSize.Min, selfSize.Preferred);
            SetLayoutInputForAxis(
                selfSize.Min + padding.vertical,
                selfSize.Preferred + padding.vertical,
                selfSize.Flexible,
                1);
        }

        public override void SetLayoutHorizontal()
        {
            (float f, float flexibleMultiplier) = GetLayoutParams(0);
            float offset = GetPositionOffset(0);
            float position = padding.left - offset;

            // set cells position and size
            for (int col = 0; col < ColumnsCount; col++)
            {
                // calculate column width
                var colSize = _columnsSize[col];
                var colWidth =
                    Mathf.Lerp(colSize.Min, colSize.Preferred, f) +
                    colSize.Flexible * flexibleMultiplier;

                // set cells position and size
                for (int row = 0; row < RowsCount; row++)
                {
                    var child = GetChild(col, row);
                    if (!child) continue;

                    var size = GetChildSize(child, 0, _controlChildWidth, _expandChildWidth);
                    var width = CalculateElementViewSize(size, colWidth, _controlChildWidth, _expandChildWidth);

                    SetChildAlongAxis(0, child, _controlChildWidth, position, width, colWidth);
                }

                position += colWidth + _spacing;
            }
        }

        public override void SetLayoutVertical()
        {
            (float f, float flexibleMultiplier) = GetLayoutParams(1);
            float offset = GetPositionOffset(1);
            float position = padding.top - offset;

            var rowItems = new (RectTransform Child, LayoutElementSize Size)[ColumnsCount];

            for (int row = 0; row < RowsCount; row++)
            {
                var rowSize = new LayoutElementSize();
                int columns = ColumnsCount;

                for (int col = 0; col < ColumnsCount; col++)
                {
                    var child = GetChild(col, row);
                    if (!child)
                    {
                        columns--;
                        continue;
                    }

                    var size = GetChildSize(child, 1, _controlChildHeight, _expandChildHeigth);

                    if (size.Min > rowSize.Min) rowSize.Min = size.Min;
                    if (size.Preferred > rowSize.Preferred) rowSize.Preferred = size.Preferred;
                    if (size.Flexible > rowSize.Flexible) rowSize.Flexible = size.Flexible;
                    rowItems[col] = (child, size);
                }

                float rowHeight =
                    Mathf.Lerp(rowSize.Min, rowSize.Preferred, f) +
                    rowSize.Flexible * flexibleMultiplier;

                // set cells position and size
                for (int col = 0; col < columns; col++)
                {
                    var item = rowItems[col];
                    var height = CalculateElementViewSize(item.Size, rowHeight, _controlChildHeight, _expandChildHeigth);
                    SetChildAlongAxis(1, item.Child, _controlChildHeight, position, height, rowHeight);
                }

                position += rowHeight + _spacing;
            }
        }

        #endregion

        #region UI Layout tools

        // All based on Unity UI source

        private float CalculateElementViewSize(LayoutElementSize childSize, float cellSize, bool controlSize, bool childForceExpand)
        {
            float max = (childSize.Flexible > 0f) ? cellSize : childSize.Preferred;

            if (childForceExpand && !controlSize)
                max = childSize.Preferred;

            return Mathf.Clamp(cellSize, childSize.Min, max);
        }

        private float GetPositionOffset(int axis)
        {
            var totalSize = GetTotalSize(axis);
            var frame = axis == 0 ? rectTransform.rect.width : rectTransform.rect.height;

            float size = Mathf.Clamp(frame, totalSize.Min, (totalSize.Flexible > 0f) ? frame : totalSize.Preferred);
            return Mathf.Min(0f, size - frame) * GetAlignmentOnAxis(axis);
        }

        private (float MinPreferredFactor, float FlexibleMultiplier) GetLayoutParams(int axis)
        {
            // calculate spacing
            float space = axis == 0
                ? padding.horizontal + _spacing * Mathf.Max(0f, ColumnsCount - 1)
                : padding.vertical + _spacing * Mathf.Max(0f, RowsCount - 1);

            float frame = rectTransform.rect.size[axis] - space;

            // calculate total size
            var totalSize = new LayoutElementSize();
            totalSize.Min = GetTotalMinSize(axis) - space;
            totalSize.Preferred = GetTotalPreferredSize(axis) - space;
            totalSize.Flexible = GetTotalFlexibleSize(axis);

            // calculate row size (taken from Unity UI)
            float flexibleMultiplier = 0f;
            float widthOverflow = frame - totalSize.Preferred;
            if (widthOverflow > 0f)
            {
                if (totalSize.Flexible > 0f)
                    flexibleMultiplier = widthOverflow / totalSize.Flexible;
            }

            float f = totalSize.Min != totalSize.Preferred
                ? Mathf.Clamp01((frame - totalSize.Min) / (totalSize.Preferred - totalSize.Min))
                : 0f;

            return (f, flexibleMultiplier);
        }

        private LayoutElementSize GetTotalSize(int axis)
        {
            var totalSize = new LayoutElementSize();
            totalSize.Min = GetTotalMinSize(axis);
            totalSize.Preferred = GetTotalPreferredSize(axis);
            totalSize.Flexible = GetTotalFlexibleSize(axis);

            return totalSize;
        }

        private LayoutElementSize GetChildSize(RectTransform child, int axis, bool controlSize, bool childForceExpand)
        {
            LayoutElementSize sizes = new LayoutElementSize();

            if (controlSize)
            {
                sizes.Min = LayoutUtility.GetMinSize(child, axis);
                sizes.Preferred = LayoutUtility.GetPreferredSize(child, axis);
                sizes.Flexible = LayoutUtility.GetFlexibleSize(child, axis);
            }
            else
            {
                sizes.Min = child.sizeDelta[axis];
                sizes.Preferred = sizes.Min;
                sizes.Flexible = 0f;
            }

            if (childForceExpand)
                sizes.Flexible = Mathf.Max(sizes.Flexible, 1f);

            return sizes;
        }

        private void SetChildAlongAxis(int axis, RectTransform child, bool controlSize, float offset, float childSize, float cellSize)
        {
            float alignmentOnAxis = GetAlignmentOnAxis(axis);
            offset += (cellSize - childSize) * alignmentOnAxis;

            if (controlSize) SetChildAlongAxis(child, axis, offset, childSize);
            else SetChildAlongAxis(child, axis, offset);

            //if (controlSize)
            //{
            //    SetChildAlongAxis(child, axis, offset, childSize);
            //}
            //else
            //{
            //    float alignmentOnAxis = GetAlignmentOnAxis(axis);
            //    float woffset = (childSize - child.sizeDelta[axis]) * alignmentOnAxis;
            //    SetChildAlongAxis(child, axis, offset + woffset);
            //}
        }

        #endregion


        private struct LayoutElementSize
        {
            public float Min;
            public float Preferred;
            public float Flexible;
        }
    }


    public enum ColumnsLayoutFillMode
    {
        ByRows,
        ByColumns,
    }
}
