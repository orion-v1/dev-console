﻿using UnityEngine;

namespace Orion.DevConsole.UI
{
    public class ConsoleColumnsLayout : BaseContainerView,
        ILayoutSpacingApi, ILayoutPaddingApi, ILayoutAlignmentApi, ILayoutChildControlApi
    {
        [SerializeField] private ColumnsLayout _layout;


        public RectOffset Padding { get => _layout.padding; set => _layout.padding = value; }

        public float Spacing { get => _layout.Spacing; set => _layout.Spacing = value; }

        public TextAnchor Alignment { get => _layout.childAlignment; set => _layout.childAlignment = value; }

        public bool ControlChildWidth { get => _layout.ControlChildWidth; set => _layout.ControlChildWidth = value; }

        public bool ControlChildHeight { get => _layout.ControlChildHeight; set => _layout.ControlChildHeight = value; }

        public bool ExpandChildWidth { get => _layout.ExpandChildWidth; set => _layout.ExpandChildWidth = value; }

        public bool ExpandChildHeight { get => _layout.ExpandChildHeight; set => _layout.ExpandChildHeight = value; }

        public int ColumnsCount { get => _layout.ColumnsCount; set => _layout.ColumnsCount = value; }

        public int RowsCount { get => _layout.RowsCount; }

        public ColumnsLayoutFillMode FillMode { get => _layout.FillMode; set => _layout.FillMode = value; }


        public static ConsoleColumnsLayout Create()
        {
            const string AssetPath = "Controls/ConsoleColumnsLayout";
            return InternalResources.CreateFromResource<ConsoleColumnsLayout>(AssetPath);
        }


        #region API Chaining

        public ConsoleColumnsLayout SetColumnsCount(int value)
        {
            _layout.ColumnsCount = value;
            return this;
        }

        public ConsoleColumnsLayout SetFillMode(ColumnsLayoutFillMode mode)
        {
            _layout.FillMode = mode;
            return this;
        }

        #endregion
    }


    public static class ConsoleColumnsLayoutExtensions
    {
        public static ConsoleColumnsLayout AddColumnsLayout(this IConsoleContainerWidget container)
        {
            var widget = ConsoleColumnsLayout.Create();
            container.AddWidget(widget);
            return widget;
        }
    }
}
