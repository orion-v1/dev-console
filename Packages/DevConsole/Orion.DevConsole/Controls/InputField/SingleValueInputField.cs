﻿using UnityEngine;

namespace Orion.DevConsole.UI
{
    public abstract class SingleValueInputField<TSelf, TValue> : InputFieldBase<TSelf, TValue>,
        IConsoleWidget, IConsoleInteractableWidget,
        ILayoutWidthApi, ILayoutHeightApi
        where TSelf : SingleValueInputField<TSelf, TValue>
    {
        private InputFieldView _view;
        private bool _valueModified;
        private bool _dismissInputFieldEvents;
        protected ITextInputValidator _textInputValidator;

        private protected InputFieldView View => _view;

        public RectTransform RectTransform => _view.RectTransform;

        public bool Visible { get => _view.Visible; set => _view.Visible = value; }

        public bool Interactable { get => _view.InputField.interactable; set => _view.InputField.interactable = value; }

        public float MinWidth { get => _view.LayoutElement.minWidth; set => _view.LayoutElement.minWidth = value; }

        public float PreferredWidth { get => _view.LayoutElement.preferredWidth; set => _view.LayoutElement.preferredWidth = value; }

        public float FlexibleWidth { get => _view.LayoutElement.flexibleWidth; set => _view.LayoutElement.flexibleWidth = value; }

        public float MinHeight { get => _view.LayoutElement.minHeight; set => _view.LayoutElement.minHeight = value; }

        public float PreferredHeight { get => _view.LayoutElement.preferredHeight; set => _view.LayoutElement.preferredHeight = value; }

        public float FlexibleHeight { get => _view.LayoutElement.flexibleHeight; set => _view.LayoutElement.flexibleHeight = value; }

        //public bool IsFocused => _view.IsFocused;


        public SingleValueInputField()
        {
            _view = InputFieldView.CreateView();
            _view.InputField.onValidateInput = OnValidateInput;
            _view.InputField.onValueChanged.AddListener(OnValueChanged);
            _view.InputField.onEndEdit.AddListener(OnEndEdit);

            SetValue(default(TValue), false);
        }

        private char OnValidateInput(string input, int charIndex, char addedChar)
            => _textInputValidator != null
                ? _textInputValidator.ValidateInput(input, charIndex, addedChar)
                : addedChar;

        private void OnValueChanged(string text)
        {
            if (_dismissInputFieldEvents) return;
            if (!TryConvertStringToValue(text, out var value)) return;

            _valueModified = true;
            SetValue(value, true);
        }

        private void OnEndEdit(string text)
        {
            if (_dismissInputFieldEvents) return;
            if (!_valueModified) return;

            _valueModified = false;
            RaiseEndEditEvent();
        }

        protected override void SetValue(TValue value, bool raiseEvent)
        {
            _dismissInputFieldEvents = true;
            _view.Text = ConvertValueToString(value);
            _dismissInputFieldEvents = false;

            base.SetValue(value, raiseEvent);
        }


        protected abstract bool TryConvertStringToValue(string s, out TValue value);

        protected abstract string ConvertValueToString(TValue value);
    }
}
