﻿using UnityEngine;

namespace Orion.DevConsole.UI
{
    public class Vector2InputField : MultipleValuesInputField<Vector2InputField, Vector2>
    {
        private bool _dismissInputFieldEvents;
        private FloatInputField _xInputField;
        private FloatInputField _yInputField;


        public Vector2InputField() : this(ConsoleHorizontalLayout.Create()) { }

        public Vector2InputField(string title) : this(ConsolePropertyLayout.Create(title)) { }

        private Vector2InputField(IConsoleContainerWidget layout) : base(layout)
        {
            // x
            layout.AddText("X:");
            _xInputField = layout.AddFloatInputField()
                .AddValueChangedHandler(OnXValueChanged)
                .AddEndEditHandler(OnEndEdit);

            // y
            layout.AddText("Y:");
            _yInputField = layout.AddFloatInputField()
                .AddValueChangedHandler(OnYValueChanged)
                .AddEndEditHandler(OnEndEdit);
        }


        private void OnXValueChanged(FloatInputField inputField)
        {
            if (_dismissInputFieldEvents) return;

            Vector2 v = Value;
            v.x = inputField.Value;
            SetValue(v, true);
        }

        private void OnYValueChanged(FloatInputField inputField)
        {
            if (_dismissInputFieldEvents) return;

            Vector2 v = Value;
            v.y = inputField.Value;
            SetValue(v, true);
        }

        private void OnEndEdit(FloatInputField inputField)
        {
            if (_dismissInputFieldEvents) return;
            //if (_xInputField.IsFocused || _yInputField.IsFocused) return;

            RaiseEndEditEvent();
        }


        protected override void SetValue(Vector2 value, bool raiseEvent)
        {
            _dismissInputFieldEvents = true;
            _xInputField.Value = value.x;
            _yInputField.Value = value.y;
            _dismissInputFieldEvents = false;

            base.SetValue(value, raiseEvent);
        }
    }


    public static class Vector2InputFieldExtensions
    {
        public static Vector2InputField AddVector2InputField(this IConsoleContainerWidget container)
        {
            var widget = new Vector2InputField();
            container.AddWidget(widget);
            return widget;
        }

        public static Vector2InputField AddVector2Property(this IConsoleContainerWidget container, string title)
        {
            var widget = new Vector2InputField(title);
            container.AddWidget(widget);
            return widget;
        }
    }
}
