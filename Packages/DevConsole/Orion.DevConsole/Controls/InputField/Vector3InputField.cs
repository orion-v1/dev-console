﻿using UnityEngine;

namespace Orion.DevConsole.UI
{
    public class Vector3InputField : MultipleValuesInputField<Vector3InputField, Vector3>
    {
        private bool _dismissInputFieldEvents;
        private FloatInputField _xInputField;
        private FloatInputField _yInputField;
        private FloatInputField _zInputField;


        public Vector3InputField() : this(ConsoleHorizontalLayout.Create()) { }

        public Vector3InputField(string title) : this(ConsolePropertyLayout.Create(title)) { }

        private Vector3InputField(IConsoleContainerWidget layout) : base(layout)
        {
            // x
            layout.AddText("X:");
            _xInputField = layout.AddFloatInputField()
                .AddValueChangedHandler(OnXValueChanged)
                .AddEndEditHandler(OnEndEdit);

            // y
            layout.AddText("Y:");
            _yInputField = layout.AddFloatInputField()
                .AddValueChangedHandler(OnYValueChanged)
                .AddEndEditHandler(OnEndEdit);

            // z
            layout.AddText("Z:");
            _zInputField = layout.AddFloatInputField()
                .AddValueChangedHandler(OnZValueChanged)
                .AddEndEditHandler(OnEndEdit);
        }


        private void OnXValueChanged(FloatInputField inputField)
        {
            if (_dismissInputFieldEvents) return;

            Vector3 v = Value;
            v.x = inputField.Value;
            SetValue(v, true);
        }

        private void OnYValueChanged(FloatInputField inputField)
        {
            if (_dismissInputFieldEvents) return;

            Vector3 v = Value;
            v.y = inputField.Value;
            SetValue(v, true);
        }

        private void OnZValueChanged(FloatInputField inputField)
        {
            if (_dismissInputFieldEvents) return;

            Vector3 v = Value;
            v.z = inputField.Value;
            SetValue(v, true);
        }

        private void OnEndEdit(FloatInputField inputField)
        {
            if (_dismissInputFieldEvents) return;
            //if (_xInputField.IsFocused || _yInputField.IsFocused || _zInputField.IsFocused) return;

            RaiseEndEditEvent();
        }


        protected override void SetValue(Vector3 value, bool raiseEvent)
        {
            _dismissInputFieldEvents = true;
            _xInputField.Value = value.x;
            _yInputField.Value = value.y;
            _zInputField.Value = value.z;
            _dismissInputFieldEvents = false;

            base.SetValue(value, raiseEvent);
        }
    }


    public static class Vector3InputFieldExtensions
    {
        public static Vector3InputField AddVector3InputField(this IConsoleContainerWidget container)
        {
            var widget = new Vector3InputField();
            container.AddWidget(widget);
            return widget;
        }

        public static Vector3InputField AddVector3Property(this IConsoleContainerWidget container, string title)
        {
            var widget = new Vector3InputField(title);
            container.AddWidget(widget);
            return widget;
        }
    }
}
