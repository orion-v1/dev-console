﻿using UnityEngine;
using static UnityEngine.UI.InputField;

namespace Orion.DevConsole.UI
{
    public sealed class StringInputField : SingleValueInputField<StringInputField, string>
    {
        public LineType LineType { get => View.LineType; set => View.LineType = value; }

        public TextAnchor TextAlignment { get => View.TextAlignment; set => View.TextAlignment = value; }

        public ITextInputValidator TextInputValidator
        {
            get => _textInputValidator;
            set => _textInputValidator = value;
        }

        protected override bool TryConvertStringToValue(string s, out string value)
        {
            value = s;
            return true;
        }

        protected override string ConvertValueToString(string value) => value;


        #region API Chaining

        public StringInputField SetTextInputValidator(ITextInputValidator validator)
        {
            TextInputValidator = validator;
            return this;
        }

        public StringInputField SetLineType(LineType value)
        {
            LineType = value;
            return this;
        }

        public StringInputField SetMultiline(bool value)
        {
            LineType = value ? LineType.MultiLineNewline : LineType.SingleLine;
            return this;
        }

        public StringInputField SetTextAlignment(TextAnchor value)
        {
            TextAlignment = value;
            return this;
        }

        #endregion
    }


    public static class StringInputFieldExtensions
    {
        public static StringInputField AddStringInputField(this IConsoleContainerWidget container)
        {
            var widget = new StringInputField();
            container.AddWidget(widget);
            return widget;
        }

        public static StringInputField AddStringProperty(this IConsoleContainerWidget container, string title)
        {
            var layout = container.AddPropertyLayout(title);
            return layout.AddStringInputField();
        }
    }
}
