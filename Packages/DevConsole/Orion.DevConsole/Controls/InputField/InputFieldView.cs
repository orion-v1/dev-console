﻿using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.UI.InputField;

namespace Orion.DevConsole.UI
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    public class InputFieldView : BaseWidgetView
    {
        [SerializeField] private InputField _inputField;
        [SerializeField] private LayoutElement _layoutElement;


        public InputField InputField => _inputField;

        public LayoutElement LayoutElement => _layoutElement;

        public bool IsFocused => _inputField.isFocused;

        public string Text { get => _inputField.text; set => _inputField.text = value; }

        public LineType LineType { get => _inputField.lineType; set => _inputField.lineType = value; }

        public TextAnchor TextAlignment
        {
            get => _inputField.textComponent.alignment;
            set
            {
                _inputField.textComponent.alignment = value;
                (_inputField.placeholder as Text).alignment = value;
            }
        }


        public static InputFieldView CreateView()
        {
            const string AssetPath = "Controls/InputFieldView";
            return InternalResources.CreateFromResource<InputFieldView>(AssetPath);
        }
    }
}
