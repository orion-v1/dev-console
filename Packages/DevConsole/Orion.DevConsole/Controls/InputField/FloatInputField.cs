﻿using System.Globalization;

namespace Orion.DevConsole.UI
{
    public sealed class FloatInputField : SingleValueInputField<FloatInputField, float>
    {
        public FloatInputField()
        {
            _textInputValidator = DigitsInputValidator.FloatValidator;
        }

        protected override bool TryConvertStringToValue(string s, out float value)
        {
            if (string.IsNullOrEmpty(s))
            {
                value = 0;
                return true;
            }

            s = s.Replace(',', '.');
            var rs = float.TryParse(s, NumberStyles.Float, CultureInfo.InvariantCulture, out value);

            return rs && s[s.Length - 1] != '.';
        }

        protected override string ConvertValueToString(float value)
            => value.ToString();
    }


    public static class FloatInputFieldExtensions
    {
        public static FloatInputField AddFloatInputField(this IConsoleContainerWidget container)
        {
            var widget = new FloatInputField();
            container.AddWidget(widget);
            return widget;
        }

        public static FloatInputField AddFloatProperty(this IConsoleContainerWidget container, string title)
        {
            var layout = container.AddPropertyLayout(title);
            return layout.AddFloatInputField();
        }
    }
}
