﻿using System;

namespace Orion.DevConsole.UI
{
    public abstract class InputFieldBase<TSelf, TValue> :
        IValueApi<TValue>, IValueChangedApi<TSelf>, IEndEditChangedApi<TSelf>
        where TSelf : InputFieldBase<TSelf, TValue>
    {
        public event Action<TSelf> ValueChanged;
        public event Action<TSelf> EndEdit;

        private TValue _value;
        public TValue Value { get => _value; set => SetValue(value, false); }


        protected virtual void SetValue(TValue value, bool raiseEvent)
        {
            _value = value;
            if (raiseEvent) RaiseValueChangedEvent();
        }

        protected void RaiseValueChangedEvent()
        {
            ValueChanged?.Invoke(this as TSelf);
        }

        protected void RaiseEndEditEvent()
        {
            EndEdit?.Invoke(this as TSelf);
        }

        #region API Chaining

        public TSelf SetValue(TValue value)
        {
            Value = value;
            return this as TSelf;
        }

        #endregion
    }
}
