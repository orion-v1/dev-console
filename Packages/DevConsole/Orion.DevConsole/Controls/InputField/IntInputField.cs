﻿namespace Orion.DevConsole.UI
{
    public sealed class IntInputField : SingleValueInputField<IntInputField, int>
    {
        public IntInputField()
        {
            _textInputValidator = DigitsInputValidator.IntegerValidator;
        }

        protected override bool TryConvertStringToValue(string s, out int value)
        {
            if (string.IsNullOrEmpty(s))
            {
                value = 0;
                return true;
            }

            return int.TryParse(s, out value);
        }

        protected override string ConvertValueToString(int value)
            => value.ToString();
    }


    public static class IntInputFieldExtensions
    {
        public static IntInputField AddIntInputField(this IConsoleContainerWidget container)
        {
            var widget = new IntInputField();
            container.AddWidget(widget);
            return widget;
        }

        public static IntInputField AddIntProperty(this IConsoleContainerWidget container, string title)
        {
            var layout = container.AddPropertyLayout(title);
            return layout.AddIntInputField();
        }
    }
}
