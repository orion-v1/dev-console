﻿using UnityEngine;

namespace Orion.DevConsole.UI
{
    public abstract class MultipleValuesInputField<TSelf, TValue> : InputFieldBase<TSelf, TValue>,
        IConsoleWidget, IConsoleInteractableWidget,
        ILayoutWidthApi, ILayoutHeightApi
        where TSelf : MultipleValuesInputField<TSelf, TValue>
    {
        private readonly IConsoleContainerWidget _layout;


        public RectTransform RectTransform => _layout.RectTransform;

        public bool Visible { get => _layout.Visible; set => _layout.Visible = value; }

        public bool Interactable { get => _layout.Interactable; set => _layout.Interactable = value; }

        public float MinWidth
        {
            get => (_layout as ILayoutWidthApi).MinWidth;
            set => (_layout as ILayoutWidthApi).MinWidth = value;
        }

        public float PreferredWidth
        {
            get => (_layout as ILayoutWidthApi).PreferredWidth;
            set => (_layout as ILayoutWidthApi).PreferredWidth = value;
        }

        public float FlexibleWidth
        {
            get => (_layout as ILayoutWidthApi).FlexibleWidth;
            set => (_layout as ILayoutWidthApi).FlexibleWidth = value;
        }

        public float MinHeight
        {
            get => (_layout as ILayoutHeightApi).MinHeight;
            set => (_layout as ILayoutHeightApi).MinHeight = value;
        }

        public float PreferredHeight
        {
            get => (_layout as ILayoutHeightApi).PreferredHeight;
            set => (_layout as ILayoutHeightApi).PreferredHeight = value;
        }

        public float FlexibleHeight
        {
            get => (_layout as ILayoutHeightApi).FlexibleHeight;
            set => (_layout as ILayoutHeightApi).FlexibleHeight = value;
        }


        public MultipleValuesInputField(IConsoleContainerWidget layout)
        {
            _layout = layout;
        }
    }
}
