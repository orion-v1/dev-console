﻿namespace Orion.DevConsole.UI
{
    public interface ILayoutWidthApi
    {
        float MinWidth { get; set; }
        float PreferredWidth { get; set; }
        float FlexibleWidth { get; set; }
    }


    public static class LayoutWidthApiExtensions
    {
        public static T SetMinWidth<T>(this T layout, float value)
            where T : ILayoutWidthApi
        {
            layout.MinWidth = value;
            return layout;
        }

        public static T SetPreferredWidth<T>(this T layout, float value)
            where T : ILayoutWidthApi
        {
            layout.PreferredWidth = value;
            return layout;
        }

        public static T SetFlexibleWidth<T>(this T layout, float value)
            where T : ILayoutWidthApi
        {
            layout.FlexibleWidth = value;
            return layout;
        }

        public static T SetFixedWidth<T>(this T layout, float value)
            where T : ILayoutWidthApi
        {
            layout.FlexibleWidth = -1;
            layout.MinWidth = value;
            layout.PreferredWidth = value;
            return layout;
        }
    }
}
