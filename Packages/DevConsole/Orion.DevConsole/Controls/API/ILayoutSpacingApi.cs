﻿namespace Orion.DevConsole.UI
{
    public interface ILayoutSpacingApi
    {
        float Spacing { get; set; }
    }


    public static class LayoutSpacingApiExtensions
    {
        public static T SetSpacing<T>(this T layout, float value)
            where T : ILayoutSpacingApi
        {
            layout.Spacing = value;
            return layout;
        }
    }
}
