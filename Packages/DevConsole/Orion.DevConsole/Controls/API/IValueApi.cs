﻿namespace Orion.DevConsole.UI
{
    public interface IValueApi<T>
    {
        T Value { get; set; }
    }

    //public static class ValueApiExtensions
    //{
    //    public static TWidget SetValue<TWidget, TValue>(this TWidget widget, TValue value)
    //        where TWidget : IValueApi<TValue>
    //    {
    //        widget.Value = value;
    //        return widget;
    //    }
    //}
}
