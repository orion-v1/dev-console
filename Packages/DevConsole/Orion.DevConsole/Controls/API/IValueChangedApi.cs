﻿using System;

namespace Orion.DevConsole.UI
{
    public interface IValueChangedApi<T>
    {
        event Action<T> ValueChanged;
    }


    public static class ValueChangedApiExtensions
    {
        public static T AddValueChangedHandler<T>(this T widget, Action<T> handler)
            where T: IValueChangedApi<T>
        {
            widget.ValueChanged += handler;
            return widget;
        }
    }
}
