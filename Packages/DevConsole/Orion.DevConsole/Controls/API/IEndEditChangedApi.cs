﻿using System;

namespace Orion.DevConsole.UI
{
    public interface IEndEditChangedApi<T>
    {
        event Action<T> EndEdit;
    }


    public static class EndEditChangedApiExtensions
    {
        public static T AddEndEditHandler<T>(this T widget, Action<T> handler)
            where T : IEndEditChangedApi<T>
        {
            widget.EndEdit += handler;
            return widget;
        }
    }
}
