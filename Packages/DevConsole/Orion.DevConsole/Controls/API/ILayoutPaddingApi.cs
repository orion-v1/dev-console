﻿using UnityEngine;

namespace Orion.DevConsole.UI
{
    public interface ILayoutPaddingApi
    {
        RectOffset Padding { get; set; }
    }


    public static class LayoutPaddingApiExtensions
    {
        public static T SetPadding<T>(this T layout, RectOffset value)
            where T : ILayoutPaddingApi
        {
            layout.Padding = value;
            return layout;
        }

        public static T SetTopPadding<T>(this T layout, int value)
            where T : ILayoutPaddingApi
        {
            layout.Padding.top = value;
            return layout;
        }

        public static T SetBottomPadding<T>(this T layout, int value)
            where T : ILayoutPaddingApi
        {
            layout.Padding.bottom = value;
            return layout;
        }

        public static T SetLeftPadding<T>(this T layout, int value)
            where T : ILayoutPaddingApi
        {
            layout.Padding.left = value;
            return layout;
        }

        public static T SetRightPadding<T>(this T layout, int value)
            where T : ILayoutPaddingApi
        {
            layout.Padding.right = value;
            return layout;
        }
    }
}
