﻿namespace Orion.DevConsole.UI
{
    public static class InteractableWidgetExtensions
    {
        public static T SetInteractable<T>(this T widget, bool value)
            where T : IConsoleInteractableWidget
        {
            widget.Interactable = value;
            return widget;
        }
    }
}
