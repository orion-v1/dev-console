﻿namespace Orion.DevConsole.UI
{
    public interface ILayoutHeightApi
    {
        float MinHeight { get; set; }
        float PreferredHeight { get; set; }
        float FlexibleHeight { get; set; }
    }


    public static class LayoutHeightApiExtensions
    {
        public static T SetMinHeight<T>(this T layout, float value)
            where T : ILayoutHeightApi
        {
            layout.MinHeight = value;
            return layout;
        }

        public static T SetPreferredHeight<T>(this T layout, float value)
            where T : ILayoutHeightApi
        {
            layout.PreferredHeight = value;
            return layout;
        }

        public static T SetFlexibleHeight<T>(this T layout, float value)
            where T : ILayoutHeightApi
        {
            layout.FlexibleHeight = value;
            return layout;
        }

        public static T SetFixedHeight<T>(this T layout, float value)
            where T : ILayoutHeightApi
        {
            layout.FlexibleHeight = -1;
            layout.MinHeight = value;
            layout.PreferredHeight = value;
            return layout;
        }
    }
}
