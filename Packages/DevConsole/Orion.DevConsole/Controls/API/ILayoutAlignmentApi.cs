﻿using UnityEngine;

namespace Orion.DevConsole.UI
{
    public interface ILayoutAlignmentApi
    {
        TextAnchor Alignment { get; set; }
    }


    public static class LayoutAlignmentApiExtensions
    {
        public static T SetAlignment<T>(this T layout, TextAnchor value)
            where T : ILayoutAlignmentApi
        {
            layout.Alignment = value;
            return layout;
        }
    }
}
