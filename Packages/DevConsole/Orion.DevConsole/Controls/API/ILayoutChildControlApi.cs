﻿namespace Orion.DevConsole.UI
{
    public interface ILayoutChildControlApi
    {
        bool ControlChildWidth { get; set; }
        bool ControlChildHeight { get; set; }
        bool ExpandChildWidth { get; set; }
        bool ExpandChildHeight { get; set; }
    }


    public static class LayoutChildControlApiExtensions
    {
        public static T SetControlChildWidth<T>(this T layout, bool value)
            where T : ILayoutChildControlApi
        {
            layout.ControlChildWidth = value;
            return layout;
        }

        public static T SetControlChildHeight<T>(this T layout, bool value)
            where T : ILayoutChildControlApi
        {
            layout.ControlChildHeight = value;
            return layout;
        }

        public static T SetExpandChildWidth<T>(this T layout, bool value)
            where T : ILayoutChildControlApi
        {
            layout.ExpandChildWidth = value;
            return layout;
        }

        public static T SetExpandChildHeight<T>(this T layout, bool value)
            where T : ILayoutChildControlApi
        {
            layout.ExpandChildHeight = value;
            return layout;
        }
    }
}
