﻿namespace Orion.DevConsole
{
    public interface IConsoleOverlayDesigner
    {
        void AddWidget(IConsoleOverlayWidget widget);
    }
}
