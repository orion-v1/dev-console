﻿using System.Collections.Generic;

namespace Orion.DevConsole
{
    public interface IConsoleContainerWidget : IConsoleWidget, IConsoleInteractableWidget
    {
        IReadOnlyList<IConsoleWidget> Widgets { get; }

        void AddWidget(IConsoleWidget widget);
    }
}
