﻿namespace Orion.DevConsole
{
    public interface IDevConsoleTrigger
    {
        void BindToDevConsole(IDevConsoleDesigner designer, IDevConsole console);
    }
}
