﻿using System;

namespace Orion.DevConsole
{
    public interface IDevConsoleDesigner
    {
        IConsoleDashboardDesigner Dashboard { get; }
        IConsoleOverlayDesigner Overlay { get; }

        void AddPage(IConsolePage page);
        void AddInitializeAction(Action<IDevConsoleDesigner> action);
    }


    [Obsolete("Use IDevConsoleDesigner instead")]
    public interface IDevConsolePagesContainer : IDevConsoleDesigner { }
}
