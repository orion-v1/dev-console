﻿namespace Orion.DevConsole
{
    public interface ITextInputValidator
    {
        char ValidateInput(string text, int charIndex, char c);
    }
}
