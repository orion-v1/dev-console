﻿using UnityEngine;

namespace Orion.DevConsole
{
    public interface IConsoleOverlayWidget
    {
        RectTransform RectTransform { get; }
        bool Visible { get; set; }
    }
}
