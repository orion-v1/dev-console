﻿namespace Orion.DevConsole.UI
{
    public enum LayoutOrientation
    {
        Horizontal,
        Vertical
    }
}
