﻿namespace Orion.DevConsole
{
    public interface IConsolePage : IConsoleContainerWidget
    {
        string Title { get; }
    }
}
