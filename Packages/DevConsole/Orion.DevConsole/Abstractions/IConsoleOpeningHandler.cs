﻿namespace Orion.DevConsole
{
    public interface IConsoleOpeningHandler
    {
        void OnConsoleOpening();
    }
}
