﻿using UnityEngine;

namespace Orion.DevConsole
{
    public interface IConsoleWidget
    {
        RectTransform RectTransform { get; }
        bool Visible { get; set; }
    }
}
