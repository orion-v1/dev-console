﻿using UnityEngine;

namespace Orion.DevConsole
{
    public interface IConsoleDashboardWidget
    {
        RectTransform RectTransform { get; }
        bool Visible { get; set; }
    }
}
