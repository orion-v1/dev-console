﻿namespace Orion.DevConsole
{
    public interface IConsoleInteractableWidget : IConsoleWidget
    {
        bool Interactable { get; set; }
    }
}
