﻿namespace Orion.DevConsole
{
    public interface IDevConsole
    {
        bool IsOpen { get; }

        void Open();
        void Close();
    }
}
