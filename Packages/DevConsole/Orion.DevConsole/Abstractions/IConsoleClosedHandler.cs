﻿namespace Orion.DevConsole
{
    public interface IConsoleClosedHandler
    {
        void OnConsoleClosed();
    }
}
