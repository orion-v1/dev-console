﻿namespace Orion.DevConsole
{
    public interface IConsoleDashboardDesigner
    {
        void AddToolbarWidget(IConsoleDashboardWidget widget);
    }
}
