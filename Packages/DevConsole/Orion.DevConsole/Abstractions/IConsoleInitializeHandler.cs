﻿namespace Orion.DevConsole
{
    public interface IConsoleInitializeHandler
    {
        void OnConsoleInitialize(IDevConsoleDesigner console);
    }
}
