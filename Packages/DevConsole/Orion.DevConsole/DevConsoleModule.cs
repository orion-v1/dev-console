﻿using Orion.DevConsole.UnityLog;
using Orion.Ioc;
using System;
using System.Collections.Generic;

namespace Orion.DevConsole
{
    internal class DevConsoleModule : IAppHostModule
    {
        private const string ServiceName = "Dev Console";

        private DevConsoleConfig _config;
        private Type _consoleType;
        private Func<bool> _predicate;
        private bool? _enabled;
        private bool _binded;

        public IEnumerable<AppHostLayer> Layers => AppHostLayerUtils.ToEnumerable(AppHostLayer.Core, AppHostLayer.Default);

        public DevConsoleModule(Type consoleType, DevConsoleConfig config, Func<bool> predicate)
        {
            _config = config;
            _consoleType = consoleType;
            _predicate = predicate;
        }

        private bool IsEnabled
        {
            get
            {
                if (!_enabled.HasValue)
                    _enabled = _predicate == null || _predicate();

                return _enabled.Value;
            }
        }

        public void AttachToAppHostBuilder(IAppHostBuilder appHostBuilder)
        {
            appHostBuilder.AddConfigureAction(AppHostLayer.Core, ServiceName, CreateRecordsCache);
            appHostBuilder.AddConfigureAction(AppHostLayer.Default, ServiceName, ConfigureConsole);
            appHostBuilder.AddRunAction(AppHostLayer.Default, ServiceName, RunConsole);
        }

        public void CreateRecordsCache(IAppHost appHost)
        {
            if (!IsEnabled) return;

            var logger = new LogRecordsCache();
            logger.TrySetAsCurrent();
            logger.IncReference();
        }

        public void ConfigureConsole(IAppHost appHost)
        {
            if (!IsEnabled) return;

            // bind to abstraction
            appHost.ServiceContainer.CreateService()
                .SetImplementationType(_consoleType)
                .BindTo<IDevConsole>(ServiceBindingOptions.Exclusive)
                .AsSingleton();

            _predicate = null;
            _binded = true;
        }

        public void RunConsole(IAppHost appHost)
        {
            if (_binded)
            {
                var console = appHost.ServiceContainer.GetService<IDevConsole>() as DevConsole;
                console.Init(_config);
            }

            LogRecordsCache.Current?.DecReference();
        }
    }
}
