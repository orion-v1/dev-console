﻿using Orion.DevConsole.UI;
using System.Collections;
using UnityEngine;

namespace Orion.DevConsole
{
    public static class ConsoleUtils
    {
        public const string HiddenComponentMenu = "";

        public static string Clipboard
        {
            get => GUIUtility.systemCopyBuffer;
            set => GUIUtility.systemCopyBuffer = value;
        }

        internal static ConsoleView ViewController { get; set; }


        public static ToolbarToggleButton CreateToolbarToggleButton(string text, Sprite icon)
        {
            var widget = UnityEngine.Object.Instantiate(InternalResources.Instance.DashboardToolbarToggle);
            widget.TopText = text;
            widget.Icon = icon;

            return widget;
        }

        public static Coroutine StartCoroutine(IEnumerator coroutine)
            => ViewController.StartCoroutine(coroutine); // TODO: needed a scheduler

        public static void MoveToDashboardTop(Transform transform)
        {
            transform.SetParent(ViewController.Dashboard.Frame, false);
            transform.SetAsLastSibling();
        }
    }
}
