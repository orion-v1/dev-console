﻿using System;

namespace Orion.DevConsole
{
    internal class DevConsoleDesigner : IDevConsoleDesigner
    {
        private readonly DevConsole _devConsole;
        private Action<IDevConsoleDesigner> _initializeAction;

        public IConsoleDashboardDesigner Dashboard => _devConsole.Dashboard;

        public IConsoleOverlayDesigner Overlay => _devConsole.Overlay;


        public DevConsoleDesigner(DevConsole console)
        {
            _devConsole = console;
        }

        internal void InvokeInitializeActions()
        {
            if (_initializeAction == null) return;

            int calls = 0;
            while (_initializeAction != null)
            {
                calls++;
                if (calls > 512)
                    throw new InvalidOperationException($"Invokation cycle detected. {_initializeAction}");

                var action = _initializeAction;
                _initializeAction = null;
                action.Invoke(this);
            }
        }

        public void AddInitializeAction(Action<IDevConsoleDesigner> action)
        {
            _initializeAction += action;
        }

        public void AddPage(IConsolePage page) => _devConsole.Dashboard.AddPage(page);
    }
}
