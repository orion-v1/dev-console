﻿using System.Runtime.CompilerServices;
using UnityEngine;

namespace Orion.DevConsole
{
    internal static class ConsoleStateData
    {
        private const string KeyPrefix = "Orion.DevConsole.";


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string GetFullQualifiedKey(string key)
            => string.Concat(KeyPrefix, key);

        public static bool GetBool(string key, bool defaultValue = default)
            => PlayerPrefs.GetInt(GetFullQualifiedKey(key), defaultValue ? 1 : 0) > 0;

        public static void SetBool(string key, bool value)
            => PlayerPrefs.SetInt(GetFullQualifiedKey(key), value ? 1 : 0);

        public static Vector2? GetVector2(string key)
        {
            key = GetFullQualifiedKey(key);
            string x_key = string.Concat(key, ".x");
            string y_key = string.Concat(key, ".y");

            if (PlayerPrefs.HasKey(x_key) && PlayerPrefs.HasKey(x_key))
                return new Vector2(PlayerPrefs.GetFloat(x_key), PlayerPrefs.GetFloat(y_key));

            return null;
        }

        public static Vector2 GetVector2(string key, Vector2 defaultValue = default)
            => GetVector2(key) ?? defaultValue;

        public static void SetVector2(string key, Vector2? value)
        {
            key = GetFullQualifiedKey(key);
            string x_key = string.Concat(key, ".x");
            string y_key = string.Concat(key, ".y");

            if (value.HasValue)
            {
                PlayerPrefs.SetFloat(x_key, value.Value.x);
                PlayerPrefs.SetFloat(y_key, value.Value.y);
            }
            else
            {
                PlayerPrefs.DeleteKey(x_key);
                PlayerPrefs.DeleteKey(y_key);
            }
        }
    }
}
