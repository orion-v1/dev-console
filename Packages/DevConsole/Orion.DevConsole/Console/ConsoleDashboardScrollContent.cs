﻿using UnityEngine;

namespace Orion.DevConsole.UI
{
    internal class ConsoleDashboardScrollContent : MonoBehaviour
    {
        [SerializeField] private ConsoleDashboardScroll _scroll;

        private void OnRectTransformDimensionsChange()
        {
            _scroll.UpdateGridDimensions();
            //Debug.Log($"Content: OnRectTransformDimensionsChange");
        }
    }
}
