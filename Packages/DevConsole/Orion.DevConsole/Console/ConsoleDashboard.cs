﻿using Orion.DevConsole.UI;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Orion.DevConsole
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    internal class ConsoleDashboard : MonoBehaviour, IConsoleDashboardDesigner
    {
        [SerializeField] private RectTransform _frame;
        [SerializeField] private RectTransform _toolbar;
        [SerializeField] private RectTransform _pagesRoot;
        [SerializeField] private ConsoleDashboardScroll _scroll;
        [SerializeField] private Button _pageLeftButton;
        [SerializeField] private Button _pageRightButton;
        [SerializeField] private Button _closeConsoleButton1;
        [SerializeField] private Button _closeConsoleButton2;

        private List<IConsolePage> _pages = new List<IConsolePage>();
        private int _selectedPageIndex;
        private bool _isFirstOpen = true;


        public event Action CloseConsoleTriggered;

        public IReadOnlyList<IConsolePage> Pages => _pages;

        public int SelectedPageIndex => _selectedPageIndex;

        public bool IsOpen { get; private set; } = false;

        internal RectTransform Frame => _frame;


        private void Awake()
        {
            _scroll.ContentPositionChanged += OnScrollContentPositionChanged;
            _pageLeftButton.onClick.AddListener(OnLeftPageClicked);
            _pageRightButton.onClick.AddListener(OnRightPageClicked);
            _closeConsoleButton1.onClick.AddListener(OnCloseConsoleButtonClicked);
            _closeConsoleButton2.onClick.AddListener(OnCloseConsoleButtonClicked);
        }


        public void Init(DevConsoleConfig config)
        {
        }

        public void Open()
        {
            bool multiplePages = _pages.Count > 1;
            _pageLeftButton.gameObject.SetActive(multiplePages);
            _pageRightButton.gameObject.SetActive(multiplePages);

            gameObject.SetActive(true);
            IsOpen = true;

            if (_isFirstOpen)
            {
                _isFirstOpen = false;

                //LayoutRebuilder.ForceRebuildLayoutImmediate(_pagesRoot); // sometimes it breaks layout. used ForceUpdateCanvases instead.
                Canvas.ForceUpdateCanvases();
                MoveToPage(_selectedPageIndex, false);
            }
        }

        public void Close()
        {
            IsOpen = false;
            gameObject.SetActive(false);
        }

        public void AddPage(IConsolePage page)
        {
            _pages.Add(page);

            page.RectTransform.SetParent(_pagesRoot, false);
            page.RectTransform.localScale = Vector3.one;
        }

        public void MoveToPage(int index, bool animate = true)
        {
            if (IsOpen)
            {
                var cell = new Vector2Int(index, 0);
                cell = _scroll.SnapToCell(cell, animate);
                if (cell.x < 0) return;
            }
            else _selectedPageIndex = index;
        }

        public void MoveToPage(IConsolePage page, bool animate = true)
        {
            int index = _pages.IndexOf(page);
            if (index >= 0) MoveToPage(index, animate);
        }

        public void AddToolbarWidget(IConsoleDashboardWidget widget)
        {
            widget.RectTransform.SetParent(_toolbar, false);
        }


        private void OnLeftPageClicked() => MoveToPage(_selectedPageIndex - 1);

        private void OnRightPageClicked() => MoveToPage(_selectedPageIndex + 1);

        private void OnCloseConsoleButtonClicked() => CloseConsoleTriggered?.Invoke();

        private void OnScrollContentPositionChanged()
        {
            var cell = _scroll.GetCellIndex(_scroll.ContentPosition);
            if (cell.x == _selectedPageIndex) return;

            _selectedPageIndex = cell.x;
        }

        //private void OnRectTransformDimensionsChange()
        //{
        //    if (!_frame) return; // unity hotfix

        //    Vector2 size = _rectTransform.rect.size;

        //    float aspectRatio = size.x / size.y;
        //    if (aspectRatio <= 9f / 16f)
        //    {
        //        _frame.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, 0, size.x);
        //        _frame.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0, size.y);
        //    }

        //    Debug.Log($"{size}, {aspectRatio}");
        //}
    }
}