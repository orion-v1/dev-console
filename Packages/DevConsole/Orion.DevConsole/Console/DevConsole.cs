﻿using System;
using UnityEngine;

namespace Orion.DevConsole
{
    public class DevConsole : IDevConsole
    {
        private ConsoleView _viewController;
        private bool _inited;

        public bool IsOpen => _viewController?.IsDashboardOpen ?? false;

        public bool IsOverlayVisible
        {
            get => _viewController?.IsOverlayVisible ?? false;
            set
            {
                if (_viewController != null) _viewController.IsOverlayVisible = value;
            }
        }

        internal ConsoleDashboard Dashboard => _viewController?.Dashboard;

        internal ConsoleOverlay Overlay => _viewController?.Overlay;


        internal void Init(DevConsoleConfig config)
        {
            _viewController = ConsoleView.Create();
            _viewController.ConsoleOpening += OnConsoleOpening;
            _viewController.ConsoleClosed += OnConsoleClosed;
            UnityEngine.Object.DontDestroyOnLoad(_viewController.gameObject);
            ConsoleUtils.ViewController = _viewController;

            var designer = new DevConsoleDesigner(this);
            _viewController.Init(config);
            OnInitialize(designer);
            _viewController.BroadcastConsoleInitialize(designer);

            // Console triggers
            if (config.ConsoleTriggers == null || config.ConsoleTriggers.Count == 0)
            {
                config.ConsoleTriggers.Add(new HotKeyTrigger(KeyCode.D, KeyModifiers.Ctrl));
                config.ConsoleTriggers.Add(new ThreeFingersTapTrigger());
                config.ConsoleTriggers.Add(new OverlayButtonTrigger());
            }

            foreach (var trigger in config.ConsoleTriggers)
                trigger.BindToDevConsole(designer, this);

            // invoke deferred initialize actions
            designer.InvokeInitializeActions();

            _inited = true;

            // disable picking in editor
#if UNITY_EDITOR
            var visibilityManager = UnityEditor.SceneVisibilityManager.instance;
            visibilityManager.DisablePicking(_viewController.Canvas.gameObject, false);
            visibilityManager.DisablePicking(_viewController.Overlay.gameObject, true);
#endif
        }

        protected virtual void OnInitialize(IDevConsoleDesigner console) { }

        protected virtual void OnConsoleOpening() { }

        protected virtual void OnConsoleClosed() { }


        public void Open()
        {
            if (!_inited) throw new InvalidOperationException("Dev console is not inited yet.");
            _viewController?.Open();
        }

        public void Close()
        {
            if (!_inited) throw new InvalidOperationException("Dev console is not inited yet.");
            _viewController?.Close();
        }

        public void MoveToPage(int index) => _viewController?.MoveToPage(index, IsOpen);

        public void MoveToPage(IConsolePage page) => _viewController?.MoveToPage(page, IsOpen);
    }
}
