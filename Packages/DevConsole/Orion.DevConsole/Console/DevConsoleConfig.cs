﻿using System;
using System.Collections.Generic;

namespace Orion.DevConsole
{
    public class DevConsoleConfig
    {
        private CanvasSettings _canvas;
        private List<IDevConsoleTrigger> _consoleTriggers;

        /// <summary>
        /// Gets or sets whether the FPS meter should be visible by default.
        /// </summary>
        [Obsolete("Use console.AddFpsMeterOverlay() instead.")]
        public bool ShowFpsMonitor { get; set; } = true;

        /// <summary>
        /// Gets or sets whether the console button should be visible by default.
        /// </summary>
        [Obsolete("Use OverlayButtonTrigger instead.")]
        public bool ShowConsoleButton { get; set; } = true;

        /// <summary>
        /// Canvas settings
        /// </summary>
        public ref CanvasSettings Canvas => ref _canvas;

        /// <summary>
        /// Gets a collection of console triggers allows you to add more triggers.
        /// </summary>
        public List<IDevConsoleTrigger> ConsoleTriggers => _consoleTriggers;


        public DevConsoleConfig()
        {
            Canvas.SortingOrder = 10;

            _consoleTriggers = new List<IDevConsoleTrigger>();
        }


        public struct CanvasSettings
        {
            public int SortingOrder { get; set; }
        }
    }
}
