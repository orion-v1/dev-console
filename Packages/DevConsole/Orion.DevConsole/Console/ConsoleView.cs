﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Orion.DevConsole
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    internal class ConsoleView : MonoBehaviour
    {
        [SerializeField] private EventSystem _eventSystem;
        [SerializeField] private Canvas _canvas;
        [SerializeField] private ConsoleOverlay _overlay;
        [SerializeField] private ConsoleDashboard _dashboard;


        public event Action ConsoleOpening;
        public event Action ConsoleClosed;

        public Canvas Canvas => _canvas;

        public ConsoleDashboard Dashboard => _dashboard;

        public ConsoleOverlay Overlay => _overlay;

        public bool IsDashboardOpen => _dashboard.IsOpen;

        public bool IsOverlayVisible
        {
            get => _overlay.gameObject.activeSelf;
            set => _overlay.gameObject.SetActive(value);
        }


        public static ConsoleView Create()
        {
            var view = Instantiate(InternalResources.Instance.ConsoleView);
            view.name = "DevConsole";
            return view;
        }

        private void Awake()
        {
            _dashboard.Close();
        }

        private void OnEnable()
        {
            if (EventSystem.current == null)
                _eventSystem.gameObject.SetActive(true);
        }

        private void OnDisable()
        {
            _eventSystem.gameObject.SetActive(false);
        }

        internal void Init(DevConsoleConfig config)
        {
            _canvas.sortingOrder = config.Canvas.SortingOrder;

            _overlay.Init(this, config);
            _dashboard.Init(config);

            //_overlay.OpenConsoleTriggered += Open;
            _dashboard.CloseConsoleTriggered += Close;
        }

        public void Open()
        {
            if (IsDashboardOpen) return;

            ConsoleOpening?.Invoke();
            BroadcastConsoleOpening();
            _dashboard.Open();
        }

        public void Close()
        {
            if (!IsDashboardOpen) return;

            _dashboard.Close();
            BroadcastConsoleClosed();
            ConsoleClosed?.Invoke();
        }

        public void AddPage(IConsolePage page) 
            => _dashboard.AddPage(page);

        public void MoveToPage(int index, bool animate = true)
            => _dashboard.MoveToPage(index, animate);

        public void MoveToPage(IConsolePage page, bool animate = true)
            => _dashboard.MoveToPage(page, animate);

        #region Broadcasting

        private void BroadcastForward(Action<IConsoleWidget> action)
        {
            var pages = _dashboard.Pages;
            for (int i = 0; i < pages.Count; i++)
            {
                var page = pages[i];
                action(page);

                BroadcastForward(page.Widgets, action);
            }
        }

        private void BroadcastForward(IReadOnlyList<IConsoleWidget> widgets, Action<IConsoleWidget> action)
        {
            for (int i = 0; i < widgets.Count; i++)
            {
                var widget = widgets[i];
                action(widget);

                if (widget is IConsoleContainerWidget containter)
                    BroadcastForward(containter.Widgets, action);
            }
        }

        private void BroadcastBackward(Action<IConsoleWidget> action)
        {
            var pages = _dashboard.Pages;
            for (int i = pages.Count - 1; i >= 0; i--)
            {
                var page = pages[i];

                BroadcastBackward(page.Widgets, action);

                action(page);
            }
        }

        private void BroadcastBackward(IReadOnlyList<IConsoleWidget> widgets, Action<IConsoleWidget> action)
        {
            for (int i = widgets.Count - 1; i >= 0; i--)
            {
                var widget = widgets[i];

                if (widget is IConsoleContainerWidget containter)
                    BroadcastForward(containter.Widgets, action);

                action(widget);
            }
        }

        internal void BroadcastConsoleInitialize(IDevConsoleDesigner console)
        {
            BroadcastForward(widget =>
            {
                if (widget is IConsoleInitializeHandler hdl)
                    hdl.OnConsoleInitialize(console);
            });
        }

        private void BroadcastConsoleOpening()
        {
            BroadcastForward(widget =>
            {
                if (widget is IConsoleOpeningHandler hdl)
                    hdl.OnConsoleOpening();
            });
        }

        private void BroadcastConsoleClosed()
        {
            BroadcastBackward(widget =>
            {
                if (widget is IConsoleClosedHandler hdl)
                    hdl.OnConsoleClosed();
            });
        }

        #endregion
    }
}
