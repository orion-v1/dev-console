﻿using Orion.DevConsole.UI;
using System;
using UnityEngine;

namespace Orion.DevConsole
{
    internal class InternalResources : ScriptableObject
    {
        private const string ResourcesRoot = "Orion.DevConsole";

        [SerializeField] private ConsoleView _consoleView;
        [SerializeField] private ToolbarToggleButton _dashboardToolbarToggle;
        [SerializeField] private Sprite _infoIcon;
        [SerializeField] private Sprite _warningIcon;
        [SerializeField] private Sprite _errorIcon;
        [SerializeField] private Color _infoIconColor;
        [SerializeField] private Color _warningIconColor;
        [SerializeField] private Color _errorIconColor;

        public ConsoleView ConsoleView => _consoleView;
        public ToolbarToggleButton DashboardToolbarToggle => _dashboardToolbarToggle;
        public Sprite InfoIcon => _infoIcon;
        public Sprite WarningIcon => _warningIcon;
        public Sprite ErrorIcon => _errorIcon;
        public Color InfoIconColor => _infoIconColor;
        public Color WarningIconColor => _warningIconColor;
        public Color ErrorIconColor => _errorIconColor;

        #region Instance

        [NonSerialized]
        private static InternalResources _instance;

        public static InternalResources Instance => _instance ? _instance : (_instance = Load<InternalResources>("InternalResources"));

        #endregion

        #region Create/Load from resources

        public static T Load<T>(string path) where T : UnityEngine.Object
            => Resources.Load<T>($"{ResourcesRoot}/{path}");

        public static T CreateFromResource<T>(string path, string name = null) where T : UnityEngine.Object
        {
            T obj = Instantiate(Load<T>(path));
            obj.name = string.IsNullOrEmpty(name) ? typeof(T).Name : name;
            return obj;
        }

        #endregion
    }
}
