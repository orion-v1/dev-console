﻿using System;
using UnityEngine;

namespace Orion.DevConsole
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    [RequireComponent(typeof(RectTransform))]
    internal class ConsoleOverlay : MonoBehaviour, IConsoleOverlayDesigner
    {
        private RectTransform _transform;


        private void Awake()
        {
            _transform = transform as RectTransform;
        }

        public void Init(ConsoleView consoleView, DevConsoleConfig config)
        {
        }

        //public T GetWidget<T>() where T : IConsoleOverlayWidget
        //{
        //    // TODO: get reference to overlay widget
        //    return GetComponentInChildren<T>(true);
        //}

        public void AddWidget(IConsoleOverlayWidget widget)
        {
            widget.RectTransform.SetParent(_transform, false);
        }
    }
}
