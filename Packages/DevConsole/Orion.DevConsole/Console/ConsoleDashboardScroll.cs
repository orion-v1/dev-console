﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Orion.DevConsole.UI
{
    internal class ConsoleDashboardScroll : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        [SerializeField] private ScrollRect _scroll;
        [SerializeField] private GridLayoutGroup _grid;
        [SerializeField] private float _inertia = 5f;
        [SerializeField] private float _speed = 2f;

        private Vector2 _lastContentPosition;
        private Vector2 _lastTouchPoint;
        private Vector2 _inertiaStartPoint;
        private Vector2 _inertiaDelta;
        private float _inertiaProgress = 1f;
        private float _inertiaSpeed = 1f;


        public event Action ContentPositionChanged;

        public Vector2 ViewportSize => _scroll.viewport.rect.size;

        public Vector2 ContentSize => _scroll.content.rect.size;

        public Vector2 ContentPosition
        {
            get => _scroll.content.anchoredPosition;
            set => SetContentPosition(value);
        }

        public Vector2 CellSize => _grid.cellSize;

        public Vector2 CellSizeWithSpacing => _grid.cellSize + _grid.spacing;

        public Vector2Int GridDimensions { get; private set; }


        #region Event handlers

        private void Start()
        {
            _scroll.inertia = false;
        }

        private void LateUpdate()
        {
            if (_inertiaProgress < 1f)
            {
                _inertiaProgress += Time.unscaledDeltaTime * _inertiaSpeed;

                var pos = _inertiaProgress >= 1f
                    ? _inertiaStartPoint + _inertiaDelta
                    : _inertiaStartPoint + _inertiaDelta * (-Mathf.Pow(2f, -10f * _inertiaProgress) + 1f);

                SetContentPosition(pos, false);
            }

            if (_lastContentPosition != ContentPosition)
            {
                _lastContentPosition = ContentPosition;
                ContentPositionChanged?.Invoke();
            }
        }

        private void OnRectTransformDimensionsChange()
        {
            _grid.cellSize =
                _scroll.viewport.rect.size -
                new Vector2(_grid.padding.horizontal, _grid.padding.vertical);
        }

        private void OnDrawGizmos()
        {
            if (!_grid) return;

            Rect rect = _scroll.content.rect;
            if (rect.size.y <= 0f) return;

            Vector2 cellSize = CellSize;
            if (cellSize.y <= 0f) return;

            Vector2 cellSizeWithSpaceing = CellSizeWithSpacing;
            Vector2Int dim = GridDimensions;

            Gizmos.matrix = _scroll.content.localToWorldMatrix;
            Gizmos.color = Color.gray;
            Gizmos.DrawWireCube(rect.center, rect.size);

            Gizmos.color = Color.blue;
            Vector2 origin = new Vector2(_grid.padding.left, -_grid.padding.top);
            for (int y = 0; y < dim.y; y++)
            {
                for (int x = 0; x < dim.x; x++)
                {
                    Vector2 center = new Vector2(
                        origin.x + cellSizeWithSpaceing.x * x + cellSize.x * .5f,
                        origin.y - cellSizeWithSpaceing.y * y - cellSize.y * .5f);
                    Gizmos.DrawWireCube(center, cellSize);
                }
            }
        }

        #endregion

        #region Dragging

        public void OnBeginDrag(PointerEventData eventData)
        {
            CancelSnapping();
            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                transform as RectTransform,
                eventData.position,
                eventData.pressEventCamera,
                out _lastTouchPoint);
        }

        public void OnDrag(PointerEventData eventData)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                transform as RectTransform,
                eventData.position,
                eventData.pressEventCamera,
                out _lastTouchPoint);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            // calculate
            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                transform as RectTransform,
                eventData.position,
                eventData.pressEventCamera,
                out var position);

            Vector2 delta = (position - _lastTouchPoint) * _inertia;

            // get nearest position and clamp
            Vector2 maxBound = ContentSize - ViewportSize;
            position = GetNearestContentPosition(ContentPosition + delta);
            position.x = Mathf.Clamp(position.x, -maxBound.x, 0f);
            position.y = Mathf.Clamp(position.y, -maxBound.y, 0f);

            // snap to position
            SnapToPosition(position);
        }

        #endregion

        #region Snapping

        public void SetContentPosition(Vector2 position, bool resetActions = true)
        {
            if (resetActions) CancelSnapping();

            _scroll.content.anchoredPosition = position;
        }

        private void SnapToPosition(Vector2 position, float speed = 0f, bool animate = true)
        {
            if (!animate)
            {
                SetContentPosition(position, true);
                return;
            }

            _inertiaSpeed = speed > 0 ? speed : _speed;
            _inertiaStartPoint = ContentPosition;
            _inertiaDelta = position - _inertiaStartPoint;
            _inertiaProgress = 0f;
        }

        private void CancelSnapping()
        {
            _inertiaProgress = 1f;
        }

        public Vector2Int SnapToCell(Vector2Int index, bool animate = true)
        {
            Vector2 cellSize = CellSizeWithSpacing;
            if (cellSize.x <= 0f || cellSize.y <= 0f)
                return new Vector2Int(-1, -1);

            var dimensions = GridDimensions;
            if (dimensions.x <= 0 || dimensions.y <= 0)
                return new Vector2Int(-1, -1);

            Vector2 offset = Vector2.zero;
            if (index.x < 0)
            {
                index.x = 0;
                offset.x = 50;
            }
            else if (index.x >= dimensions.x)
            {
                index.x = dimensions.x - 1;
                offset.x = -50;
            }

            if (index.y < 0)
            {
                index.y = 0;
                offset.y = 50;
            }
            else if (index.y >= dimensions.y)
            {
                index.y = dimensions.y - 1;
                offset.y = -50;
            }

            var endPoint = new Vector2(-index.x * cellSize.x, -index.y * cellSize.y);

            // set position
            float speed = 0f;
            if (offset.x != 0f || offset.y != 0f) speed = _speed * 3f;
            SnapToPosition(endPoint + offset, speed, animate);

            return index;
        }

        #endregion

        public void UpdateGridDimensions()
        {
            GridDimensions = CalculateGridDimensions();
        }

        private Vector2Int CalculateGridDimensions()
        {
            Vector2 cellSize = CellSize;
            if (cellSize.x <= 0f || cellSize.y <= 0f)
                return Vector2Int.zero;

            Vector2 viewSize = ContentSize;
            viewSize.x -= _grid.padding.horizontal;
            viewSize.y -= _grid.padding.vertical;

            Vector2 cellSizeWithSpacing = CellSizeWithSpacing;

            int cols = Mathf.FloorToInt(viewSize.x / cellSizeWithSpacing.x);
            if (viewSize.x - cellSizeWithSpacing.x * cols >= cellSize.x)
                cols++;

            int rows = Mathf.CeilToInt(viewSize.y / cellSizeWithSpacing.y);

            return new Vector2Int(cols, rows);
        }

        public Vector2 GetNearestContentPosition(Vector2 position)
        {
            Vector2 cellSize = CellSizeWithSpacing;
            if (cellSize.x <= 0f || cellSize.y <= 0f)
                return position;

            float x_idx = Mathf.Round(position.x / cellSize.x);
            float y_idx = Mathf.Round(position.y / cellSize.y);

            return new Vector2(x_idx * cellSize.x, y_idx * cellSize.y);
        }

        public Vector2Int GetCellIndex(Vector2 position)
        {
            Vector2 cellSize = CellSizeWithSpacing;
            if (cellSize.x <= 0f || cellSize.y <= 0f)
                return new Vector2Int(-1, -1);

            return new Vector2Int(
                Mathf.RoundToInt(-position.x / cellSize.x),
                Mathf.RoundToInt(-position.y / cellSize.y));
        }
    }
}
