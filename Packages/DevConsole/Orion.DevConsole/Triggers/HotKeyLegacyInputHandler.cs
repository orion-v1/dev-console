﻿using UnityEngine;

namespace Orion.DevConsole
{
    internal class HotKeyLegacyInputHandler : MonoBehaviour
    {
        private IDevConsole _console;
        private KeyCode _keyCode;
        private KeyModifiers _keyModifiers;

        public void BindToDevConsole(IDevConsole console, KeyCode keyCode, KeyModifiers keyModifiers)
        {
            _console = console;
            _keyCode = keyCode;
            _keyModifiers = keyModifiers;
        }

        private void Update()
        {
            bool b = Input.GetKeyDown(_keyCode) &&
                CheckModifierKey(KeyModifiers.Shift, KeyCode.LeftShift, KeyCode.RightShift) &&
                CheckModifierKey(KeyModifiers.Ctrl, KeyCode.LeftControl, KeyCode.RightControl) &&
                CheckModifierKey(KeyModifiers.Alt, KeyCode.LeftAlt, KeyCode.RightAlt);

            if (b)
            {
                if (_console.IsOpen) _console.Close();
                else _console.Open();
            }


            bool CheckModifierKey(KeyModifiers modifier, KeyCode keyCode1, KeyCode keyCode2) =>
                (_keyModifiers & modifier) == 0 || Input.GetKey(keyCode1) || Input.GetKey(keyCode2);
        }
    }
}
