﻿using Orion.DevConsole.UI;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Orion.DevConsole
{
    [AddComponentMenu(ConsoleUtils.HiddenComponentMenu)]
    internal class OverlayButtonTriggerView : MonoBehaviour, IConsoleOverlayWidget, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private Sprite _toolbarIcon;
        [SerializeField] private UiMovableObject _movableObject;

        private ToolbarToggleButton _toolbarToggle;


        public event Action Clicked;

        public RectTransform RectTransform => transform as RectTransform;

        public Vector2 Position { get => _movableObject.Position; set => SetPosition(value); }

        public bool Visible { get => gameObject.activeSelf; set => SetVisible(value); }


        internal static OverlayButtonTriggerView Create()
        {
            const string AssetPath = "Triggers/OverlayButtonTrigger";
            return InternalResources.CreateFromResource<OverlayButtonTriggerView>(AssetPath);
        }

        public void Init(IDevConsoleDesigner console, OverlayButtonTrigger data)
        {
            // movable object
            _movableObject.ResetTransformAnchors();
            _movableObject.EndDrag += sender => ConsoleStateData.SetVector2(OverlayButtonTrigger.PositionKey, sender.Position);

            // toolbar button
            _toolbarToggle = ConsoleUtils.CreateToolbarToggleButton("Button", _toolbarIcon);
            _toolbarToggle.ValueChanged += sender => SetVisible(sender.Value, true);
            console.Dashboard.AddToolbarWidget(_toolbarToggle);

            // visibility
            bool visible = ConsoleStateData.GetBool(OverlayButtonTrigger.OverlayVisibleKey, data.VisibleByDefault);
            SetVisible(visible, false);

            // position
            var pos = ConsoleStateData.GetVector2(OverlayButtonTrigger.PositionKey);
            if (pos.HasValue) SetPosition(pos.Value, false);
            else SetPosition(data.DefaultPosition, data.DefaultAnchor, false);
        }


        private void SetVisible(bool value, bool keep = false)
        {
            gameObject.SetActive(value);
            _toolbarToggle.Value = value;
            if (keep) ConsoleStateData.SetBool(OverlayButtonTrigger.OverlayVisibleKey, value);
        }

        private void SetPosition(Vector2 position, Vector2 anchor, bool keep = false)
        {
            _movableObject.SetPosition(position, anchor);
            if (keep) ConsoleStateData.SetVector2(OverlayButtonTrigger.PositionKey, _movableObject.Position);
        }

        private void SetPosition(Vector2 position, bool keep = false)
            => SetPosition(position, new Vector2(.5f, .5f), keep);


        public void OnPointerClick(PointerEventData eventData)
        {
            if (_movableObject.IsDragging || eventData.used) return;

            Clicked?.Invoke();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            transform.localScale = new Vector3(1.1f, 1.1f, 1.1f);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            transform.localScale = Vector3.one;
        }
    }
}
