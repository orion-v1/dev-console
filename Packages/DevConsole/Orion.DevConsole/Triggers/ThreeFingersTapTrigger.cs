﻿namespace Orion.DevConsole
{
    public class ThreeFingersTapTrigger : MultipleTapTrigger
    {
        public ThreeFingersTapTrigger() : base(3) { }
    }
}
