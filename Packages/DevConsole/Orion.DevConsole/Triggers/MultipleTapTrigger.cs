﻿using System;

#if UNITY_INPUT_SYSTEM
using UnityEngine.InputSystem;
#endif

namespace Orion.DevConsole
{
    public class MultipleTapTrigger : IDevConsoleTrigger
    {
#if UNITY_INPUT_SYSTEM
        private InputAction _inputAction;
        private IDevConsole _console;

        public MultipleTapTrigger(int touchesCount)
        {
            if (touchesCount < 2)
                throw new ArgumentException("Number of touches should not be less than 2.");

            if (Touchscreen.current == null)
            {
#if !UNITY_EDITOR
                UnityEngine.Debug.LogWarning("Touchscreen is not supported.");
#endif
                return;
            }

            if (Touchscreen.current.touches.Count < touchesCount)
            {
                UnityEngine.Debug.LogWarning($"Touchscreen doesn't support {touchesCount} fingers.");
                return;
            }

            _inputAction = new InputAction($"MultipleTapTrigger_{touchesCount}", InputActionType.Button);
            _inputAction.AddBinding(Touchscreen.current.touches[touchesCount - 1].tap);
            _inputAction.performed += OnGesturePerformed;
        }

        public void BindToDevConsole(IDevConsoleDesigner designer, IDevConsole console)
        {
            _console = console;
            _inputAction?.Enable();
        }

        private void OnGesturePerformed(InputAction.CallbackContext obj)
        {
            if (_console == null) return;

            if (_console.IsOpen) _console.Close();
            else _console.Open();
        }
#else
        private readonly int _touchesCount;
        private MultipleTapLegacyInputHandler _inputHandler;

        public MultipleTapTrigger(int touchesCount)
        {
            if (touchesCount < 2)
                throw new ArgumentException("Number of touches should not be less than 2.");

            _touchesCount = touchesCount;
        }

        private MultipleTapLegacyInputHandler GetInputHandler()
        {
            if (_inputHandler == null)
                _inputHandler = MultipleTapLegacyInputHandler.Create(_touchesCount);

            return _inputHandler;
        }

        public void BindToDevConsole(IDevConsoleDesigner designer, IDevConsole console)
        {
            GetInputHandler().BindToDevConsole(console);
        }
#endif
    }
}
