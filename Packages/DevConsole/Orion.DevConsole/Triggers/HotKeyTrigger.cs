﻿using UnityEngine;

namespace Orion.DevConsole
{
    public class HotKeyTrigger : IDevConsoleTrigger
    {
        private KeyCode _keyCode;
        private KeyModifiers _keyModifiers;


        public HotKeyTrigger(KeyCode keyCode, KeyModifiers keyModifiers)
        {
            _keyCode = keyCode;
            _keyModifiers = keyModifiers;
        }

        public void BindToDevConsole(IDevConsoleDesigner designer, IDevConsole console)
        {
            GetInputHandler().BindToDevConsole(console, _keyCode, _keyModifiers);
        }

#if UNITY_INPUT_SYSTEM
        private HotKeyUnityInputHandler _inputHandler;

        private HotKeyUnityInputHandler GetInputHandler()
        {
            if (_inputHandler) return _inputHandler;

            _inputHandler = new GameObject(nameof(HotKeyUnityInputHandler))
                .AddComponent<HotKeyUnityInputHandler>();

            UnityEngine.Object.DontDestroyOnLoad(_inputHandler.gameObject);
            return _inputHandler;
        }
#else
        private HotKeyLegacyInputHandler _inputHandler;

        private HotKeyLegacyInputHandler GetInputHandler()
        {
            if (_inputHandler) return _inputHandler;

            _inputHandler = new GameObject(nameof(HotKeyLegacyInputHandler))
                .AddComponent<HotKeyLegacyInputHandler>();

            UnityEngine.Object.DontDestroyOnLoad(_inputHandler.gameObject);
            return _inputHandler;
        }
#endif
    }
}
