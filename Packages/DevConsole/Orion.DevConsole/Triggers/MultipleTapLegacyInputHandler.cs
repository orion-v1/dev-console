﻿using UnityEngine;

namespace Orion.DevConsole
{
    internal class MultipleTapLegacyInputHandler : MonoBehaviour
    {
        private IDevConsole _console;
        private bool _triggered;
        private int _touchesCount = 3;

        public static MultipleTapLegacyInputHandler Create(int touchesCount)
        {
            var handler = new GameObject(nameof(MultipleTapLegacyInputHandler))
                .AddComponent<MultipleTapLegacyInputHandler>();

            handler._touchesCount = touchesCount;
            DontDestroyOnLoad(handler.gameObject);

            return handler;
        }

        public void BindToDevConsole(IDevConsole console)
        {
            _console = console;
        }

        private void Update()
        {
            if (Input.touchCount >= _touchesCount)
            {
                if (!_triggered)
                {
                    _triggered = true;

                    if (_console.IsOpen) _console.Close();
                    else _console.Open();
                }
            }
            else _triggered = false;
        }
    }
}
