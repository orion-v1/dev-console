﻿using UnityEngine;

namespace Orion.DevConsole
{
    public class OverlayButtonTrigger : IDevConsoleTrigger
    {
        internal const string PositionKey = "OverlayButtonTrigger.Position";
        internal const string OverlayVisibleKey = "OverlayButtonTrigger.Visible";

        public bool VisibleByDefault { get; set; } = true;
        public Vector2 DefaultPosition { get; set; } = new Vector2(-100, 100);
        public Vector2 DefaultAnchor { get; set; } = new Vector2(1f, 0f);


        public void BindToDevConsole(IDevConsoleDesigner designer, IDevConsole console)
        {
            var button = OverlayButtonTriggerView.Create();
            button.Clicked += console.Open;

            designer.Overlay.AddWidget(button);
            designer.AddInitializeAction(d => button.Init(d, this));
        }

        #region API Chaining

        public OverlayButtonTrigger SetVisibleByDefault(bool value)
        {
            VisibleByDefault = value;
            return this;
        }

        public OverlayButtonTrigger SetDefaultPosition(Vector2 position, Vector2 anchor)
        {
            DefaultPosition = position;
            DefaultAnchor = anchor;
            return this;
        }

        public OverlayButtonTrigger SetDefaultPosition(Vector2 position)
        {
            DefaultPosition = position;
            DefaultAnchor = new Vector2(.5f, .5f);
            return this;
        }

        #endregion
    }
}
