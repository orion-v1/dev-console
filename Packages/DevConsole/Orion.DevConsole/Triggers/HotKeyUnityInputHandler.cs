﻿#if UNITY_INPUT_SYSTEM
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;

namespace Orion.DevConsole
{
    internal class HotKeyUnityInputHandler : MonoBehaviour
    {
        private IDevConsole _console;
        private KeyModifiers _keyModifiers;
        private ButtonControl _buttonControl;
        private bool _isPressed;

        public void BindToDevConsole(IDevConsole console, KeyCode keyCode, KeyModifiers keyModifiers)
        {
            _console = console;
            _keyModifiers = keyModifiers;
            _buttonControl = GetButtonControl(keyCode);
        }

        private ButtonControl GetButtonControl(KeyCode keyCode)
        {
            Key key = Key.None;
            switch (keyCode)
            {
                case KeyCode.Backslash: key = Key.Backslash; break;
                case KeyCode.Backspace: key = Key.Backspace; break;
                case KeyCode.Tab: key = Key.Tab; break;
                case KeyCode.Return: key = Key.Enter; break;
                case KeyCode.Escape: key = Key.Escape; break;
                case KeyCode.Space: key = Key.Space; break;
                //case KeyCode.Tilde: key = Key.???; break;
                case KeyCode.A: key = Key.A; break;
                case KeyCode.B: key = Key.B; break;
                case KeyCode.C: key = Key.C; break;
                case KeyCode.D: key = Key.D; break;
                case KeyCode.E: key = Key.E; break;
                case KeyCode.F: key = Key.F; break;
                case KeyCode.G: key = Key.G; break;
                case KeyCode.H: key = Key.H; break;
                case KeyCode.I: key = Key.I; break;
                case KeyCode.J: key = Key.J; break;
                case KeyCode.K: key = Key.K; break;
                case KeyCode.L: key = Key.L; break;
                case KeyCode.M: key = Key.M; break;
                case KeyCode.N: key = Key.N; break;
                case KeyCode.O: key = Key.O; break;
                case KeyCode.P: key = Key.P; break;
                case KeyCode.Q: key = Key.Q; break;
                case KeyCode.R: key = Key.R; break;
                case KeyCode.S: key = Key.S; break;
                case KeyCode.T: key = Key.T; break;
                case KeyCode.U: key = Key.U; break;
                case KeyCode.V: key = Key.V; break;
                case KeyCode.W: key = Key.W; break;
                case KeyCode.X: key = Key.X; break;
                case KeyCode.Y: key = Key.Y; break;
                case KeyCode.Z: key = Key.Z; break;
                case KeyCode.Alpha0: key = Key.Digit0; break;
                case KeyCode.Alpha1: key = Key.Digit1; break;
                case KeyCode.Alpha2: key = Key.Digit2; break;
                case KeyCode.Alpha3: key = Key.Digit3; break;
                case KeyCode.Alpha4: key = Key.Digit4; break;
                case KeyCode.Alpha5: key = Key.Digit5; break;
                case KeyCode.Alpha6: key = Key.Digit6; break;
                case KeyCode.Alpha7: key = Key.Digit7; break;
                case KeyCode.Alpha8: key = Key.Digit8; break;
                case KeyCode.Alpha9: key = Key.Digit9; break;
                case KeyCode.Minus: key = Key.Minus; break;
                case KeyCode.Equals: key = Key.Equals; break;
                case KeyCode.Keypad0: key = Key.Numpad0; break;
                case KeyCode.Keypad1: key = Key.Numpad1; break;
                case KeyCode.Keypad2: key = Key.Numpad2; break;
                case KeyCode.Keypad3: key = Key.Numpad3; break;
                case KeyCode.Keypad4: key = Key.Numpad4; break;
                case KeyCode.Keypad5: key = Key.Numpad5; break;
                case KeyCode.Keypad6: key = Key.Numpad6; break;
                case KeyCode.Keypad7: key = Key.Numpad7; break;
                case KeyCode.Keypad8: key = Key.Numpad8; break;
                case KeyCode.Keypad9: key = Key.Numpad9; break;
                case KeyCode.KeypadPlus: key = Key.NumpadPlus; break;
                case KeyCode.KeypadMinus: key = Key.NumpadMinus; break;
                case KeyCode.KeypadMultiply: key = Key.NumpadMultiply; break;
                case KeyCode.KeypadDivide: key = Key.NumpadDivide; break;
                case KeyCode.KeypadPeriod: key = Key.NumpadPeriod; break;
                case KeyCode.KeypadEnter: key = Key.NumpadEnter; break;
                case KeyCode.Insert: key = Key.Insert; break;
                case KeyCode.Delete: key = Key.Delete; break;
                case KeyCode.Home: key = Key.Home; break;
                case KeyCode.End: key = Key.End; break;
                case KeyCode.PageUp: key = Key.PageUp; break;
                case KeyCode.PageDown: key = Key.PageDown; break;
                case KeyCode.F1: key = Key.F1; break;
                case KeyCode.F2: key = Key.F2; break;
                case KeyCode.F3: key = Key.F3; break;
                case KeyCode.F4: key = Key.F4; break;
                case KeyCode.F5: key = Key.F5; break;
                case KeyCode.F6: key = Key.F6; break;
                case KeyCode.F7: key = Key.F7; break;
                case KeyCode.F8: key = Key.F8; break;
                case KeyCode.F9: key = Key.F9; break;
                case KeyCode.F10: key = Key.F10; break;
                case KeyCode.F11: key = Key.F11; break;
                case KeyCode.F12: key = Key.F12; break;

                default:
                    Debug.LogWarning($"Failed to handle key '{keyCode}'. Not implemented.");
                    break;
            }

            return key != Key.None
                ? Keyboard.current[key]
                : null;
        }

        private void Update()
        {
            if (_buttonControl == null) return;

            if (!_isPressed && _buttonControl.isPressed)
            {
                _isPressed = true;
                KeyDown();
            }
            else if (_isPressed && !_buttonControl.isPressed)
            {
                _isPressed = false;
            }
        }

        private void KeyDown()
        {
            bool b =
                CheckModifierKey(KeyModifiers.Shift, Keyboard.current.shiftKey) &&
                CheckModifierKey(KeyModifiers.Ctrl, Keyboard.current.ctrlKey) &&
                CheckModifierKey(KeyModifiers.Alt, Keyboard.current.altKey);

            if (b)
            {
                if (_console.IsOpen) _console.Close();
                else _console.Open();
            }


            bool CheckModifierKey(KeyModifiers modifier, ButtonControl buttonControl) =>
                (_keyModifiers & modifier) == 0 || buttonControl.isPressed;
        }
    }
}
#endif
